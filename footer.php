    <!--Footer Widget Wrap Start-->
    <footer class="kf_footer_bg">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-3 col-sm-6">
                	<div class="kf_foo_contact">
                    	<div class="kf_footer_logo">
                        	<a href="#"><img src="assets/footer-logo.png" alt=""></a>
                            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum..</p>
                        </div>
                        <ul class="kf_foo_address">
                        	<li>
                            	<i class="fa fa-facebook"></i>
                                <span>666 71 888 &amp; 211 222 333</span>
                            </li>
                            <li>
                            	<i class="fa fa-envelope"></i>
                                <a href="#">info@kodeforest.com</a>
                            </li>
                            <li>
                            	<i class="fa fa-map-marker"></i>
                                <span>Visit Us 153 Fake Street-London United Kindom</span>
                            </li>
                        </ul>
                        <ul class="kf_foo_social_icon">
                        	<li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                	<div class="kf_foo_featured_listing widget">
                    	<h6>Featured Listing</h6>
                        <figure>
                        	<img src="assets/featured-listing-01.jpg" alt="">
                            <figcaption class="kf_foo_listing_hover">
                            	<p>$ 2,856,00</p>
                            </figcaption>
                        </figure>
                        <div class="kf_foo_listing_des">
                        	<h6><a href="#">Good Looking Family Vila</a></h6>
                            <p>2908 Rd. South Colomibanos Stree DE 12907</p>
                            <ul class="kf_foo_listing_meta">
                                <li><i class="fa fa-bed"></i>7 bd</li>
                                <li><i class="icon-bath"></i>4 ba</li>
                                <li><i class="fa fa-arrows-alt"></i>9387 sqf</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                	<div class="kf_foo_latest_wrap widget">
                    	<h6>Latest Post</h6>
                        <ul>
                        	<li>
                            	<div class="kf_foo_latest_post">
                                	<figure>
                                    	<img src="assets/latest-post-01.jpg" alt="">
                                    </figure>
                                    <div class="kf_foo_post_des">
                                    	<p><a href="#">An organization or Economic</a></p>
                                        <span>Feb 12 2016</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                            	<div class="kf_foo_latest_post">
                                	<figure>
                                    	<img src="assets/latest-post-01.jpg" alt="">
                                    </figure>
                                    <div class="kf_foo_post_des">
                                    	<p><a href="#">An organization or Economic</a></p>
                                        <span>Feb 12 2016</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                            	<div class="kf_foo_latest_post">
                                	<figure>
                                    	<img src="assets/latest-post-01.jpg" alt="">
                                    </figure>
                                    <div class="kf_foo_post_des">
                                    	<p><a href="#">An organization or Economic</a></p>
                                        <span>Feb 12 2016</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                	<div class="kf_foo_property_type widget">
                    	<h6>PROPERTY TYPES</h6>
                        <ul>
                        	<li><a href="#">Residential  <span>09</span></a></li>
                            <li><a href="#">Villa  <span>10</span></a></li>
                            <li><a href="#">Single Family  <span>11</span></a></li>
                            <li><a href="#">Condominium  <span>08</span></a></li>
                            <li><a href="#">Apartment Building  <span>03</span></a></li>
                            <li><a href="#">Apartment  <span>06</span></a></li>
                            <li><a href="#">Commercial  <span>18</span></a></li>
                            <li><a href="#">Shop  <span>12</span></a></li>
                            <li><a href="#">Office  <span>09</span></a></li>
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>
    </footer>
    <!--Footer Widget Wrap End-->
    
    <!--Copy Right Wrap Start-->
    <div class="kf_copy_right">
    	<div class="container">
        	<div class="kf_copyright_element">
            	<ul>
                	<li>Copyright@2016 <a href="#">kodeproperty</a></li>
                    <li><a href="#">Terms &amp; Coditions</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li>Designed by <a href="#"> kodeforest</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!--Copy Right Wrap Start-->
    
    <!--Footer Wrap End-->
	
</div>
<!--Kode Wrapper End-->



    <!--Jquery Library-->
    <script src="assets/jquery_004.js"></script>
    <!--Jquery UI Library-->
    <script src="assets/range-slider.js"></script>
	<!--Bootstrap core JavaScript-->
    <script src="assets/bootstrap.js"></script>
    <!--Bx-Slider JavaScript-->
	<script src="assets/jquery_003.js"></script>
    <!--Owl Carousel JavaScript-->
	<script src="assets/owl.js"></script-->
    <!--Pretty Photo JavaScript-->
	<script src="assets/jquery_005.js"></script>
    <!--Accordian JavaScript-->
	<script src="assets/jquery.js"></script>
    <!--Number Count (Waypoints) JavaScript-->
	<script src="assets/waypoints-min.js"></script>
    <!-- Chosen Script Javascript -->
    <script src="assets/chosen.js"></script>
	<!-- Time Counter Script Javascript -->
    <script src="assets/jquery_006.js"></script>
	<!-- Filterable Script Javascript -->
    <script src="assets/jquery-filterable.js"></script>
	<!--Dl Menu Script-->
	<script src="assets/modernizr.js"></script>
	<script src="assets/jquery_002.js"></script>
	<!--Map JavaScript-->
    <script src="assets/js"></script>
    <!--Custom JavaScript-->
	<script src="assets/custom.js"></script>

  

</body></html>
