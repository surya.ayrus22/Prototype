   <?php include('header.php');?>
    <!--Sub Banner Wrap Start-->
    <div class="kf_property_sub_banner">
    	<div class="container">
        	<div class="kf_sub_banner_hdg">
            	<h3>Property Listing 02</h3>
            </div>
            <div class="kf_property_breadcrumb">
            	<ul>
                	<li><a href="#">Home</a></li>
                    <li><a href="#">Property Listing 02</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!--Sub Banner Wrap End-->
    
    <!--Content Wrap Start-->
    <div class="kf_property_content_wrap">
        <!--Property Listing 01 Wrap Start-->
        <section class="kf_property_lissting_bg">
        	<div class="container">
            	<!--Property Meta Wrap Start-->
            	<div class="kf_property_meta">
                	<h5>Property Listing Veiw</h5>
                    <div class="kf_view_type">
                    	<div class="kf_property_view">
                            <span>Sort By:</span>
                            <select style="display: none;" class="chosen-select">
                                <option selected="selected">Any</option>
                                <option>Option 2</option>
                                <option>Option 3</option>
                            </select><div title="" style="width: 145px;" class="chosen-container chosen-container-single"><a class="chosen-single" tabindex="-1"><span>Any</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input autocomplete="off" type="text"></div><ul class="chosen-results"></ul></div></div>
                        </div>
                        
                        <div class="kf_property_view">
                            <span>Select Veiw:</span>
                            <select style="display: none;" class="chosen-select">
                                <option selected="selected">Any</option>
                                <option>Option 2</option>
                                <option>Option 3</option>
                            </select><div title="" style="width: 145px;" class="chosen-container chosen-container-single"><a class="chosen-single" tabindex="-1"><span>Any</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input autocomplete="off" type="text"></div><ul class="chosen-results"></ul></div></div>
                        </div>
                        
                        <div class="kf_property_view">
                            <span>Select Veiw:</span>
                            <a href="list_view_all.php"><i class="fa fa-th-list"></i></a>
                            <a href="grid_view_all.php"><i class="fa fa-th-large"></i></a>
                        </div>
                    </div>
                    
                </div>
                <!--Property Meta Wrap End-->
                
                <!--Section Wrap Start-->
                <div class="row">
                	<!--Property Listing 02 List Wrap Start-->
                	<div class="col-md-9">
                        <div class="kf_listing_outer_wrap">
                            <div class="kf_property_img">
                                <figure>
                                    <img src="assets/property-listing-01.jpg" alt="image">
                                </figure>
                            </div>
                            <div class="kf_property_place">
                                <div class="kf_property_caption">
                                    <h5><a href="#">University near to sabestian</a></h5>
                                    <p><i class="fa fa-map-marker"></i>Myths Vealesd  568 E 1st Ave, New Jersey </p>
                                </div>
                                <h5>$2,450,00</h5>
                                <ul class="kf_property_dolar">
                                    <li><i class="fa fa-arrows"></i><a href="#">Area: 20,000 Sq. FT</a></li>
                                    <li><i class="fa fa-bed"></i><a href="#">Bedroom: 5</a></li>
                                    <li><i class="fa fa-bed"></i><a href="#">Bathrooms: 03</a></li>
                                </ul>
                                <ul class="kf_property_dolar">
                                    <li><i class="fa fa-arrows"></i><a href="#">Garage:  2</a></li>
                                    <li><i class="fa fa-arrows"></i><a href="#">Swim-poll:  1</a></li>
                                </ul>
                                <a class="kf_property_more" href="#">More Information</a>
                            </div>
                        </div>
                        <div class="kf_listing_outer_wrap">
                            <div class="kf_property_img">
                                <figure>
                                    <img src="assets/property-listing-02.jpg" alt="image">
                                </figure>
                            </div>
                            <div class="kf_property_place">
                                <div class="kf_property_caption">
                                    <h5><a href="#">LAW FIRM OPENED NEAR TO VILLA</a></h5>
                                    <p><i class="fa fa-map-marker"></i>Braidwood Dakota 68 E 1st, New Jersey </p>
                                </div>
                                <h5>$8,450,00</h5>
                                <ul class="kf_property_dolar">
                                    <li><i class="fa fa-arrows"></i><a href="#">Area: 20,000 Sq. FT</a></li>
                                    <li><i class="fa fa-bed"></i><a href="#">Bedroom: 05</a></li>
                                    <li><i class="fa fa-bed"></i><a href="#">Bathrooms: 06</a></li>
                                </ul>
                                <ul class="kf_property_dolar">
                                    <li><i class="fa fa-arrows"></i><a href="#">Garage:  2</a></li>
                                    <li><i class="fa fa-arrows"></i><a href="#">Swim-poll:  1</a></li>
                                </ul>
                                <a class="kf_property_more" href="#">More Information</a>
                            </div>
                        </div>
                        <div class="kf_listing_outer_wrap">
                            <div class="kf_property_img">
                                <figure>
                                    <img src="assets/property-listing-03.jpg" alt="image">
                                </figure>
                            </div>
                            <div class="kf_property_place">
                                <div class="kf_property_caption">
                                    <h5><a href="#">Villa In Hialeah, Dade County</a></h5>
                                    <p><i class="fa fa-map-marker"></i>  Queen's Walk, 568 E 1st Ave, New Jersey </p>
                                </div>
                                <h5>$65,00</h5>
                                <ul class="kf_property_dolar">
                                    <li><i class="fa fa-arrows"></i><a href="#">Area: 80,000 Sq. FT</a></li>
                                    <li><i class="fa fa-bed"></i><a href="#">Bedroom: 05</a></li>
                                    <li><i class="fa fa-bed"></i><a href="#">Bathrooms: 12</a></li>
                                </ul>
                                <ul class="kf_property_dolar">
                                    <li><i class="fa fa-arrows"></i><a href="#">Garage:  2</a></li>
                                    <li><i class="fa fa-arrows"></i><a href="#">Swim-poll:  1</a></li>
                                </ul>
                                <a class="kf_property_more" href="#">More Information</a>
                            </div>
                        </div>
                        <div class="kf_listing_outer_wrap">
                            <div class="kf_property_img">
                                <figure>
                                    <img src="assets/property-listing-04.jpg" alt="image">
                                </figure>
                            </div>
                            <div class="kf_property_place">
                                <div class="kf_property_caption">
                                    <h5><a href="#">1200 Anastasia Avenue, Coral Gables</a></h5>
                                    <p><i class="fa fa-map-marker"></i>Trinity Square 86 A Block </p>
                                </div>
                                <h5>$3,50,00</h5>
                                <ul class="kf_property_dolar">
                                    <li><i class="fa fa-arrows"></i><a href="#">Area: 15,000 Sq. FT</a></li>
                                    <li><i class="fa fa-bed"></i><a href="#">Bedroom: 10</a></li>
                                    <li><i class="fa fa-bed"></i><a href="#">Bathrooms: 13</a></li>
                                </ul>
                                <ul class="kf_property_dolar">
                                    <li><i class="fa fa-arrows"></i><a href="#">Garage:  2</a></li>
                                    <li><i class="fa fa-arrows"></i><a href="#">Swim-poll:  1</a></li>
                                </ul>
                                <a class="kf_property_more" href="#">More Information</a>
                            </div>
                        </div>
                        <div class="kf_listing_outer_wrap">
                            <div class="kf_property_img">
                                <figure>
                                    <img src="assets/property-listing-05.jpg" alt="image">
                                </figure>
                            </div>
                            <div class="kf_property_place">
                                <div class="kf_property_caption">
                                    <h5><a href="#">20 Apartments Of Type A</a></h5>
                                    <p><i class="fa fa-map-marker"></i>Braidwood Dakota </p>
                                </div>
                                <h5>$5,450,00</h5>
                                <ul class="kf_property_dolar">
                                    <li><i class="fa fa-arrows"></i><a href="#">Area: 5,000 Sq. FT</a></li>
                                    <li><i class="fa fa-bed"></i><a href="#">Bedroom: 8</a></li>
                                    <li><i class="fa fa-bed"></i><a href="#">Bathrooms: 10</a></li>
                                </ul>
                                <ul class="kf_property_dolar">
                                    <li><i class="fa fa-arrows"></i><a href="#">Garage:  2</a></li>
                                    <li><i class="fa fa-arrows"></i><a href="#">Swim-poll:  1</a></li>
                                </ul>
                                <a class="kf_property_more" href="#">More Information</a>
                            </div>
                        </div>
                        <div class="kf_listing_outer_wrap">
                            <div class="kf_property_img">
                                <figure>
                                    <img src="assets/property-listing-06.jpg" alt="image">
                                </figure>
                            </div>
                            <div class="kf_property_place">
                                <div class="kf_property_caption">
                                    <h5><a href="#">University near to sabestian villa</a></h5>
                                    <p><i class="fa fa-map-marker"></i>South Aylesford </p>
                                </div>
                                <h5>$450,00</h5>
                                <ul class="kf_property_dolar">
                                    <li><i class="fa fa-arrows"></i><a href="#">Area: 20,000 Sq. FT</a></li>
                                    <li><i class="fa fa-bed"></i><a href="#">Bedroom: 5</a></li>
                                    <li><i class="fa fa-bed"></i><a href="#">Bathrooms: 03</a></li>
                                </ul>
                                <ul class="kf_property_dolar">
                                    <li><i class="fa fa-arrows"></i><a href="#">Garage:  2</a></li>
                                    <li><i class="fa fa-arrows"></i><a href="#">Swim-poll:  1</a></li>
                                </ul>
                                <a class="kf_property_more" href="#">More Information</a>
                            </div>
                        </div>
                    </div>
                    <!--Property Listing 02 List Wrap End-->
                   
                    <!--Aside bar Wrap Start-->
                    <div class="col-md-3">
                    	<div class="kf_asidebar_wrap">
                        	<div class="kf_search_property_wrap">
                            	<h6>Search for Properties</h6>
                                <div class="kf_property_element">
                                	<div class="kf_property_field">
                                        <select style="display: none;" class="chosen-select">
                                            <option selected="selected">City</option>
                                            <option>Option 2</option>
                                            <option>Option 3</option>
                                        </select>
                                    </div>
                                    
                                    <div class="kf_property_field">
                                        <select style="display: none;" class="chosen-select">
                                            <option selected="selected">Location</option>
                                            <option>Option 2</option>
                                            <option>Option 3</option>
                                        </select>
                                    </div>
                                    
                                    <div class="kf_property_field">
                                        <select style="display: none;" class="chosen-select">
                                            <option selected="selected">Property Status</option>
                                            <option>Option 2</option>
                                            <option>Option 3</option>
                                        </select>
                                    </div>
                                    
                                    <div class="kf_property_field">
                                        <select style="display: none;" class="chosen-select">
                                            <option selected="selected">Property Type</option>
                                            <option>Option 2</option>
                                            <option>Option 3</option>
                                        </select>
                                    </div>
                                    
                                    <div class="kf_property_field">
                                        <select style="display: none;" class="chosen-select">
                                            <option selected="selected">Bedroom</option>
                                            <option>Option 2</option>
                                            <option>Option 3</option>
                                        </select>
                                    </div>
                                    
                                    <div class="kf_property_field">
                                        <select style="display: none;" class="chosen-select">
                                            <option selected="selected">Bathroom</option>
                                            <option>Option 2</option>
                                            <option>Option 3</option>
                                        </select>
                                    </div>
                                    
                                   
                                    <div class="kf_property_field">
                                    	<a href="#">More Filter</a>
                                        <input value="Search" type="submit">
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="kf_aside_post_wrap aside_hdg">
                            	<h5>Featured Post</h5>
                                <ul>
                                	<li>
                                    	<figure>
                                        	<img src="assets/aside-post-01.jpg" alt="">
                                        </figure>
                                        <div class="kf_aside_post_des">
                                        	<h6><a href="#">Myths Vealesd </a></h6>
                                            <p>Vila</p>
                                            <span>$2,965,300</span>
                                        </div>
                                    </li>
                                    <li>
                                    	<figure>
                                        	<img src="assets/aside-post-02.jpg" alt="">
                                        </figure>
                                        <div class="kf_aside_post_des">
                                        	<h6><a href="#">Classic home </a></h6>
                                            <p>Vila</p>
                                            <span>$2,965,300</span>
                                        </div>
                                    </li>
                                    <li>
                                    	<figure>
                                        	<img src="assets/aside-post-03.jpg" alt="">
                                        </figure>
                                        <div class="kf_aside_post_des">
                                        	<h6><a href="#">Sunrise Townhomes </a></h6>
                                            <p>Vila</p>
                                            <span>$2,965,300</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            
                            <div class="kf_aside_fea_agent aside_hdg">
                            	<h5>Featured Agent</h5>
                                <ul>
                                	<li>
                                    	<figure>
                                        	<img src="assets/aside-post-04.jpg" alt="">
                                        </figure>
                                        <div class="kf_aside_agent_des">
                                        	<h6><a href="#">Beautiful single home </a></h6>
                                            <span><i class="fa fa-phone"></i>+91 222 334 551</span>
                                        </div>
                                    </li>
                                    <li>
                                    	<figure>
                                        	<img src="assets/aside-post-05.jpg" alt="">
                                        </figure>
                                        <div class="kf_aside_agent_des">
                                        	<h6><a href="#">Charming single family </a></h6>
                                            <span><i class="fa fa-phone"></i>+91 222 334 551</span>
                                        </div>
                                    </li>
                                    <li>
                                    	<figure>
                                        	<img src="assets/aside-post-06.jpg" alt="">
                                        </figure>
                                        <div class="kf_aside_agent_des">
                                        	<h6><a href="#">Hansom Alex </a></h6>
                                            <span><i class="fa fa-phone"></i>+91 222 334 551</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            
                            <div class="kf_aside_fea_properties aside_hdg">
                            	<h5>Featured Agent</h5>
                                <ul>
                                	<li><a href="#"><img src="assets/aside-post-01.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="assets/aside-post-02.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="assets/aside-post-03.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="assets/aside-post-04.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="assets/aside-post-05.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="assets/aside-post-06.jpg" alt=""></a></li>
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                    <!--Aside bar Wrap End-->
                   
                </div>
                <!--Section Wrap End-->
            </div>
        </section>
        <!--Property Listing 01 Wrap End-->
     </div>   	
    <!--Content Wrap End-->
    
    <!--Footer Wrap Start-->
    
    <!--Map Wrap Start-->
    <!--div class="kf_property_map_wrap">
        <div style="position: relative; background-color: rgb(229, 227, 223); overflow: hidden;" class="map-canvas" id="map-canvas"><div class="gm-style" style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; width: 100%; will-change: transform;"><div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; position: absolute; left: 318px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 574px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 318px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 318px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 574px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 574px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 62px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 830px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 62px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 62px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 830px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 830px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -194px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1086px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -194px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -194px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1086px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1086px; top: 264px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 318px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 574px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 318px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 318px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 574px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 574px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 62px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 830px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 62px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 62px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 830px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 830px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -194px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1086px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -194px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -194px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1086px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1086px; top: 264px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="position: absolute; left: 318px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_007.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 574px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_005.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 318px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_014.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 318px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_012.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 574px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_015.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 574px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_008.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 62px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_017.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 830px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_002.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 62px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_009.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 62px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_016.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 830px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_004.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 830px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_013.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -194px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_010.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1086px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -194px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_011.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -194px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_018.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1086px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_006.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1086px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_003.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div></div></div><div class="gm-style-pbc" style="position: absolute; left: 0px; top: 0px; z-index: 2; width: 100%; height: 100%; transition-duration: 0.3s; opacity: 0; display: none;"><p class="gm-style-pbt">Use two fingers to move the map</p></div><div style="position: absolute; left: 0px; top: 0px; z-index: 3; width: 100%; height: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 4; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a title="Click to see this area on Google Maps" href="https://maps.google.com/maps?ll=40.674389,-73.9455&amp;z=15&amp;hl=en-US&amp;gl=US&amp;mapclient=apiv3" target="_blank" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 66px; height: 26px; cursor: pointer;"><img draggable="false" src="assets/google_white5.png" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;"></div></a></div><div style="background-color: white; padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto,Arial,sans-serif; color: rgb(34, 34, 34); box-shadow: 0px 4px 16px rgba(0, 0, 0, 0.2); z-index: 10000002; display: none; width: 256px; height: 148px; position: absolute; left: 505px; top: 110px;"><div style="padding: 0px 0px 10px; font-size: 16px;">Map Data</div><div style="font-size: 13px;">Map data ©2016 Google</div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img draggable="false" src="assets/mapcnt6.png" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div><div style="z-index: 1000001; position: absolute; right: 75px; bottom: 0px; width: 121px;" class="gmnoprint"><div class="gm-style-cc" style="-moz-user-select: none; height: 14px; line-height: 14px;" draggable="false"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer; display: none;">Map Data</a><span style="">Map data ©2016 Google</span></div></div></div><div style="position: absolute; right: 0px; bottom: 0px;" class="gmnoscreen"><div style="font-family: Roboto,Arial,sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Map data ©2016 Google</div></div><div draggable="false" style="z-index: 1000001; -moz-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;" class="gmnoprint gm-style-cc"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_blank" href="https://www.google.com/intl/en-US_US/help/terms_maps.html" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Terms of Use</a></div></div><div style="width: 25px; height: 25px; overflow: hidden; display: none; margin: 10px 14px; position: absolute; top: 0px; right: 0px;"><img class="gm-fullscreen-control" draggable="false" src="assets/sv5.png" style="position: absolute; left: -52px; top: -86px; width: 164px; height: 112px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;"></div><div class="gm-style-cc" style="-moz-user-select: none; height: 14px; line-height: 14px; display: none; position: absolute; right: 0px; bottom: 0px;" draggable="false"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/maps/@40.674389,-73.9455,15z/data=%2110m1%211e1%2112b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;" title="Report errors in the road map or imagery to Google" target="_new">Report a map error</a></div></div><div controlheight="93" controlwidth="28" draggable="false" style="margin: 10px; -moz-user-select: none; position: absolute; bottom: 107px; right: 28px;" class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom"><div controlheight="55" controlwidth="28" style="position: absolute; left: 0px; top: 38px;" class="gmnoprint"><div style="-moz-user-select: none; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); border-radius: 2px; cursor: pointer; background-color: rgb(255, 255, 255); width: 28px; height: 55px;" draggable="false"><div style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;" title="Zoom in"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img draggable="false" src="assets/tmapctrl.png" style="position: absolute; left: 0px; top: 0px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div><div style="position: relative; overflow: hidden; width: 67%; height: 1px; left: 16%; background-color: rgb(230, 230, 230); top: 0px;"></div><div style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;" title="Zoom out"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img draggable="false" src="assets/tmapctrl.png" style="position: absolute; left: 0px; top: -15px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div></div></div><div controlheight="28" controlwidth="28" style="background-color: rgb(255, 255, 255); box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); border-radius: 2px; width: 28px; height: 28px; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default; position: absolute; left: 0px; top: 0px;" class="gm-svpc"><div style="position: absolute; left: 1px; top: 1px;"></div><div style="position: absolute; left: 1px; top: 1px;"><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px;"><img draggable="false" src="assets/cb_scout5.png" style="position: absolute; left: -147px; top: -26px; width: 215px; height: 835px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Pegman is on top of the Map" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img draggable="false" src="assets/cb_scout5.png" style="position: absolute; left: -147px; top: -52px; width: 215px; height: 835px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img draggable="false" src="assets/cb_scout5.png" style="position: absolute; left: -147px; top: -78px; width: 215px; height: 835px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div style="display: none; position: absolute;" controlheight="0" controlwidth="28" class="gmnoprint"><div title="Rotate map 90 degrees" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); cursor: pointer; background-color: rgb(255, 255, 255); display: none;"><img draggable="false" src="assets/tmapctrl4.png" style="position: absolute; left: -141px; top: 6px; width: 170px; height: 54px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div class="gm-tilt" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); top: 0px; cursor: pointer; background-color: rgb(255, 255, 255);"><img draggable="false" src="assets/tmapctrl4.png" style="position: absolute; left: -141px; top: -13px; width: 170px; height: 54px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div style="margin: 10px; z-index: 0; position: absolute; cursor: pointer; left: 0px; top: 0px;" class="gmnoprint"><div class="gm-style-mtc" style="float: left;"><div title="Show street map" draggable="false" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(86, 86, 86); font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-left-radius: 2px; border-top-left-radius: 2px; background-clip: padding-box; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); min-width: 22px;">Map</div></div><div class="gm-style-mtc" style="float: left;"><div draggable="false" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(0, 0, 0); font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-right-radius: 2px; border-top-right-radius: 2px; background-clip: padding-box; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); border-left: 0px none; min-width: 67px; font-weight: 500;">Custom Style</div></div></div></div></div>
    </div>
    <!--Map Wrap End-->
    
    <!--Company Wrap Start-->
    <!--div class="kf_property_compnay_bg">
        <div class="container">
            <div style="opacity: 1; display: block;" class="kf_company_slider owl-carousel owl-theme">
                <div class="owl-wrapper-outer"><div style="width: 3192px; left: 0px; display: block; transition: all 1000ms ease 0s; transform: translate3d(0px, 0px, 0px);" class="owl-wrapper"><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-01.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-02.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-03.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-04.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-05.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-01.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-02.jpg" alt=""></a>
                    </div>
                </div></div></div></div>
                
                
                
                
                
                
            <div style="display: block;" class="owl-controls clickable"><div class="owl-pagination"><div class="owl-page active"><span class=""></span></div><div class="owl-page"><span class=""></span></div></div><div class="owl-buttons"><div class="owl-prev">prev</div><div class="owl-next">next</div></div></div></div>
        </div>
    </div>
    <!--Company Wrap End-->
    <?php include('footer.php');?>
