<?php include('header.php');?>
<style>
.bx-prev {
    display: none;
}
.bx-next {
    display: none;
}
</style>
    <!--Main Banner Wrap Start-->
    <div class="kf_banner_wrap">
    	<div style="max-width: 100%;" class="bx-wrapper"><div style="width: 100%; overflow: hidden; position: relative; height: 539px;" class="bx-viewport"><ul style="width: 815%; position: relative; transition-duration: 0s; transform: translate3d(-7860px, 0px, 0px);" class="banner_bxslider"><li class="bx-clone" style="float: left; list-style: outside none none; position: relative; width: 1310px;">
            	<img src="assets/banner-06.jpg" alt="">
            </li>
        	<li style="float: left; list-style: outside none none; position: relative; width: 1310px;">
            	<img src="assets/banner-01.jpg" alt="">
            </li>
			<li style="float: left; list-style: outside none none; position: relative; width: 1310px;">
            	<img src="assets/banner-02.jpg" alt="">
            </li>
			<li style="float: left; list-style: outside none none; position: relative; width: 1310px;">
            	<img src="assets/banner-03.jpg" alt="">
            </li>
			<li style="float: left; list-style: outside none none; position: relative; width: 1310px;">
            	<img src="assets/banner-04.jpg" alt="">
            </li>
			<li style="float: left; list-style: outside none none; position: relative; width: 1310px;">
            	<img src="assets/banner-05.jpg" alt="">
            </li>
			<li style="float: left; list-style: outside none none; position: relative; width: 1310px;">
            	<img src="assets/banner-06.jpg" alt="">
            </li>
        <li class="bx-clone" style="float: left; list-style: outside none none; position: relative; width: 1310px;">
            	<img src="assets/banner-01.jpg" alt="">
            </li></ul></div><div class="bx-controls bx-has-pager bx-has-controls-direction"><div class="bx-pager bx-default-pager"><div class="bx-pager-item"><a href="" data-slide-index="0" class="bx-pager-link">1</a></div><div class="bx-pager-item"><a href="" data-slide-index="1" class="bx-pager-link">2</a></div><div class="bx-pager-item"><a href="" data-slide-index="2" class="bx-pager-link">3</a></div><div class="bx-pager-item"><a href="" data-slide-index="3" class="bx-pager-link">4</a></div><div class="bx-pager-item"><a href="" data-slide-index="4" class="bx-pager-link">5</a></div><div class="bx-pager-item"><a href="" data-slide-index="5" class="bx-pager-link active">6</a></div></div><div class="bx-controls-direction"><!--a class="bx-prev" href="">Prev</a><a class="bx-next" href="index.php">Next</a--></div></div></div>
        <!--Schedule Visit Wrap Start-->
        <div class="kf_schedule_wrap">
        	<h3>SABESTIAN VILLA</h3>
            <p>123, NEAR SHAMMER LAKE, NEW YOURK, NY - 10001</p>
            <ul class="kf_schedule_room">
            	<li>03 Bedroom </li>
                <li>03 Bathrooms </li>
                <li>2790 Sq Ft Area </li>
            </ul>
            <div class="kf_schedule_visit">
            	<h4>$1,555,450</h4>
                <a href="#">Schedule Visit</a>
            </div>
        </div>
        <!--Schedule Visit Wrap End-->
    </div>
    <!--Main Banner Wrap End-->
    
    <!--Content Wrap Start-->
    <div class="kf_property_content_wrap">
    	<!--Advance Search Wrap Start-->
        <div class="kf_advance_search_bg">
        	<div class="container">
            	<!--Tab Link Wrap Start-->
            	<div class="kf_search_tab_wrap">
                	<ul data-tabs="tabs">
                    	<li class="active"><a data-toggle="tab" href="#listing">Search</a></li>
                        <li><a data-toggle="tab" href="#rent">For Rent</a></li>
                        <li><a data-toggle="tab" href="#sale">For Sale</a></li>
                        <li><a data-toggle="tab" href="#auction">Auctions</a></li>
                    </ul>
                </div>
                <!--Tab Link Wrap End-->
                
                <!--Search Form Wrap Start-->
                <div class="tab-content">
                	<div class="tab-pane active" id="listing">
                        <div class="kf_advacnce_search_form">
                            <form>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="kf_search_field">
                                            <label>Location</label>
                                            <!--input name="property_id" type="text"-->
                                            <input placeholder="Search by Zip Code, Address or MLS #" spellcheck="false" autocapitalize="off" autocorrect="off" autocomplete="off" class="js-qs-loc ui-autocomplete-input" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Min Price</label>
											<select style="display: none;" sb="97056361" class="chosen-select" data-name="lprice">
												<option value="">Min Price</option>
												<option value="50000">$50,000</option>
												<option value="60000">$60,000</option>
												<option selected="selected" value="70000">$70,000</option>
												<option value="80000">$80,000</option>
												<option value="90000">$90,000</option>
												<option value="100000">$100,000</option>
												<option value="125000">$125,000</option>
												<option value="150000">$150,000</option>
												<option value="175000">$175,000</option>
												<option value="200000">$200,000</option>
												<option value="225000">$225,000</option>
												<option value="250000">$250,000</option>
												<option value="275000">$275,000</option>
												<option value="300000">$300,000</option>
												<option value="325000">$325,000</option>
												<option value="350000">$350,000</option>
												<option value="375000">$375,000</option>
												<option value="400000">$400,000</option>
												<option value="425000">$425,000</option>
												<option value="450000">$450,000</option>
												<option value="475000">$475,000</option>
												<option value="500000">$500,000</option>
												<option value="550000">$550,000</option>
												<option value="600000">$600,000</option>
												<option value="650000">$650,000</option>
												<option value="700000">$700,000</option>
												<option value="750000">$750,000</option>
												<option value="800000">$800,000</option>
												<option value="850000">$850,000</option>
												<option value="900000">$900,000</option>
												<option value="950000">$950,000</option>
												<option value="1000000">$1,000,000</option>
												<option value="1250000">$1,250,000</option>
												<option value="1500000">$1,500,000</option>
												<option value="1750000">$1,750,000</option>
												<option value="2000000">$2,000,000</option>
												<option value="2250000">$2,250,000</option>
												<option value="2500000">$2,500,000</option>
												<option value="2750000">$2,750,000</option>
												<option value="3000000">$3,000,000</option>
												<option value="3250000">$3,250,000</option>
												<option value="3500000">$3,500,000</option>
												<option value="3750000">$3,750,000</option>
												<option value="4000000">$4,000,000</option>
												<option value="4250000">$4,250,000</option>
												<option value="4500000">$4,500,000</option>
												<option value="4750000">$4,750,000</option>
												<option value="5000000">$5,000,000</option>
												<option value="5500000">$5,500,000</option>
												<option value="6000000">$6,000,000</option>
												<option value="6500000">$6,500,000</option>
												<option value="7000000">$7,000,000</option>
												<option value="7500000">$7,500,000</option>
												<option value="8000000">$8,000,000</option>
												<option value="8500000">$8,500,000</option>
												<option value="9000000">$9,000,000</option>
												<option value="9500000">$9,500,000</option>
												<option value="10000000">$10,000,000</option>
												<option value="11000000">$11,000,000</option>
												<option value="12000000">$12,000,000</option>
												<option value="13000000">$13,000,000</option>
												<option value="14000000">$14,000,000</option>
												<option value="15000000">$15,000,000</option>
												<option value="16000000">$16,000,000</option>
												<option value="17000000">$17,000,000</option>
												<option value="18000000">$18,000,000</option>
												<option value="19000000">$19,000,000</option>
												<option value="20000000">$20,000,000</option>
											</select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Max Price</label>
											<select style="display: none;" sb="3904622" class="chosen-select" data-name="uprice">
												<option value="">Max Price</option>
												<option value="50000">$50,000</option>
												<option value="60000">$60,000</option>
												<option value="70000">$70,000</option>
												<option value="80000">$80,000</option>
												<option selected="selected" value="90000">$90,000</option>
												<option value="100000">$100,000</option>
												<option value="125000">$125,000</option>
												<option value="150000">$150,000</option>
												<option value="175000">$175,000</option>
												<option value="200000">$200,000</option>
												<option value="225000">$225,000</option>
												<option value="250000">$250,000</option>
												<option value="275000">$275,000</option>
												<option value="300000">$300,000</option>
												<option value="325000">$325,000</option>
												<option value="350000">$350,000</option>
												<option value="375000">$375,000</option>
												<option value="400000">$400,000</option>
												<option value="425000">$425,000</option>
												<option value="450000">$450,000</option>
												<option value="475000">$475,000</option>
												<option value="500000">$500,000</option>
												<option value="550000">$550,000</option>
												<option value="600000">$600,000</option>
												<option value="650000">$650,000</option>
												<option value="700000">$700,000</option>
												<option value="750000">$750,000</option>
												<option value="800000">$800,000</option>
												<option value="850000">$850,000</option>
												<option value="900000">$900,000</option>
												<option value="950000">$950,000</option>
												<option value="1000000">$1,000,000</option>
												<option value="1250000">$1,250,000</option>
												<option value="1500000">$1,500,000</option>
												<option value="1750000">$1,750,000</option>
												<option value="2000000">$2,000,000</option>
												<option value="2250000">$2,250,000</option>
												<option value="2500000">$2,500,000</option>
												<option value="2750000">$2,750,000</option>
												<option value="3000000">$3,000,000</option>
												<option value="3250000">$3,250,000</option>
												<option value="3500000">$3,500,000</option>
												<option value="3750000">$3,750,000</option>
												<option value="4000000">$4,000,000</option>
												<option value="4250000">$4,250,000</option>
												<option value="4500000">$4,500,000</option>
												<option value="4750000">$4,750,000</option>
												<option value="5000000">$5,000,000</option>
												<option value="5500000">$5,500,000</option>
												<option value="6000000">$6,000,000</option>
												<option value="6500000">$6,500,000</option>
												<option value="7000000">$7,000,000</option>
												<option value="7500000">$7,500,000</option>
												<option value="8000000">$8,000,000</option>
												<option value="8500000">$8,500,000</option>
												<option value="9000000">$9,000,000</option>
												<option value="9500000">$9,500,000</option>
												<option value="10000000">$10,000,000</option>
												<option value="11000000">$11,000,000</option>
												<option value="12000000">$12,000,000</option>
												<option value="13000000">$13,000,000</option>
												<option value="14000000">$14,000,000</option>
												<option value="15000000">$15,000,000</option>
												<option value="16000000">$16,000,000</option>
												<option value="17000000">$17,000,000</option>
												<option value="18000000">$18,000,000</option>
												<option value="19000000">$19,000,000</option>
												<option value="20000000">$20,000,000</option>
											</select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Beds</label>
											<select style="display: none;" sb="81264612" class="chosen-select" data-name="lbeds">
												<option value="">Beds</option>
												<option value="1">1+</option>
												<option selected="selected" value="2">2+</option>
												<option value="3">3+</option>
												<option value="4">4+</option>
												<option value="5">5+</option>
												<option value="6">6+</option>
											</select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Baths</label>
											<select style="display: none;" sb="16590636" class="chosen-select" data-name="lbaths">
												<option value="">Baths</option>
												<option value="1">1+</option>
												<option value="2">2+</option>
												<option selected="selected" value="3">3+</option>
												<option value="4">4+</option>
												<option value="5">5+</option>
												<option value="6">6+</option>
											</select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Property type</label>
                                            <select style="display: none;" class="chosen-select">
                                                <option selected="selected">Any</option>
                                                <option>Commercial</option>
                                                <option>-Office</option>
												<option>-Shop</option>
                                                <option>-Residential</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Square Feet</label>
   											<select style="display: none;" sb="19377466" class="chosen-select" data-name="lsqft">
												<option value="">Square Feet</option>
												<option value="500">500+</option>
												<option value="1000">1000+</option>
												<option value="1500">1500+</option>
												<option selected="selected" value="2000">2000+</option>
												<option value="2500">2500+</option>
												<option value="3000">3000+</option>
												<option value="3500">3500+</option>
												<option value="4000">4000+</option>
												<option value="4500">4500+</option>
												<option value="5000">5000+</option>
												<option value="5500">5500+</option>
												<option value="6000">6000+</option>
												<option value="6500">6500+</option>
												<option value="7000">7000+</option>
											</select>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
										  <div class="col-md-3 col-sm-6"><label><input  name="daysonmarket" value="14" type="checkbox"> <span>New to Market</span></label></div>
										  <div class="col-md-3 col-sm-6"><label><input  name="interior" value="pets allowed" type="checkbox"> <span>Pet Friendly</span></label></div>
										  <div class="col-md-3 col-sm-6"><label><input  name="pricepctchng" value="-.10" type="checkbox"> <span>Price Reductions</span></label></div>
										  <div class="col-md-3 col-sm-6"><label><input  name="reo" value="true" type="checkbox"> <span>Distressed</span></label></div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <input value="List Results" type="submit">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <input value="Map Results" type="submit">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6" style="margin-top:40px;">
                                        <div class="kf_search_field">
                                            <a href="#" style="color:#0070BB;text-decoration:underline;">More Search options</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                    <div class="tab-pane" id="rent">
                    	<div class="kf_advacnce_search_form">
                            <form>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Keyword</label>
                                            <input name="property_id" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Property ID</label>
                                            <select style="display: none;" class="chosen-select">
                                                <option selected="selected">Any</option>
                                                <option>Option 2</option>
                                                <option>Option 3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Location</label>
                                            <select style="display: none;" class="chosen-select">
                                                <option selected="selected">Any</option>
                                                <option>Miami</option>
                                                <option>-Little Havana</option>
												<option>-Perrine</option>
                                                <option>-Doral</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Property Status</label>
                                            <select style="display: none;" class="chosen-select">
                                                <option selected="selected">Any</option>
                                                <option>For Rent</option>
                                                <option>For Rent</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Min Beds</label>
                                            <select style="display: none;" class="chosen-select">
                                                <option selected="selected">Any</option>
                                                <option>Commercial</option>
                                                <option>-Office</option>
												<option>-Shop</option>
                                                <option>-Residential</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Min Baths</label>
                                            <select style="display: none;" class="chosen-select">
                                                <option selected="selected">Any</option>
                                                <option>2</option>
                                                <option>3</option>
												<option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <!--div class="kf_search_field kf_range_slider">
                                            <label>Price Range</label>
                                            <input value="$50 - $450" class="amount" readonly="readonly" type="text">
                                            <div class="kf_range_slider">
                                                <span>Min</span>
                                                <div class="slider-range ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div style="left: 10%; width: 80%;" class="ui-slider-range ui-widget-header ui-corner-all"></div><span style="left: 10%;" class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"></span><span style="left: 90%;" class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"></span></div>
                                                <span>Max</span>
                                            </div>
                                        </div-->
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">

                                            <input value="Search Property" type="submit">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                    <div class="tab-pane" id="sale">
                    	<div class="kf_advacnce_search_form">
                            <form>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Keyword</label>
                                            <input name="property_id" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Property ID</label>
                                            <select style="display: none;" class="chosen-select">
                                                <option selected="selected">Any</option>
                                                <option>Option 2</option>
                                                <option>Option 3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Location</label>
                                            <select style="display: none;" class="chosen-select">
                                                <option selected="selected">Any</option>
                                                <option>Miami</option>
                                                <option>-Little Havana</option>
												<option>-Perrine</option>
                                                <option>-Doral</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Property Status</label>
                                            <select style="display: none;" class="chosen-select">
                                                <option selected="selected">Any</option>
                                                <option>For Rent</option>
                                                <option>For Rent</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Min Beds</label>
                                            <select style="display: none;" class="chosen-select">
                                                <option selected="selected">Any</option>
                                                <option>Commercial</option>
                                                <option>-Office</option>
												<option>-Shop</option>
                                                <option>-Residential</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Min Beds</label>
                                            <select style="display: none;" class="chosen-select">
                                                <option selected="selected">Any</option>
                                                <option>2</option>
                                                <option>3</option>
												<option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <!--div class="kf_search_field kf_range_slider">
                                            <label>Price Range</label>
                                            <input value="$50 - $450" class="amount" readonly="readonly" type="text">
                                            <div class="kf_range_slider">
                                                <span>Min</span>
                                                <div class="slider-range ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div style="left: 10%; width: 80%;" class="ui-slider-range ui-widget-header ui-corner-all"></div><span style="left: 10%;" class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"></span><span style="left: 90%;" class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"></span></div>
                                                <span>Max</span>
                                            </div>
                                        </div-->
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <input value="Search Property" type="submit">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                    <div class="tab-pane" id="auction">
                    	<div class="kf_advacnce_search_form">
                            <form>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Keyword</label>
                                            <input name="property_id" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Property ID</label>
                                            <select style="display: none;" class="chosen-select">
                                                <option selected="selected">Any</option>
                                                <option>Option 2</option>
                                                <option>Option 3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Location</label>
                                            <select style="display: none;" class="chosen-select">
                                                <option selected="selected">Any</option>
                                                <option>Miami</option>
                                                <option>-Little Havana</option>
												<option>-Perrine</option>
                                                <option>-Doral</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Property Status</label>
                                            <select style="display: none;" class="chosen-select">
                                                <option selected="selected">Any</option>
                                                <option>For Rent</option>
                                                <option>For Rent</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Property Type</label>
                                            <select style="display: none;" class="chosen-select">
                                                <option selected="selected">Any</option>
                                                <option>Commercial</option>
                                                <option>-Office</option>
												<option>-Shop</option>
                                                <option>-Residential</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <label>Min Beds</label>
                                            <select style="display: none;" class="chosen-select">
                                                <option selected="selected">Any</option>
                                                <option>2</option>
                                                <option>3</option>
												<option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <!--div class="kf_search_field kf_range_slider">
                                            <label>Price Range</label>
                                            <input value="$50 - $450" class="amount" readonly="readonly" type="text">
                                            <div class="kf_range_slider">
                                                <span>Min</span>
                                                <div class="slider-range ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div style="left: 10%; width: 80%;" class="ui-slider-range ui-widget-header ui-corner-all"></div><span style="left: 10%;" class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"></span><span style="left: 90%;" class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"></span></div>
                                                <span>Max</span>
                                            </div>
                                        </div-->
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="kf_search_field">
                                            <input value="Search Property" type="submit">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--Search Form Wrap End-->
            </div>
        </div>
        <!--Advance Search Wrap Start-->
        
        <!--Services Wrap Start-->
        <!--section class="kf_home_services_bg">
        	<div class="container">
                <!--Heading Wrap Start-->
                <!--div class="kf_heading_1">
                    <h3>PROPERTY Services</h3>
                    <p>Lorem Ipsum orem  simply dummy </p>
                    <span class="kf_property_line"></span>
                </div-->
                <!--Heading Wrap End-->
                
                <!--Services List Wrap Start-->
                <!--div class="row">
                    <div class="col-md-3">
                        <div class="kf-action7_content">
                            <a href="#"><i class="icon-dollar"></i></a>
                            <h6>HOMES FOR SALE</h6>
                            <p>Sed turpis mi, porttitor nec congue tempus, posuere nec sapien. Mauris laoreet   </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="kf-action7_content">
                            <a href="#"><i class="icon-business"></i></a>
                            <h6>HOUSE FOR Rent</h6>
                            <p>Aliquam dui justo, feugiat eu ligula at, posuere cursus enim. Fusce dictum egestas sem eu convallis.  </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="kf-action7_content">
                            <a href="#"><i class="icon-apartment"></i></a>
                            <h6>Best Price In Market</h6>
                            <p>Curabitur non mi id elit vestibulum posuere. Nunc in risus arcu. Quisque quis risus vitae augue   </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="kf-action7_content">
                            <a href="#"><i class="icon-office"></i></a>
                            <h6>Hottest Property List</h6>
                            <p>Curabitur non mi id elit vestibulum posuere. Nunc in risus arcu. Quisque quis risus   </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="kf-action7_content">
                            <a href="#"><i class="icon-icon-1203"></i></a>
                            <h6>Make Money</h6>
                            <p>Etiam non libero vitae augue varius bibendum vel quis felis. Sed nec arcu nulla. </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="kf-action7_content">
                            <a href="#"><i class="icon-money-1"></i></a>
                            <h6>Price Analysis</h6>
                            <p>A wonderful serenity has taken possession of my entire soul,which I enjoy with my whole heart.  </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="kf-action7_content">
                            <a href="#"><i class="icon-people"></i></a>
                            <h6>Property Management</h6>
                            <p>The carry-cost rule is used by banks to evaluate. It gives the maximum percentage of a borrower's  </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="kf-action7_content">
                            <a href="#"><i class="icon-money"></i></a>
                            <h6>Matching Buyer</h6>
                            <p>Proin gravida nibh vel velit auctor aliquet. Aene. Proin gravida nibh vel velit auctor aliquet  </p>
                        </div>
                    </div>
                </div-->
                <!--Services List Wrap End-->
            <!--/div>
        </section-->
        <!--Services Wrap Start-->
        
        <!--Properting Listing Wrap Start-->
        <section class="kf_property_listing_bg">
        	<div class="container">
            	<!--Heading Wrap Start-->
            	<div class="kf_heading_1">
                    <h3>LISTINGS</h3>
                    <p>Lorem Ipsum orem  simply dummy </p>
                    <span class="kf_property_line"></span>
                    <div class="kf_search_field text-right" >
                       <a href="grid_view_all.php" class="button" style="float:right;">See all listings</a>
                    </div>
                </div>
                <!--Heading Wrap End-->
                
                <!--Property Listing Wrap Start-->
                <div class="row">
                	<div class="col-md-4 col-sm-6">
                    	<div class="kf_property_listing_wrap">
                        	<figure>
                            	<img src="assets/listing-01.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                	<span class="kf_listing_overlay"></span>
                                	<a class="kf_md_btn kf_link_1" href="property_details.php">View Detail</a>
                                </figcaption>
                            </figure>
                            <div class="kf_property_listing_des">
                            	<h5><a href="#">Bill Tower, Survey, NJ</a></h5>
                                <p>Aliquam vitae sem id sem efficitur interdum. Phasellus ut nulla nisi. Sed varius… </p>
                                <div class="kf_listing_total_price">
                                	<h4>$5,505</h4>
                                </div>
                                <ul>
                                	<li>3 Room</li>
                                    <li>1 Bath</li>
                                    <li>2 Beds</li>
                                    <li>10 Sqft</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_property_listing_wrap">
                        	<figure>
                            	<img src="assets/listing-02.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                	<span class="kf_listing_overlay"></span>
                                	<a class="kf_md_btn kf_link_1" href="property_details.php">View Detail</a>
                                </figcaption>
                            </figure>
                            <div class="kf_property_listing_des">
                            	<h5><a href="#">Florida 5, Pinecrest, FL</a></h5>
                                <p>The bedding was hardly able to cover it and seemed ready to slide off… </p>
                                <div class="kf_listing_total_price">
                                	<h4>$195,000</h4>
                                </div>
                                <ul>
                                	<li>4 Room</li>
                                    <li>3 Bath</li>
                                    <li>2 Beds</li>
                                    <li>84 Sqft</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_property_listing_wrap">
                        	<figure>
                            	<img src="assets/listing-03.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                	<span class="kf_listing_overlay"></span>
                                	<a class="kf_md_btn kf_link_1" href="property_details.php">View Detail</a>
                                </figcaption>
                            </figure>
                            <div class="kf_property_listing_des">
                            	<h5><a href="#">Southwest Croase, CA</a></h5>
                                <p>Look at our Latest listed properties and check out the facilities on them, We… </p>
                                <div class="kf_listing_total_price">
                                	<h4>$250,199</h4>
                                </div>
                                <ul>
                                	<li>8 Room</li>
                                    <li>5 Bath</li>
                                    <li>10 Beds</li>
                                    <li>20 Sqft</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_property_listing_wrap">
                        	<figure>
                            	<img src="assets/listing-04.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                	<span class="kf_listing_overlay"></span>
                                	<a class="kf_md_btn kf_link_1" href="property_details.php">View Detail</a>
                                </figcaption>
                            </figure>
                            <div class="kf_property_listing_des">
                            	<h5><a href="#">5359 Kensington, Colorado Rd</a></h5>
                                <p>Many desktop publishing fact thagr or reader will distracted packages and … </p>
                                <div class="kf_listing_total_price">
                                	<h4>$ 2,500</h4>
                                </div>
                                <ul>
                                	<li>3 Room</li>
                                    <li>1 Bath</li>
                                    <li>2 Beds</li>
                                    <li>10 Sqft</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_property_listing_wrap">
                        	<figure>
                            	<img src="assets/listing-05.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                	<span class="kf_listing_overlay"></span>
                                	<a class="kf_md_btn kf_link_1" href="property_details.php">View Detail</a>
                                </figcaption>
                            </figure>
                            <div class="kf_property_listing_des">
                            	<h5><a href="#">120 Biscayne Rd, Miami</a></h5>
                                <p>He lay on his armour-like back, and if he lifted his head a little he could see… </p>
                                <div class="kf_listing_total_price">
                                	<h4>$125,799</h4>
                                </div>
                                <ul>
                                	<li>6 Room</li>
                                    <li>2 Bath</li>
                                    <li>4 Beds</li>
                                    <li>15 Sqft</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_property_listing_wrap">
                        	<figure>
                            	<img src="assets/listing-06.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                	<span class="kf_listing_overlay"></span>
                                	<a class="kf_md_btn kf_link_1" href="property_details.php">View Detail</a>
                                </figcaption>
                            </figure>
                            <div class="kf_property_listing_des">
                            	<h5><a href="#">Avenue Boulevard, Coral</a></h5>
                                <p>One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed… </p>
                                <div class="kf_listing_total_price">
                                	<h4>$990,985</h4>
                                </div>
                                <ul>
                                	<li>7 Room</li>
                                    <li>3 Bath</li>
                                    <li>4 Beds</li>
                                    <li>30 Sqft</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Property Listing Wrap End-->               
            </div>
        </section>
        <!--Properting Listing Wrap End-->
        
        <!--Watch The Video Wrap Start-->
        <!--section class="kf_property_video_bg">
            <div class="kf_video_wrap">
                <div class="kf_play_btn">
					<a data-rel="prettyPhoto" href="http://www.youtube.com/watch?v=kh29_SERH0Y?rel=0"><i class="fa fa-play"></i></a>
				</div>
                <h2>Watch the video</h2>
                <p>Check Out Our Video Presentation</p>
                <a href="#">Veiw  Video <i class="fa fa-caret-right"></i></a>
            </div>
        </section-->
        <!--Watch The Video Wrap End-->
        
        <!--Most Recent Property Wrap Start-->
        <section class="kf_recent_property_bg">
        	<div class="container">
            	<!--Heading Wrap Start-->
            	<div class="kf_heading_1">
                    <h3>Most Recent </h3>
                    <p>Lorem Ipsum orem  simply dummy </p>
                    <span class="kf_property_line"></span>
                </div>
                <!--Heading Wrap End-->
                
                <!--Most Recent Property List Wrap Start-->
                <div class="row">
                	<div class="col-md-4 col-sm-6">
                    	<div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-01.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$2,200</h5>
                                <p>87 Weepingwood #33, Irvine, CA</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>One morning, when Gregor Samsa woke from troubled dreams,He lay on his armour-like back, and if he found himself transformed…</p>
                                
                                <ul class="kf_recent_rating">
                                	<li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star-half-full"></i></a></li>
                                </ul>
                                
                                <div class="kf_recent_property_meta">
                                	<ul>
                                    	<li>
                                        	<i class="fa fa-list"></i>
                                            <div class="kf_recent_meta_des">
                                            	<p>Listing ID: 1234</p>
                                            </div>
                                        </li>
                                        
                                        <li>
                                        	<i class="fa fa-map-marker"></i>
                                            <div class="kf_recent_meta_des">
                                            	<p>Loaction: Clooney Road, London</p>
                                            </div>
                                        </li>
                                        
                                        <li>
                                        	<i class="fa fa-dollar"></i>
                                            <div class="kf_recent_meta_des">
                                            	<p>Type: For Sale</p>
                                            </div>
                                        </li>
                                        
                                        <li>
                                        	<i class="fa fa-bed"></i>
                                            <div class="kf_recent_meta_des">
                                            	<p>Bedrooms : 6</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-02.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$ 170,000,000</h5>
                                <p>2139 Anniversary Ln, Newport Beach</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                        
                        <div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-02.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$ 10,000,000</h5>
                                <p>34 Morena #17, Irvine, CA</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>Lorem Ipsum.when Gregor Samsa Proin gravida nibh vel velit auctor aliquet...</p>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                    	<div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-03.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$ 150,000,000</h5>
                                <p>635 W 42nd St New York</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>Lorem Ipsum. Proin gravida nibh vel 
velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, 
nisi elit consequat ipsum..</p>
                                
                                <ul class="kf_recent_rating">
                                	<li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star-half-full"></i></a></li>
                                </ul>
                                
                                <div class="kf_recent_property_meta">
                                	<ul>
                                    	<li>
                                        	<i class="fa fa-list"></i>
                                            <div class="kf_recent_meta_des">
                                            	<p>Listing ID: 1234</p>
                                            </div>
                                        </li>
                                        
                                        <li>
                                        	<i class="fa fa-map-marker"></i>
                                            <div class="kf_recent_meta_des">
                                            	<p>Loaction: Clooney Road, London</p>
                                            </div>
                                        </li>
                                        
                                        <li>
                                        	<i class="fa fa-dollar"></i>
                                            <div class="kf_recent_meta_des">
                                            	<p>Type: For Sale</p>
                                            </div>
                                        </li>
                                        
                                        <li>
                                        	<i class="fa fa-bed"></i>
                                            <div class="kf_recent_meta_des">
                                            	<p>Bedrooms : 6</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Most Recent Property List Wrap End-->
                
            </div>
        </section>
        <!--Most Recent Property Wrap End-->
        
        <!--Leased And Sold Property Wrap Start-->
        <div class="kf_leased_property_bg">
            <div class="kf_leased_property">
                <!--h2>105</h2>
                <span class="kf_property_line white_color"></span>
                <h3>Leased Properties</h3>
                <a class="kf_sm_btn kf_link_1" href="#">View All</a-->
<div class="page-content">
                                    
	<div id="pageComponent5188">

      <div class="si-content-area">
<h5 style="color:#fff;text-align:justify;">Boca Raton Condos For Sale</h5>
<p style="color:#fff;text-align:justify;">BocaLuxuryCondos.com offers user friendly tools which are exclusive to searching the nearly 900 Boca Raton condos for sale. Easy navigation allows you to browse through a wide variety of Boca Raton condos while being educated on the different choices available including descriptions of over 60 oceanfront, waterfront, downtown or pet friendly buildings. You will also find guidance on the selling and buying process of a condo, current market statistics and the latest news about the condo market.</p>
<h5 style="color:#fff;text-align:justify;">SEARCHING FOR A BOCA RATON CONDO?</h5>
<p style="color:#fff;text-align:justify;">Throughout Boca Raton, you will find a wide array of condo types in all different price ranges. Most of the downtown condos offer concierge services &amp; valet parking such as 200 East Boca Raton &amp; Townsend Place.</p>
<p style="color:#fff;text-align:justify;">The oceanfront condos for sale in Boca Raton can vary greatly in price. The Addison has condos in the multi-millions while Chalfonte's prices range from the low $400,000 to just under a million.</p>
<p style="color:#fff;text-align:justify;">Owning a Boca Raton condo entitles you to endless amenities illustrating the dream of the South Florida lifestyle. Enjoy our website and remember that the luxury real estate specialists of Champagne &amp; Parisi are available for any of your Boca Raton condo needs.</p>
      </div>

    </div>

                                </div>
            </div>
            
            <div class="kf_leased_property ">
                <!--h2>105</h2>
                <span class="kf_property_line white_color"></span>
                <h3>Sold Properties</h3>
                <a class="kf_sm_btn kf_link_1" href="#">View All</a-->
                <div class="square square--trends">
  <div class="square__shell">
    <div class="square__title square__title--sans">Boca Raton Trends</div>

    <div class="square__content">
      <div class="market-trends">
        <div class="market-trends__title">Market Snapshot</div>

        <div class="market-trends__item">
          <div>Total Properties Listed: <b>843</b></div>
          <div>Month-Over-Month Change: <span>-1.40%</span></div>
        </div>

        <div class="market-trends__item">
          <div>Average Days On Market: <b>116</b></div>
          
          <div><a href="#" style="color:#fff;">New Listings Today: <b>9</b></a></div>
          
        </div>

        <div class="market-trends__item">
          <div>Median List Price: <b>$199,000</b></div>
          <div>Month-Over-Month Change: <span>-0.45%</span></div>
        </div>

        <div class="market-trends__title">Typical Property</div>

        <div class="market-trends__item">
          <table>
            <tbody><tr>
              <td>Condominium:</td>
              <td>
                <b>2.19 Beds</b><b>2.35 Baths</b>
              </td>
            </tr>

            <tr>
              <td>Avg List Price:</td>
              <td><b>$436,281</b></td>
            </tr>

            <tr>
              <td>Avg Price per SqFt:</td>
              <td><b>$221</b></td>
            </tr>
            
            <tr>
              <td>Avg Price Reduction:</td>
              <td><b>-4.60%</b></td>
            </tr>
          </tbody></table>
        </div>

        <div class="market-trends__title">Average Price by Bedrooms</div>
      
        <div class="market-trends__item">
          <table>
          
            <tbody><tr>
              <td>1 Bedroom:</td>
              <td><b>$117,308</b></td>
            </tr>
          
            <tr>
              <td>2 Bedrooms:</td>
              <td><b>$252,823</b></td>
            </tr>
          
            <tr>
              <td>3 Bedrooms:</td>
              <td><b>$777,534</b></td>
            </tr>
          
            <tr>
              <td>4 Bedrooms:</td>
              <td><b>$3,489,818</b></td>
            </tr>
          
            <tr>
              <td>5 Bedrooms:</td>
              <td><b>$5,386,667</b></td>
            </tr>
          
          </tbody></table>
        </div>
      
        <div class="market-trends__control">
          <a href="#square__title  js-qs-count" type="button" class="button">view more market statistics</a>
        </div>
      </div>
    </div>
  </div>
</div>
            </div>
            
        </div>
        <!--Leased And Sold Property Wrap End-->
        
        <!--Property For Rent Wrap Start-->
        <!--section>
        	<div class="container">
            	<!--Heading Wrap Start-->
            	<!--div class="kf_heading_1">
                    <h3>Property For Rent</h3>
                    <p>Lorem Ipsum orem  simply dummy </p>
                    <span class="kf_property_line"></span>
                </div>
                <!--Heading Wrap End-->
                
                <!--Property For rent List Start-->
                <!--div style="opacity: 1; display: block;" class="kf_rent_property owl-carousel owl-theme">
                	<div class="owl-wrapper-outer"><div style="width: 5700px; left: 0px; display: block; transition: all 800ms ease 0s; transform: translate3d(-1710px, 0px, 0px);" class="owl-wrapper"><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_property_rent_wrap">
                            <figure>
                                <img src="assets/rent-01.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                    <span class="kf_listing_overlay"></span>
                                    <a class="kf_md_btn kf_link_1" href="#">View Detail</a>
                                    <div class="kf_rent_label">
                                        <p>For Rent</p>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="kf_rent_property_des">
                                <h6><a href="#">Villa in Hialeah, Dade County</a></h6>
                                <ul>
                                    <li>
                                        <i class="fa fa-bed"></i>
                                        <p>bedroom</p>
                                        <span>9</span>
                                    </li>
                                    <li>
                                        <i class="icon-bath"></i>
                                        <p>Bathroom</p>
                                        <span>6</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-car"></i>
                                        <p>Garage</p>
                                        <span>2</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="kf_rent_location">
                                <h6><i class="fa fa-map-marker"></i>South Dakota</h6>
                                <div class="kf_rent_total_price">
                                    <h6>$2,500</h6>
                                </div>
                            </div>
                        </div>
                    </div></div><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_property_rent_wrap">
                            <figure>
                                <img src="assets/rent-02.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                    <span class="kf_listing_overlay"></span>
                                    <a class="kf_md_btn kf_link_1" href="#">View Detail</a>
                                    <div class="kf_rent_label">
                                        <p>For Sale</p>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="kf_rent_property_des">
                                <h6><a href="#">401 Biscayne Boulevard, Miami</a></h6>
                                <ul>
                                    <li>
                                        <i class="fa fa-bed"></i>
                                        <p>bedroom</p>
                                        <span>5</span>
                                    </li>
                                    <li>
                                        <i class="icon-bath"></i>
                                        <p>Bathroom</p>
                                        <span>4</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-car"></i>
                                        <p>Garage</p>
                                        <span>2</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="kf_rent_location">
                                <h6><i class="fa fa-map-marker"></i>Denman, London</h6>
                                <div class="kf_rent_total_price">
                                    <h6>$3,500</h6>
                                </div>
                            </div>
                        </div>
                    </div></div><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_property_rent_wrap">
                            <figure>
                                <img src="assets/rent-03.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                    <span class="kf_listing_overlay"></span>
                                    <a class="kf_md_btn kf_link_1" href="#">View Detail</a>
                                    <div class="kf_rent_label">
                                        <p>For Rent</p>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="kf_rent_property_des">
                                <h6><a href="#">1200 Anastasia Avenue, Coral Gables</a></h6>
                                <ul>
                                    <li>
                                        <i class="fa fa-bed"></i>
                                        <p>bedroom</p>
                                        <span>5</span>
                                    </li>
                                    <li>
                                        <i class="icon-bath"></i>
                                        <p>Bathroom</p>
                                        <span>4</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-car"></i>
                                        <p>Garage</p>
                                        <span>1</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="kf_rent_location">
                                <h6><i class="fa fa-map-marker"></i>United Kingdom</h6>
                                <div class="kf_rent_total_price">
                                    <h6>$6,600</h6>
                                </div>
                            </div>
                        </div>
                    </div></div><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_property_rent_wrap">
                            <figure>
                                <img src="assets/rent-04.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                    <span class="kf_listing_overlay"></span>
                                    <a class="kf_md_btn kf_link_1" href="#">View Detail</a>
                                    <div class="kf_rent_label">
                                        <p>For Sale</p>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="kf_rent_property_des">
                                <h6><a href="#">15421 Southwest 39th Terrace</a></h6>
                                <ul>
                                    <li>
                                        <i class="fa fa-bed"></i>
                                        <p>bedroom</p>
                                        <span>3</span>
                                    </li>
                                    <li>
                                        <i class="icon-bath"></i>
                                        <p>Bathroom</p>
                                        <span>1</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-car"></i>
                                        <p>Garage</p>
                                        <span>2</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="kf_rent_location">
                                <h6><i class="fa fa-map-marker"></i>South Aylesford</h6>
                                <div class="kf_rent_total_price">
                                    <h6>$5,800</h6>
                                </div>
                            </div>
                        </div>
                    </div></div><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_property_rent_wrap">
                            <figure>
                                <img src="assets/rent-05.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                    <span class="kf_listing_overlay"></span>
                                    <a class="kf_md_btn kf_link_1" href="#">View Detail</a>
                                    <div class="kf_rent_label">
                                        <p>For Rent</p>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="kf_rent_property_des">
                                <h6><a href="#">Sabestian Villa, NY $259 / WK</a></h6>
                                <ul>
                                    <li>
                                        <i class="fa fa-bed"></i>
                                        <p>bedroom</p>
                                        <span>5</span>
                                    </li>
                                    <li>
                                        <i class="icon-bath"></i>
                                        <p>Bathroom</p>
                                        <span>4</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-car"></i>
                                        <p>Garage</p>
                                        <span>2</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="kf_rent_location">
                                <h6><i class="fa fa-map-marker"></i>Braidwood Dakota</h6>
                                <div class="kf_rent_total_price">
                                    <h6>$4,500</h6>
                                </div>
                            </div>
                        </div>
                    </div></div><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_property_rent_wrap">
                            <figure>
                                <img src="assets/rent-06.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                    <span class="kf_listing_overlay"></span>
                                    <a class="kf_md_btn kf_link_1" href="#">View Detail</a>
                                    <div class="kf_rent_label">
                                        <p>For Sale</p>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="kf_rent_property_des">
                                <h6><a href="#">Sabestian WK Detail Villa, NY</a></h6>
                                <ul>
                                    <li>
                                        <i class="fa fa-bed"></i>
                                        <p>bedroom</p>
                                        <span>10</span>
                                    </li>
                                    <li>
                                        <i class="icon-bath"></i>
                                        <p>Bathroom</p>
                                        <span>6</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-car"></i>
                                        <p>Garage</p>
                                        <span>3</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="kf_rent_location">
                                <h6><i class="fa fa-map-marker"></i>Trinity Square</h6>
                                <div class="kf_rent_total_price">
                                    <h6>$7,500</h6>
                                </div>
                            </div>
                        </div>
                    </div></div><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_property_rent_wrap">
                            <figure>
                                <img src="assets/rent-07.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                    <span class="kf_listing_overlay"></span>
                                    <a class="kf_md_btn kf_link_1" href="#">View Detail</a>
                                    <div class="kf_rent_label">
                                        <p>For Rent</p>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="kf_rent_property_des">
                                <h6><a href="#">Sabestian Villa, NY $259 / WK</a></h6>
                                <ul>
                                    <li>
                                        <i class="fa fa-bed"></i>
                                        <p>bedroom</p>
                                        <span>8</span>
                                    </li>
                                    <li>
                                        <i class="icon-bath"></i>
                                        <p>Bathroom</p>
                                        <span>4</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-car"></i>
                                        <p>Garage</p>
                                        <span>2</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="kf_rent_location">
                                <h6><i class="fa fa-map-marker"></i>  Queen's Walk, </h6>
                                <div class="kf_rent_total_price">
                                    <h6>$8,500</h6>
                                </div>
                            </div>
                        </div>
                    </div></div><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_property_rent_wrap">
                            <figure>
                                <img src="assets/rent-08.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                    <span class="kf_listing_overlay"></span>
                                    <a class="kf_md_btn kf_link_1" href="#">View Detail</a>
                                    <div class="kf_rent_label">
                                        <p>For Sale</p>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="kf_rent_property_des">
                                <h6><a href="#">Bill Tower,Detail Survey, NJ</a></h6>
                                <ul>
                                    <li>
                                        <i class="fa fa-bed"></i>
                                        <p>bedroom</p>
                                        <span>5</span>
                                    </li>
                                    <li>
                                        <i class="icon-bath"></i>
                                        <p>Bathroom</p>
                                        <span>3</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-car"></i>
                                        <p>Garage</p>
                                        <span>1</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="kf_rent_location">
                                <h6><i class="fa fa-map-marker"></i>South Dakota</h6>
                                <div class="kf_rent_total_price">
                                    <h6>$4,000</h6>
                                </div>
                            </div>
                        </div>
                    </div></div><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_property_rent_wrap">
                            <figure>
                                <img src="assets/rent-09.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                    <span class="kf_listing_overlay"></span>
                                    <a class="kf_md_btn kf_link_1" href="#">View Detail</a>
                                    <div class="kf_rent_label">
                                        <p>For Rent</p>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="kf_rent_property_des">
                                <h6><a href="#">Florida 5, Detail Pinecrest, FL</a></h6>
                                <ul>
                                    <li>
                                        <i class="fa fa-bed"></i>
                                        <p>bedroom</p>
                                        <span>8</span>
                                    </li>
                                    <li>
                                        <i class="icon-bath"></i>
                                        <p>Bathroom</p>
                                        <span>5</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-car"></i>
                                        <p>Garage</p>
                                        <span>3</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="kf_rent_location">
                                <h6><i class="fa fa-map-marker"></i>34, Irvine, CA</h6>
                                <div class="kf_rent_total_price">
                                    <h6>$3,500</h6>
                                </div>
                            </div>
                        </div>
                    </div></div><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_property_rent_wrap">
                            <figure>
                                <img src="assets/rent-01.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                    <span class="kf_listing_overlay"></span>
                                    <a class="kf_md_btn kf_link_1" href="#">View Detail</a>
                                    <div class="kf_rent_label">
                                        <p>For Sale</p>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="kf_rent_property_des">
                                <h6><a href="#">259 Dream in Charing Detail</a></h6>
                                <ul>
                                    <li>
                                        <i class="fa fa-bed"></i>
                                        <p>bedroom</p>
                                        <span>5</span>
                                    </li>
                                    <li>
                                        <i class="icon-bath"></i>
                                        <p>Bathroom</p>
                                        <span>3</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-car"></i>
                                        <p>Garage</p>
                                        <span>2</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="kf_rent_location">
                                <h6><i class="fa fa-map-marker"></i>South Dakota</h6>
                                <div class="kf_rent_total_price">
                                    <h6>$5,600</h6>
                                </div>
                            </div>
                        </div>
                    </div></div></div></div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                <div class="owl-controls clickable"><div class="owl-pagination"><div class="owl-page"><span class=""></span></div><div class="owl-page"><span class=""></span></div><div class="owl-page active"><span class=""></span></div></div><div class="owl-buttons"><div class="owl-prev">prev</div><div class="owl-next">next</div></div></div></div>
                <!--Property For rent List End-->
            <!--/div>
        </section-->
        <!--Property For Rent Wrap End-->
        
        <!--Our Agent Warp Start-->
        <!--section class="kf_our_agent_bg">
        	<div class="container">
            	<!--Heading Wrap Start-->
            	<!--div class="kf_heading_1">
                    <h3>Our Agent</h3>
                    <p>Lorem Ipsum orem  simply dummy </p>
                    <span class="kf_property_line"></span>
                </div>
                <!--Heading Wrap End-->
                
                <!--Agent List Wrap Start-->
                <!--div style="opacity: 1; display: block;" class="kf_agent_slider owl-carousel owl-theme">
                	<div class="owl-wrapper-outer"><div style="width: 3420px; left: 0px; display: block; transition: all 800ms ease 0s; transform: translate3d(-285px, 0px, 0px);" class="owl-wrapper"><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_agent_wrap">
                            <figure>
                                <img src="assets/agent-01.jpg" alt="">
								<figcaption>
									<span class="kf_listing_overlay"></span>
								</figcaption>
                            </figure>
                            <div class="kf_agent_des">
                                <h6><a href="http://kodeforest.net/html/property/team-detail.html">Michael Smart</a></h6>
                                <span class="kf_property_line"></span>
                                <p>Expert Agent</p>
                            </div>
                        </div>
                    </div></div><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_agent_wrap">
                            <figure>
                                <img src="assets/agent-02.jpg" alt="">
								<figcaption>
									<span class="kf_listing_overlay"></span>
								</figcaption>
                            </figure>
                            <div class="kf_agent_des">
                                <h6><a href="http://kodeforest.net/html/property/team-detail.html">Mathew Root</a></h6>
                                <span class="kf_property_line"></span>
                                <p>Area Agent</p>
                            </div>
                        </div>
                    </div></div><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_agent_wrap">
                            <figure>
                                <img src="assets/agent-03.jpg" alt="">
								<figcaption>
									<span class="kf_listing_overlay"></span>
								</figcaption>
                            </figure>
                            <div class="kf_agent_des">
                                <h6><a href="http://kodeforest.net/html/property/team-detail.html">Jane Doe</a></h6>
                                <span class="kf_property_line"></span>
                                <p>Avlar Real Estate</p>
                            </div>
                        </div>
                    </div></div><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_agent_wrap">
                            <figure>
                                <img src="assets/agent-04.jpg" alt="">
								<figcaption>
									<span class="kf_listing_overlay"></span>
								</figcaption>
                            </figure>
                            <div class="kf_agent_des">
                                <h6><a href="http://kodeforest.net/html/property/team-detail.html">Laura Mills</a></h6>
                                <span class="kf_property_line"></span>
                                <p>Expert Agent</p>
                            </div>
                        </div>
                    </div></div><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_agent_wrap">
                            <figure>
                                <img src="assets/agent-05.jpg" alt="">
								<figcaption>
									<span class="kf_listing_overlay"></span>
								</figcaption>
                            </figure>
                            <div class="kf_agent_des">
                                <h6><a href="http://kodeforest.net/html/property/team-detail.html">Chelsea Huntington</a></h6>
                                <span class="kf_property_line"></span>
                                <p>Expert Agent</p>
                            </div>
                        </div>
                    </div></div><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_agent_wrap">
                            <figure>
                                <img src="assets/agent-06.jpg" alt="">
								<figcaption>
									<span class="kf_listing_overlay"></span>
								</figcaption>
                            </figure>
                            <div class="kf_agent_des">
                                <h6><a href="http://kodeforest.net/html/property/team-detail.html">Michael Smart</a></h6>
                                <span class="kf_property_line"></span>
                                <p>Expert Agent</p>
                            </div>
                        </div>
                    </div></div></div></div>
                    
                    
                    
                    
                    
                <div class="owl-controls clickable"><div class="owl-pagination"><div class="owl-page active"><span class=""></span></div><div class="owl-page"><span class=""></span></div></div><div class="owl-buttons"><div class="owl-prev">prev</div><div class="owl-next">next</div></div></div></div>
                <!--Agent List Wrap End-->
                
            <!--/div>
        </section-->
        <!--Our Agent Warp End-->
        
        <!--Number Counter Wrap Start-->
        <!--section class="kf_num_counter_bg">
        	<div class="container">
            	<div class="row">
                	<div class="col-md-3 col-sm-6">
                    	<div class="kf_num_count_wrap">
                        	<i class="fa fa-flag-o"></i>
                            <h2 class="counter">2180</h2>
                            <a href="#">Happy Client</a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                    	<div class="kf_num_count_wrap">
                        	<i class="fa fa-building-o"></i>
                            <h2 class="counter">3300</h2>
                            <a href="#">APARTMENTS</a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                    	<div class="kf_num_count_wrap">
                        	<i class="fa fa-home"></i>
                            <h2 class="counter">2200</h2>
                            <a href="#">HOUSES &amp; vila</a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                    	<div class="kf_num_count_wrap">
                        	<i class="fa fa-user"></i>
                            <h2 class="counter">5000</h2>
                            <a href="#">Professional Agent</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Number Counter Wrap End-->
        
        <!--Latest Blog Post Wrap Start-->
        <!--section>
        	<div class="container">
            	<!--Heading Wrap Start-->
            	<!--div class="kf_heading_1">
                    <h3>Our Agent</h3>
                    <p>Latest Blog Post </p>
                    <span class="kf_property_line"></span>
                </div>
                <!--Heading Wrap End-->
                
                <!--Blog List Wrap Start-->
                <!--div style="opacity: 1; display: block;" class="kf_blog_slider owl-carousel owl-theme">
                	<div class="owl-wrapper-outer"><div style="width: 3800px; left: 0px; display: block; transition: all 800ms ease 0s; transform: translate3d(-380px, 0px, 0px);" class="owl-wrapper"><div style="width: 380px;" class="owl-item"><div class="item">
                        <div class="kf_blog_post_wrap">
                            <h6>.01</h6>
                            <figure>
                                <img src="assets/blog-01.jpg" alt="">
                            </figure>
                            <div class="kf_blog_des">
                                <h6><a href="http://kodeforest.net/html/property/blog-detail.html">University near to sabestian villa</a></h6>
                                <ul class="kf_blog_post_meta">
                                    <li><i class="fa fa-calendar"></i><a href="#">11/05/2009</a></li>
                                    <li><i class="fa fa-user"></i><a href="#">Admin</a></li>
                                    <li><i class="fa fa-comments"></i><a href="#">05</a></li>
                                </ul>
                                <p>Duis facilisis nibh quam, sit amet interdum tellus sollicitudin tempor. Curabitur aliquam…</p>
                                <a class="kf_link_2" href="http://kodeforest.net/html/property/blog-detail.html">Read More</a>
                            </div>
                        </div>
                    </div></div><div style="width: 380px;" class="owl-item"><div class="item">
                        <div class="kf_blog_post_wrap">
                            <h6>.02</h6>
                            <figure>
                                <img src="assets/blog-02.jpg" alt="">
                            </figure>
                            <div class="kf_blog_des">
                                <h6><a href="http://kodeforest.net/html/property/blog-detail.html">LAW FIRM OPENED NEAR TO VILLA</a></h6>
                                <ul class="kf_blog_post_meta">
                                    <li><i class="fa fa-calendar"></i><a href="#">07/03/2010</a></li>
                                    <li><i class="fa fa-user"></i><a href="#">Admin</a></li>
                                    <li><i class="fa fa-comments"></i><a href="#">05</a></li>
                                </ul>
                                <p>Aliquam dui justo, feugiat eu ligula at, posuere cursus enim. Fusce dictum egestas sem eu </p>
                                <a class="kf_link_2" href="http://kodeforest.net/html/property/blog-detail.html">Read More</a>
                            </div>
                        </div>
                    </div></div><div style="width: 380px;" class="owl-item"><div class="item">
                        <div class="kf_blog_post_wrap">
                            <h6>.03</h6>
                            <figure>
                                <img src="assets/blog-03.jpg" alt="">
                            </figure>
                            <div class="kf_blog_des">
                                <h6><a href="http://kodeforest.net/html/property/blog-detail.html">Villa In Hialeah, Dade County</a></h6>
                                <ul class="kf_blog_post_meta">
                                    <li><i class="fa fa-calendar"></i><a href="#">15/02/2014</a></li>
                                    <li><i class="fa fa-user"></i><a href="#">Admin</a></li>
                                    <li><i class="fa fa-comments"></i><a href="#">05</a></li>
                                </ul>
                                <p>Phasellus tincidunt augue id nulla bibendum, non efficitur ante placerat. Donec tortor eros, </p>
                                <a class="kf_link_2" href="http://kodeforest.net/html/property/blog-detail.html">Read More</a>
                            </div>
                        </div>
                    </div></div><div style="width: 380px;" class="owl-item"><div class="item">
                        <div class="kf_blog_post_wrap">
                            <h6>.05</h6>
                            <figure>
                                <img src="assets/blog-04.jpg" alt="">
                            </figure>
                            <div class="kf_blog_des">
                                <h6><a href="http://kodeforest.net/html/property/blog-detail.html">1200 Anastasia Avenue, Coral Gables</a></h6>
                                <ul class="kf_blog_post_meta">
                                    <li><i class="fa fa-calendar"></i><a href="#">13/08/2013</a></li>
                                    <li><i class="fa fa-user"></i><a href="#">Admin</a></li>
                                    <li><i class="fa fa-comments"></i><a href="#">05</a></li>
                                </ul>
                                <p>Cras neque turpis, dictum at tempus a, dapibus sit amet tortor. Interdum et malesuada </p>
                                <a class="kf_link_2" href="http://kodeforest.net/html/property/blog-detail.html">Read More</a>
                            </div>
                        </div>
                    </div></div><div style="width: 380px;" class="owl-item"><div class="item">
                        <div class="kf_blog_post_wrap">
                            <h6>.05</h6>
                            <figure>
                                <img src="assets/blog-02.jpg" alt="">
                            </figure>
                            <div class="kf_blog_des">
                                <h6><a href="http://kodeforest.net/html/property/blog-detail.html">20 Apartments Of Type A</a></h6>
                                <ul class="kf_blog_post_meta">
                                    <li><i class="fa fa-calendar"></i><a href="#">15/08/2015/a&gt;</a></li><a href="#">
                                    </a><li><a href="#"><i class="fa fa-user"></i></a><a href="#">Admin</a></li>
                                    <li><i class="fa fa-comments"></i><a href="#">05</a></li>
                                </ul>
                                <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean elit porta.</p>
                                <a class="kf_link_2" href="http://kodeforest.net/html/property/blog-detail.html">Read More</a>
                            </div>
                        </div>
                    </div></div></div></div>
                    
                    
                    
                    
                <div class="owl-controls clickable"><div class="owl-pagination"><div class="owl-page active"><span class=""></span></div><div class="owl-page"><span class=""></span></div></div><div class="owl-buttons"><div class="owl-prev">prev</div><div class="owl-next">next</div></div></div></div>
                <!--Blog List Wrap End-->
                
            <!--/div>
        </section-->
        <!--Latest Blog Post Wrap End-->
        
    </div>
    <!--Content Wrap End-->
    
    <!--Footer Wrap Start-->
    <!--Map Wrap Start-->
    <div class="kf_property_map_wrap">
        <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d15719.1473036659!2d78.1279358!3d9.9516867!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1471098848002" width="1980" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <!--Map Wrap End-->
    
    <!--Company Wrap Start-->
    <!--div class="kf_property_compnay_bg">
        <div class="container">
            <div style="opacity: 1; display: block;" class="kf_company_slider owl-carousel owl-theme">
                <div class="owl-wrapper-outer"><div style="width: 3192px; left: 0px; display: block; transition: all 800ms ease 0s; transform: translate3d(-228px, 0px, 0px);" class="owl-wrapper"><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-01.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-02.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-03.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-04.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-05.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-01.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-02.jpg" alt=""></a>
                    </div>
                </div></div></div></div>
                
                
                
                
                
                
            <div style="display: block;" class="owl-controls clickable"><div class="owl-pagination"><div class="owl-page active"><span class=""></span></div><div class="owl-page"><span class=""></span></div></div><div class="owl-buttons"><div class="owl-prev">prev</div><div class="owl-next">next</div></div></div></div>
        </div>
    </div-->
    <!--Company Wrap End-->
    
    <!--Twitter Wrap Start-->
    <!--div class="kf_twitter_wrap_bg">
        <div class="container">
            <div class="kf_twitter_wrap">
                <div class="kf_twitter_logo">
                    <i class="fa fa-twitter"></i>
                </div>
                <div class="kf_twitter_des">
                    <div style="max-width: 100%;" class="bx-wrapper"><div style="width: 100%; overflow: hidden; position: relative; height: 34px;" class="bx-viewport"><ul style="width: 515%; position: relative; transition-duration: 0.5s; transform: translate3d(-3876px, 0px, 0px);" class="kf_twitter_slider"><li class="bx-clone" style="float: left; list-style: outside none none; position: relative; width: 969px;">
                            <div class="kf_twitter_msg">
                                <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet, lorem quis bibendum..</p>
                            </div>
                        </li>
                        <li style="float: left; list-style: outside none none; position: relative; width: 969px;">
                            <div class="kf_twitter_msg">
                                <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet, lorem quis bibendum..</p>
                            </div>
                        </li>
                        <li style="float: left; list-style: outside none none; position: relative; width: 969px;">
                            <div class="kf_twitter_msg">
                                <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet, lorem quis bibendum..</p>
                            </div>
                        </li>
                        <li style="float: left; list-style: outside none none; position: relative; width: 969px;">
                            <div class="kf_twitter_msg">
                                <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet, lorem quis bibendum..</p>
                            </div>
                        </li>
                    <li class="bx-clone" style="float: left; list-style: outside none none; position: relative; width: 969px;">
                            <div class="kf_twitter_msg">
                                <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet, lorem quis bibendum..</p>
                            </div>
                        </li></ul></div><div class="bx-controls bx-has-pager bx-has-controls-direction"><div class="bx-pager bx-default-pager"><div class="bx-pager-item"><a href="" data-slide-index="0" class="bx-pager-link active">1</a></div><div class="bx-pager-item"><a href="" data-slide-index="1" class="bx-pager-link">2</a></div><div class="bx-pager-item"><a href="" data-slide-index="2" class="bx-pager-link">3</a></div></div><div class="bx-controls-direction"><a class="bx-prev" href="">Prev</a><a class="bx-next" href="">Next</a></div></div></div>
                </div>
            </div>
        </div>
    </div-->
    <!--Twitter Wrap End-->
    
<?php include('footer.php'); ?>
