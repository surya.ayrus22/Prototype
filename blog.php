   <?php include('header.php');?>
    <!--Sub Banner Wrap Start-->
    <div class="kf_property_sub_banner">
    	<div class="container">
        	<div class="kf_sub_banner_hdg">
            	<h3>Blog</h3>
            </div>
            <div class="kf_property_breadcrumb">
            	<ul>
                	<li><a href="#">Home</a></li>
                    <li><a href="#">Blog</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!--Sub Banner Wrap End-->
    
    <!--Content Wrap Start-->
    <div class="kf_property_content_wrap">
        <!--Blog Listing 2 Wrap Start-->
        <section>
        	<div class="container">
            	<div class="row">
                	<!--Blog Listing 2 Detail Wrap Start-->
                	<div class="col-md-9">
                    	<div class="kf_blog_listing_list">
                        	<div class="kf_listing2_blog_wrap">
                            	<div class="kf_listing2_hdg_wrap">
                                    <div class="kf_blog2_date">
                                        <span>15 Mar</span>
                                    </div>
                                    <div class="kf_listing2_blog">
                                        <ul class="kf_blog_listing_meta">
                                            <li><i class="fa fa-calendar"></i><a href="#">12-08-2016</a></li>
                                            <li><i class="fa fa-user"></i><a href="#">Admin</a></li>
                                            <li><i class="fa fa-comments"></i><a href="#">Comment</a></li>
                                            <li><i class="fa fa-eye"></i><a href="#">View</a></li>
                                            <li><i class="fa fa-heart-o"></i><a href="#">99 + Like</a></li>
                                        </ul>
                                        <h4><a href="#">SPECTR</a></h4>
                                    </div>
                                </div>
                                <div class="kf_listing2_blog_des">
                                	<figure>
                                    	<img src="assets/blog-listing-02.jpg" alt="">
                                    </figure>
                                    <p>The quick, brown fox jumps over a
 lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by 
fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for 
quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs 
jumpy veldt fox. Bright vixens jump; dozy fowl quack. Quick wafting 
zephyrs vex bold Jim.</p>
                                    <a href="#">Continue Reading</a>
                                    <ul class="kf_blog_social_icon">
                                    	<li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            
                            <div class="kf_listing2_blog_wrap">
                            	<div class="kf_listing2_hdg_wrap">
                                    <div class="kf_blog2_date">
                                        <span>18 Feb</span>
                                    </div>
                                    <div class="kf_listing2_blog">
                                        <ul class="kf_blog_listing_meta">
                                            <li><i class="fa fa-calendar"></i><a href="#">12-08-2016</a></li>
                                            <li><i class="fa fa-user"></i><a href="#">Admin</a></li>
                                            <li><i class="fa fa-comments"></i><a href="#">Comment</a></li>
                                            <li><i class="fa fa-eye"></i><a href="#">View</a></li>
                                            <li><i class="fa fa-heart-o"></i><a href="#">99 + Like</a></li>
                                        </ul>
                                        <h4><a href="#">Merry Day</a></h4>
                                    </div>
                                </div>
                                <div class="kf_listing2_blog_des">
                                	<div style="max-width: 100%;" class="bx-wrapper"><div style="width: 100%; overflow: hidden; position: relative; height: 387px;" class="bx-viewport"><ul style="width: 515%; position: relative; transition-duration: 0.5s; transform: translate3d(-1696px, 0px, 0px);" class="kf_listing_slider"><li class="bx-clone" style="float: left; list-style: outside none none; position: relative; width: 848px;">
                                            <figure>
                                                <img src="assets/blog-listing-03.jpg" alt="">
                                            </figure>
                                        </li>
                                        <li style="float: left; list-style: outside none none; position: relative; width: 848px;">
                                            <figure>
                                                <img src="assets/blog-listing-01.jpg" alt="">
                                            </figure>
                                        </li>
                                        <li style="float: left; list-style: outside none none; position: relative; width: 848px;">
                                            <figure>
                                                <img src="assets/blog-listing-02.jpg" alt="">
                                            </figure>
                                        </li>
                                        <li style="float: left; list-style: outside none none; position: relative; width: 848px;">
                                            <figure>
                                                <img src="assets/blog-listing-03.jpg" alt="">
                                            </figure>
                                        </li>
                                    <li class="bx-clone" style="float: left; list-style: outside none none; position: relative; width: 848px;">
                                            <figure>
                                                <img src="assets/blog-listing-01.jpg" alt="">
                                            </figure>
                                        </li></ul></div><div class="bx-controls bx-has-controls-direction"><div class="bx-controls-direction"><a class="bx-prev" href="">Prev</a><a class="bx-next" href="">Next</a></div></div></div>
                                    <p>Sed ut perspiciatis unde omnis 
iste natus error sit voluptatem accusantium doloremque laudantium, totam
 rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi 
architectobeataeSed ut perspiciatis unde omnis iste natus error sit 
voluptatem accusantium doloremque laudantium, totam rem aperiam..</p>
                                    <a href="#">Continue Reading</a>
                                    <ul class="kf_blog_social_icon">
                                    	<li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            
                            <div class="kf_listing2_blog_wrap">
                            	<div class="kf_listing2_hdg_wrap">
                                    <div class="kf_blog2_date">
                                        <span>19 Dec</span>
                                    </div>
                                    <div class="kf_listing2_blog">
                                        <ul class="kf_blog_listing_meta">
                                            <li><i class="fa fa-calendar"></i><a href="#">12-08-2016</a></li>
                                            <li><i class="fa fa-user"></i><a href="#">Admin</a></li>
                                            <li><i class="fa fa-comments"></i><a href="#">Comment</a></li>
                                            <li><i class="fa fa-eye"></i><a href="#">View</a></li>
                                            <li><i class="fa fa-heart-o"></i><a href="#">99 + Like</a></li>
                                        </ul>
                                        <h4><a href="#">Happydais</a></h4>
                                    </div>
                                </div>
                                <div class="kf_listing2_blog_des">
                                	<figure>
										<img src="assets/blog-listing-01.jpg" alt="">
										<figcaption class="kf_envote_wrap">
											<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, <a href="#">http://enva.to/iaffep</a></p>
											<span>9:35 PM- 16 Feb 2016</span>
											<div class="kf_envoto_link_wrap">
												<ul class="envoto_ui_element">
													<li><a href="#">10 RETWITTS</a></li>
													<li><a href="#">10 RETWITTS</a></li>
												</ul>
												<ul class="envoto_tweet_element">
													<li><a href="#"><i class="fa fa-reply"></i></a></li>
													<li><a href="#"><i class="fa fa-retweet"></i></a></li>
													<li><a href="#"><i class="fa fa-star"></i></a></li>
												</ul>
											</div>
										</figcaption>
									</figure>
                                    <p>Watch “Jeopardy! “, Alex Trebek’s
 fun TV quiz game. Woven silk pyjamas exchanged for blue quartz. Brawny 
gods just flocked up to quiz and vex him. Adjusting quiver and bow, 
Zompyc killed the fox. My faxed joke won a pager in the cable TV quiz 
show. Amazingly few discotheques provide jukeboxes. My girl wove six 
dozen plaid jackets before she quit.</p>
                                    <a href="#">Continue Reading</a>
                                    <ul class="kf_blog_social_icon">
                                    	<li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            
                            <!--Pagination Wrap Start-->
                            <div class="kf_property_pagination">
                                <ul>
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                </ul>
                            </div>
                            <!--Pagination Wrap End-->
                            
                        </div>
                    </div>
                    <!--Blog Listing 2 Detail Wrap End-->
                    
                    <!--Aside bar Wrap Start-->
                    <div class="col-md-3">
                    	<div class="kf_asidebar_wrap">
                        	<div class="kf_aside_category aside_hdg">
                            	<h5>Categories</h5>
                                <ul>
                                	<li><a href="#">Guidance</a> <span>(55)</span></li>
                                    <li><a href="#">Listing</a> <span>(45)</span></li>
                                    <li><a href="#">Advertise</a> <span>(10)</span></li>
                                    <li><a href="#">Consulting</a> <span>(15)</span></li>
                                    <li><a href="#">Clean design</a> <span>(25)</span></li>
                                    <li><a href="#">Happy Clients</a> <span>(50)</span></li>
                                </ul>
                            </div>
                            
                            <div class="kf_aside_post_wrap aside_hdg">
                            	<h5>Featured Post</h5>
                                <ul>
                                	<li>
                                    	<figure>
                                        	<img src="assets/aside-post-01.jpg" alt="">
                                        </figure>
                                        <div class="kf_aside_post_des">
                                        	<h6><a href="#">Myths Vealesd </a></h6>
                                            <p>Vila</p>
                                            <span>$2,965,300</span>
                                        </div>
                                    </li>
                                    <li>
                                    	<figure>
                                        	<img src="assets/aside-post-02.jpg" alt="">
                                        </figure>
                                        <div class="kf_aside_post_des">
                                        	<h6><a href="#">Classic home </a></h6>
                                            <p>Vila</p>
                                            <span>$2,965,300</span>
                                        </div>
                                    </li>
                                    <li>
                                    	<figure>
                                        	<img src="assets/aside-post-03.jpg" alt="">
                                        </figure>
                                        <div class="kf_aside_post_des">
                                        	<h6><a href="#">Sunrise Townhomes </a></h6>
                                            <p>Vila</p>
                                            <span>$2,965,300</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            
                            <div class="kf_aside_fea_agent aside_hdg">
                            	<h5>Featured Agent</h5>
                                <ul>
                                	<li>
                                    	<figure>
                                        	<img src="assets/aside-post-04.jpg" alt="">
                                        </figure>
                                        <div class="kf_aside_agent_des">
                                        	<h6><a href="#">Beautiful single home </a></h6>
                                            <span><i class="fa fa-phone"></i>+91 222 334 551</span>
                                        </div>
                                    </li>
                                    <li>
                                    	<figure>
                                        	<img src="assets/aside-post-05.jpg" alt="">
                                        </figure>
                                        <div class="kf_aside_agent_des">
                                        	<h6><a href="#">Charming single family </a></h6>
                                            <span><i class="fa fa-phone"></i>+91 222 334 551</span>
                                        </div>
                                    </li>
                                    <li>
                                    	<figure>
                                        	<img src="assets/aside-post-06.jpg" alt="">
                                        </figure>
                                        <div class="kf_aside_agent_des">
                                        	<h6><a href="#">Hansom Alex </a></h6>
                                            <span><i class="fa fa-phone"></i>+91 222 334 551</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            
                            <div class="kf_aside_fea_properties aside_hdg">
                            	<h5>Featured Agent</h5>
                                <ul>
                                	<li><a href="#"><img src="assets/aside-post-01.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="assets/aside-post-02.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="assets/aside-post-03.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="assets/aside-post-04.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="assets/aside-post-05.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="assets/aside-post-06.jpg" alt=""></a></li>
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                    <!--Aside bar Wrap End-->
                </div>
            </div>
        </section>
        <!--Blog Listing 2 Wrap End-->
    </div>
    <!--Content Wrap End-->
    
    <!--Footer Wrap Start-->
    <!--Map Wrap Start-->
    <div class="kf_property_map_wrap">
        <div style="position: relative; background-color: rgb(229, 227, 223); overflow: hidden;" class="map-canvas" id="map-canvas"><div class="gm-style" style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; width: 100%; will-change: transform;"><div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; position: absolute; left: 318px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 574px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 318px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 318px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 574px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 574px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 62px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 830px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 62px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 62px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 830px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 830px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -194px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1086px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -194px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -194px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1086px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1086px; top: 264px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 318px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 574px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 318px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 318px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 574px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 574px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 62px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 830px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 62px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 62px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 830px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 830px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -194px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1086px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -194px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -194px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1086px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1086px; top: 264px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="position: absolute; left: 318px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_009.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 574px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_008.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 318px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_016.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 318px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_018.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 574px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_017.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 574px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_013.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 62px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_002.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 830px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_005.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 62px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_012.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 62px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 830px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_007.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 830px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_015.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -194px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_004.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1086px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_011.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -194px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_014.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -194px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_003.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1086px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_010.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1086px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_006.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div></div></div><div class="gm-style-pbc" style="position: absolute; left: 0px; top: 0px; z-index: 2; width: 100%; height: 100%; transition-duration: 0.3s; opacity: 0; display: none;"><p class="gm-style-pbt">Use two fingers to move the map</p></div><div style="position: absolute; left: 0px; top: 0px; z-index: 3; width: 100%; height: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 4; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a title="Click to see this area on Google Maps" href="https://maps.google.com/maps?ll=40.674389,-73.9455&amp;z=15&amp;hl=en-US&amp;gl=US&amp;mapclient=apiv3" target="_blank" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 66px; height: 26px; cursor: pointer;"><img draggable="false" src="assets/google_white5.png" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;"></div></a></div><div style="background-color: white; padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto,Arial,sans-serif; color: rgb(34, 34, 34); box-shadow: 0px 4px 16px rgba(0, 0, 0, 0.2); z-index: 10000002; display: none; width: 256px; height: 148px; position: absolute; left: 505px; top: 110px;"><div style="padding: 0px 0px 10px; font-size: 16px;">Map Data</div><div style="font-size: 13px;">Map data ©2016 Google</div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img draggable="false" src="assets/mapcnt6.png" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div><div style="z-index: 1000001; position: absolute; right: 75px; bottom: 0px; width: 121px;" class="gmnoprint"><div class="gm-style-cc" style="-moz-user-select: none; height: 14px; line-height: 14px;" draggable="false"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer; display: none;">Map Data</a><span style="">Map data ©2016 Google</span></div></div></div><div style="position: absolute; right: 0px; bottom: 0px;" class="gmnoscreen"><div style="font-family: Roboto,Arial,sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Map data ©2016 Google</div></div><div draggable="false" style="z-index: 1000001; -moz-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;" class="gmnoprint gm-style-cc"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_blank" href="https://www.google.com/intl/en-US_US/help/terms_maps.html" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Terms of Use</a></div></div><div style="width: 25px; height: 25px; overflow: hidden; display: none; margin: 10px 14px; position: absolute; top: 0px; right: 0px;"><img class="gm-fullscreen-control" draggable="false" src="assets/sv5.png" style="position: absolute; left: -52px; top: -86px; width: 164px; height: 112px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;"></div><div class="gm-style-cc" style="-moz-user-select: none; height: 14px; line-height: 14px; display: none; position: absolute; right: 0px; bottom: 0px;" draggable="false"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/maps/@40.674389,-73.9455,15z/data=%2110m1%211e1%2112b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;" title="Report errors in the road map or imagery to Google" target="_new">Report a map error</a></div></div><div controlheight="93" controlwidth="28" draggable="false" style="margin: 10px; -moz-user-select: none; position: absolute; bottom: 107px; right: 28px;" class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom"><div controlheight="55" controlwidth="28" style="position: absolute; left: 0px; top: 38px;" class="gmnoprint"><div style="-moz-user-select: none; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); border-radius: 2px; cursor: pointer; background-color: rgb(255, 255, 255); width: 28px; height: 55px;" draggable="false"><div style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;" title="Zoom in"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img draggable="false" src="assets/tmapctrl.png" style="position: absolute; left: 0px; top: 0px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div><div style="position: relative; overflow: hidden; width: 67%; height: 1px; left: 16%; background-color: rgb(230, 230, 230); top: 0px;"></div><div style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;" title="Zoom out"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img draggable="false" src="assets/tmapctrl.png" style="position: absolute; left: 0px; top: -15px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div></div></div><div controlheight="28" controlwidth="28" style="background-color: rgb(255, 255, 255); box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); border-radius: 2px; width: 28px; height: 28px; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default; position: absolute; left: 0px; top: 0px;" class="gm-svpc"><div style="position: absolute; left: 1px; top: 1px;"></div><div style="position: absolute; left: 1px; top: 1px;"><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px;"><img draggable="false" src="assets/cb_scout5.png" style="position: absolute; left: -147px; top: -26px; width: 215px; height: 835px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Pegman is on top of the Map" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img draggable="false" src="assets/cb_scout5.png" style="position: absolute; left: -147px; top: -52px; width: 215px; height: 835px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img draggable="false" src="assets/cb_scout5.png" style="position: absolute; left: -147px; top: -78px; width: 215px; height: 835px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div style="display: none; position: absolute;" controlheight="0" controlwidth="28" class="gmnoprint"><div title="Rotate map 90 degrees" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); cursor: pointer; background-color: rgb(255, 255, 255); display: none;"><img draggable="false" src="assets/tmapctrl4.png" style="position: absolute; left: -141px; top: 6px; width: 170px; height: 54px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div class="gm-tilt" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); top: 0px; cursor: pointer; background-color: rgb(255, 255, 255);"><img draggable="false" src="assets/tmapctrl4.png" style="position: absolute; left: -141px; top: -13px; width: 170px; height: 54px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div style="margin: 10px; z-index: 0; position: absolute; cursor: pointer; left: 0px; top: 0px;" class="gmnoprint"><div class="gm-style-mtc" style="float: left;"><div title="Show street map" draggable="false" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(86, 86, 86); font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-left-radius: 2px; border-top-left-radius: 2px; background-clip: padding-box; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); min-width: 22px;">Map</div></div><div class="gm-style-mtc" style="float: left;"><div draggable="false" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(0, 0, 0); font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-right-radius: 2px; border-top-right-radius: 2px; background-clip: padding-box; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); border-left: 0px none; min-width: 67px; font-weight: 500;">Custom Style</div></div></div></div></div>
    </div>
    <!--Map Wrap End-->
   <?php include('footer.php');?>
