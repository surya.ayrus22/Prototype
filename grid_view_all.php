    <?php include('header.php');?>
    <!--Sub Banner Wrap Start-->
    <div class="kf_property_sub_banner">
    	<div class="container">
        	<div class="kf_sub_banner_hdg">
            	<h3>Property Listing</h3>
            </div>
            <div class="kf_property_breadcrumb">
            	<ul>
                	<li><a href="#">Home</a></li>
                    <li><a href="#">Property Listing</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!--Sub Banner Wrap End-->
    
    <!--Content Wrap Start-->
    <div class="kf_property_content_wrap">
        <!--Property Listing Wrap Start-->
        <section class="kf_property_lissting_bg">
        	<div class="container">
				
							
				<div class="col-xs-12 si-site-container col-md-4">
				 <aside class="inner-sidebar">
    <div class="row">
      <div class="col-xs-12">
        <div class="square square--se">
          <div class="square__shell js-inner-search">
<div class="square__title js-qs-count">Search </div>

<div class="square__content">
  <div class="search-pane search-pane--inner">

    
  <form>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="kf_search_field">
                                            <!--input name="property_id" type="text"-->
                                            <input placeholder="Search by Zip Code, Address or MLS #" spellcheck="false" autocapitalize="off" autocorrect="off" autocomplete="off" class="js-qs-loc ui-autocomplete-input" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="kf_search_field">
											<select style="display: none;" sb="97056361" class="chosen-select" data-name="lprice">
												<option value="">Min Price</option>
												<option value="50000">$50,000</option>
												<option value="60000">$60,000</option>
												<option value="70000">$70,000</option>
												<option value="80000">$80,000</option>
												<option value="90000">$90,000</option>
												<option value="100000">$100,000</option>
												<option value="125000">$125,000</option>
												<option value="150000">$150,000</option>
												<option value="175000">$175,000</option>
												<option value="200000">$200,000</option>
												<option value="225000">$225,000</option>
												<option value="250000">$250,000</option>
												<option value="275000">$275,000</option>
												<option value="300000">$300,000</option>
												<option value="325000">$325,000</option>
												<option value="350000">$350,000</option>
												<option value="375000">$375,000</option>
												<option value="400000">$400,000</option>
												<option value="425000">$425,000</option>
												<option value="450000">$450,000</option>
												<option value="475000">$475,000</option>
												<option value="500000">$500,000</option>
												<option value="550000">$550,000</option>
												<option value="600000">$600,000</option>
												<option value="650000">$650,000</option>
												<option value="700000">$700,000</option>
												<option value="750000">$750,000</option>
												<option value="800000">$800,000</option>
												<option value="850000">$850,000</option>
												<option value="900000">$900,000</option>
												<option value="950000">$950,000</option>
												<option value="1000000">$1,000,000</option>
												<option value="1250000">$1,250,000</option>
												<option value="1500000">$1,500,000</option>
												<option value="1750000">$1,750,000</option>
												<option value="2000000">$2,000,000</option>
												<option value="2250000">$2,250,000</option>
												<option value="2500000">$2,500,000</option>
												<option value="2750000">$2,750,000</option>
												<option value="3000000">$3,000,000</option>
												<option value="3250000">$3,250,000</option>
												<option value="3500000">$3,500,000</option>
												<option value="3750000">$3,750,000</option>
												<option value="4000000">$4,000,000</option>
												<option value="4250000">$4,250,000</option>
												<option value="4500000">$4,500,000</option>
												<option value="4750000">$4,750,000</option>
												<option value="5000000">$5,000,000</option>
												<option value="5500000">$5,500,000</option>
												<option value="6000000">$6,000,000</option>
												<option value="6500000">$6,500,000</option>
												<option value="7000000">$7,000,000</option>
												<option value="7500000">$7,500,000</option>
												<option value="8000000">$8,000,000</option>
												<option value="8500000">$8,500,000</option>
												<option value="9000000">$9,000,000</option>
												<option value="9500000">$9,500,000</option>
												<option value="10000000">$10,000,000</option>
												<option value="11000000">$11,000,000</option>
												<option value="12000000">$12,000,000</option>
												<option value="13000000">$13,000,000</option>
												<option value="14000000">$14,000,000</option>
												<option value="15000000">$15,000,000</option>
												<option value="16000000">$16,000,000</option>
												<option value="17000000">$17,000,000</option>
												<option value="18000000">$18,000,000</option>
												<option value="19000000">$19,000,000</option>
												<option value="20000000">$20,000,000</option>
											</select>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="kf_search_field">
											<select style="display: none;" sb="3904622" class="chosen-select" data-name="uprice">
												<option value="">Max Price</option>
												<option value="50000">$50,000</option>
												<option value="60000">$60,000</option>
												<option value="70000">$70,000</option>
												<option value="80000">$80,000</option>
												<option value="90000">$90,000</option>
												<option value="100000">$100,000</option>
												<option value="125000">$125,000</option>
												<option value="150000">$150,000</option>
												<option value="175000">$175,000</option>
												<option value="200000">$200,000</option>
												<option value="225000">$225,000</option>
												<option value="250000">$250,000</option>
												<option value="275000">$275,000</option>
												<option value="300000">$300,000</option>
												<option value="325000">$325,000</option>
												<option value="350000">$350,000</option>
												<option value="375000">$375,000</option>
												<option value="400000">$400,000</option>
												<option value="425000">$425,000</option>
												<option value="450000">$450,000</option>
												<option value="475000">$475,000</option>
												<option value="500000">$500,000</option>
												<option value="550000">$550,000</option>
												<option value="600000">$600,000</option>
												<option value="650000">$650,000</option>
												<option value="700000">$700,000</option>
												<option value="750000">$750,000</option>
												<option value="800000">$800,000</option>
												<option value="850000">$850,000</option>
												<option value="900000">$900,000</option>
												<option value="950000">$950,000</option>
												<option value="1000000">$1,000,000</option>
												<option value="1250000">$1,250,000</option>
												<option value="1500000">$1,500,000</option>
												<option value="1750000">$1,750,000</option>
												<option value="2000000">$2,000,000</option>
												<option value="2250000">$2,250,000</option>
												<option value="2500000">$2,500,000</option>
												<option value="2750000">$2,750,000</option>
												<option value="3000000">$3,000,000</option>
												<option value="3250000">$3,250,000</option>
												<option value="3500000">$3,500,000</option>
												<option value="3750000">$3,750,000</option>
												<option value="4000000">$4,000,000</option>
												<option value="4250000">$4,250,000</option>
												<option value="4500000">$4,500,000</option>
												<option value="4750000">$4,750,000</option>
												<option value="5000000">$5,000,000</option>
												<option value="5500000">$5,500,000</option>
												<option value="6000000">$6,000,000</option>
												<option value="6500000">$6,500,000</option>
												<option value="7000000">$7,000,000</option>
												<option value="7500000">$7,500,000</option>
												<option value="8000000">$8,000,000</option>
												<option value="8500000">$8,500,000</option>
												<option value="9000000">$9,000,000</option>
												<option value="9500000">$9,500,000</option>
												<option value="10000000">$10,000,000</option>
												<option value="11000000">$11,000,000</option>
												<option value="12000000">$12,000,000</option>
												<option value="13000000">$13,000,000</option>
												<option value="14000000">$14,000,000</option>
												<option value="15000000">$15,000,000</option>
												<option value="16000000">$16,000,000</option>
												<option value="17000000">$17,000,000</option>
												<option value="18000000">$18,000,000</option>
												<option value="19000000">$19,000,000</option>
												<option value="20000000">$20,000,000</option>
											</select>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="kf_search_field">
											<select style="display: none;" sb="81264612" class="chosen-select" data-name="lbeds">
												<option value="">Beds</option>
												<option value="1">1+</option>
												<option value="2">2+</option>
												<option value="3">3+</option>
												<option value="4">4+</option>
												<option value="5">5+</option>
												<option value="6">6+</option>
											</select>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="kf_search_field">
											<select style="display: none;" sb="16590636" class="chosen-select" data-name="lbaths">
												<option value="">Baths</option>
												<option value="1">1+</option>
												<option value="2">2+</option>
												<option value="3">3+</option>
												<option value="4">4+</option>
												<option value="5">5+</option>
												<option value="6">6+</option>
											</select>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="kf_search_field">
                                            <select style="display: none;" class="chosen-select">
                                                <option >Any</option>
                                                <option>Commercial</option>
                                                <option>-Office</option>
												<option>-Shop</option>
                                                <option>-Residential</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="kf_search_field">
   											<select style="display: none;" sb="19377466" class="chosen-select" data-name="lsqft">
												<option value="">Square Feet</option>
												<option value="500">500+</option>
												<option value="1000">1000+</option>
												<option value="1500">1500+</option>
												<option value="2000">2000+</option>
												<option value="2500">2500+</option>
												<option value="3000">3000+</option>
												<option value="3500">3500+</option>
												<option value="4000">4000+</option>
												<option value="4500">4500+</option>
												<option value="5000">5000+</option>
												<option value="5500">5500+</option>
												<option value="6000">6000+</option>
												<option value="6500">6500+</option>
												<option value="7000">7000+</option>
											</select>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12">
                                        <div class="kf_search_field">
                                            <input value="More Search options" type="submit">
                                        </div>
                                    </div>

 
                                </div>
                            </form>
 


  </div>
</div></div>
        </div>
      </div>
    </div>



  </aside>
				</div>
				
				<h5>Property Grid Veiw</h5>
				</br>
            	<!--Property Meta Wrap Start-->
            	<div class="col-xs-12 si-site-container col-md-8">
            	<div class="kf_property_meta">
                    <div class="kf_view_type">
                    	<div class="kf_property_view">
                            <span>Sort By:</span>
                            <select style="display: none;" class="chosen-select">
                                <option selected="selected">Any</option>
                                <option>Option 2</option>
                                <option>Option 3</option>
                            </select><div title="" style="width: 145px;" class="chosen-container chosen-container-single"><a class="chosen-single" tabindex="-1"><span>Any</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input autocomplete="off" type="text"></div><ul class="chosen-results"></ul></div></div>
                        </div>
                        
                        <div class="kf_property_view">
                            <span>Select Veiw:</span>
                            <select style="display: none;" class="chosen-select">
                                <option selected="selected">Any</option>
                                <option>Option 2</option>
                                <option>Option 3</option>
                            </select><div title="" style="width: 145px;" class="chosen-container chosen-container-single"><a class="chosen-single" tabindex="-1"><span>Any</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input autocomplete="off" type="text"></div><ul class="chosen-results"></ul></div></div>
                        </div>
                        
                        <div class="kf_property_view">
                            <span>Select Veiw:</span>
                            <a href="list_view_all.php"><i class="fa fa-th-list"></a></i>
                            <a href="grid_view_all.php"><i class="fa fa-th-large"></a></i>
                        </div>
                    </div>
                    
                </div>
                <!--Property Meta Wrap End-->
                
                <!--Property Listing List Wrap Start-->
                <div class="row">
                	<div class="col-md-4 col-md-6">
                    	<div class="kf_property_listing_wrap">
                        	<figure>
                            	<img src="assets/listing-01.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                	<span class="kf_listing_overlay"></span>
                                	<a class="kf_md_btn kf_link_1" href="property_details.php">View Detail</a>
                                </figcaption>
                            </figure>
                            <div class="kf_property_listing_des">
                            	<h5><a href="property_details.php">Bill Tower, Survey, NJ</a></h5>
                                <p>Aliquam vitae sem id sem efficitur interdum. Phasellus ut nulla nisi. </p>
                                <div class="kf_listing_total_price">
                                	<h4>$5,505</h4>
                                </div>
                                <ul>
                                	<li>3 Room</li>
                                    <li>1 Bath</li>
                                    <li>2 Beds</li>
                                    <li>10 Sqft</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-md-6">
                    	<div class="kf_property_listing_wrap">
                        	<figure>
                            	<img src="assets/listing-02.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                	<span class="kf_listing_overlay"></span>
                                	<a class="kf_md_btn kf_link_1" href="property_details.php">View Detail</a>
                                </figcaption>
                            </figure>
                            <div class="kf_property_listing_des">
                            	<h5><a href="property_details.php">Florida 5, Pinecrest, FL</a></h5>
                                <p>The bedding was hardly able to cover it and seemed ready to slide off… </p>
                                <div class="kf_listing_total_price">
                                	<h4>$195,000</h4>
                                </div>
                                <ul>
                                	<li>4 Room</li>
                                    <li>3 Bath</li>
                                    <li>2 Beds</li>
                                    <li>84 Sqft</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-md-6">
                    	<div class="kf_property_listing_wrap">
                        	<figure>
                            	<img src="assets/listing-03.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                	<span class="kf_listing_overlay"></span>
                                	<a class="kf_md_btn kf_link_1" href="property_details.php">View Detail</a>
                                </figcaption>
                            </figure>
                            <div class="kf_property_listing_des">
                            	<h5><a href="property_details.php">Southwest Croase, CA</a></h5>
                                <p>Look at our Latest listed properties and check out the facilities on them, We… </p>
                                <div class="kf_listing_total_price">
                                	<h4>$250,199</h4>
                                </div>
                                <ul>
                                	<li>8 Room</li>
                                    <li>5 Bath</li>
                                    <li>10 Beds</li>
                                    <li>20 Sqft</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-md-6">
                    	<div class="kf_property_listing_wrap">
                        	<figure>
                            	<img src="assets/listing-04.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                	<span class="kf_listing_overlay"></span>
                                	<a class="kf_md_btn kf_link_1" href="property_details.php">View Detail</a>
                                </figcaption>
                            </figure>
                            <div class="kf_property_listing_des">
                            	<h5><a href="property_details.php">5359 Kensington, Colorado Rd</a></h5>
                                <p>Many desktop publishing fact thagr or reader will distracted packages and … </p>
                                <div class="kf_listing_total_price">
                                	<h4>$ 2,500</h4>
                                </div>
                                <ul>
                                	<li>3 Room</li>
                                    <li>1 Bath</li>
                                    <li>2 Beds</li>
                                    <li>10 Sqft</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-md-6">
                    	<div class="kf_property_listing_wrap">
                        	<figure>
                            	<img src="assets/listing-05.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                	<span class="kf_listing_overlay"></span>
                                	<a class="kf_md_btn kf_link_1" href="property_details.php">View Detail</a>
                                </figcaption>
                            </figure>
                            <div class="kf_property_listing_des">
                            	<h5><a href="property_details.php">120 Biscayne Rd, Miami</a></h5>
                                <p>He lay on his armour-like back, and if he lifted his head a little he could see… </p>
                                <div class="kf_listing_total_price">
                                	<h4>$125,799</h4>
                                </div>
                                <ul>
                                	<li>6 Room</li>
                                    <li>2 Bath</li>
                                    <li>4 Beds</li>
                                    <li>15 Sqft</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-md-6">
                    	<div class="kf_property_listing_wrap">
                        	<figure>
                            	<img src="assets/listing-06.jpg" alt="">
                                <figcaption class="kf_listing_detail">
                                	<span class="kf_listing_overlay"></span>
                                	<a class="kf_md_btn kf_link_1" href="property_details.php">View Detail</a>
                                </figcaption>
                            </figure>
                            <div class="kf_property_listing_des">
                            	<h5><a href="property_details.php">Avenue Boulevard, Coral</a></h5>
                                <p>One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed… </p>
                                <div class="kf_listing_total_price">
                                	<h4>$990,985</h4>
                                </div>
                                <ul>
                                	<li>7 Room</li>
                                    <li>3 Bath</li>
                                    <li>4 Beds</li>
                                    <li>30 Sqft</li>
                                </ul>
                            </div>
                        </div>
                    
                    </div></div>
                </div>
                <!--Property Listing List Wrap End-->
                
            </div>
        </section>
        <!--Property Listing Wrap End-->
     </div>   	
    <!--Content Wrap End-->
    
    <!--Footer Wrap Start-->
    
    <!--Map Wrap Start-->
    <!--div class="kf_property_map_wrap">
        <div style="position: relative; background-color: rgb(229, 227, 223); overflow: hidden;" class="map-canvas" id="map-canvas"><div class="gm-style" style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; width: 100%; will-change: transform;"><div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; position: absolute; left: 318px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 574px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 318px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 318px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 574px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 574px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 62px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 830px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 62px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 62px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 830px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 830px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -194px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1086px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -194px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -194px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1086px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1086px; top: 264px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 318px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 574px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 318px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 318px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 574px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 574px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 62px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 830px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 62px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 62px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 830px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 830px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -194px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1086px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -194px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -194px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1086px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1086px; top: 264px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="position: absolute; left: 318px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_012.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 574px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_009.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 318px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_017.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 318px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_016.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 574px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_018.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 574px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_013.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 62px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_002.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 830px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_005.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 62px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_014.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 62px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 830px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_008.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 830px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_007.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -194px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_004.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1086px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_011.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -194px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_015.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -194px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_003.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1086px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_010.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1086px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_006.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div></div></div><div class="gm-style-pbc" style="position: absolute; left: 0px; top: 0px; z-index: 2; width: 100%; height: 100%; transition-duration: 0.3s; opacity: 0; display: none;"><p class="gm-style-pbt">Use two fingers to move the map</p></div><div style="position: absolute; left: 0px; top: 0px; z-index: 3; width: 100%; height: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 4; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a title="Click to see this area on Google Maps" href="https://maps.google.com/maps?ll=40.674389,-73.9455&amp;z=15&amp;hl=en-US&amp;gl=US&amp;mapclient=apiv3" target="_blank" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 66px; height: 26px; cursor: pointer;"><img draggable="false" src="assets/google_white5.png" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;"></div></a></div><div style="background-color: white; padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto,Arial,sans-serif; color: rgb(34, 34, 34); box-shadow: 0px 4px 16px rgba(0, 0, 0, 0.2); z-index: 10000002; display: none; width: 256px; height: 148px; position: absolute; left: 505px; top: 110px;"><div style="padding: 0px 0px 10px; font-size: 16px;">Map Data</div><div style="font-size: 13px;">Map data ©2016 Google</div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img draggable="false" src="assets/mapcnt6.png" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div><div style="z-index: 1000001; position: absolute; right: 75px; bottom: 0px; width: 121px;" class="gmnoprint"><div class="gm-style-cc" style="-moz-user-select: none; height: 14px; line-height: 14px;" draggable="false"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer; display: none;">Map Data</a><span style="">Map data ©2016 Google</span></div></div></div><div style="position: absolute; right: 0px; bottom: 0px;" class="gmnoscreen"><div style="font-family: Roboto,Arial,sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Map data ©2016 Google</div></div><div draggable="false" style="z-index: 1000001; -moz-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;" class="gmnoprint gm-style-cc"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_blank" href="https://www.google.com/intl/en-US_US/help/terms_maps.html" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Terms of Use</a></div></div><div style="width: 25px; height: 25px; overflow: hidden; display: none; margin: 10px 14px; position: absolute; top: 0px; right: 0px;"><img class="gm-fullscreen-control" draggable="false" src="assets/sv5.png" style="position: absolute; left: -52px; top: -86px; width: 164px; height: 112px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;"></div><div class="gm-style-cc" style="-moz-user-select: none; height: 14px; line-height: 14px; display: none; position: absolute; right: 0px; bottom: 0px;" draggable="false"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/maps/@40.674389,-73.9455,15z/data=%2110m1%211e1%2112b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;" title="Report errors in the road map or imagery to Google" target="_new">Report a map error</a></div></div><div controlheight="93" controlwidth="28" draggable="false" style="margin: 10px; -moz-user-select: none; position: absolute; bottom: 107px; right: 28px;" class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom"><div controlheight="55" controlwidth="28" style="position: absolute; left: 0px; top: 38px;" class="gmnoprint"><div style="-moz-user-select: none; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); border-radius: 2px; cursor: pointer; background-color: rgb(255, 255, 255); width: 28px; height: 55px;" draggable="false"><div style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;" title="Zoom in"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img draggable="false" src="assets/tmapctrl.png" style="position: absolute; left: 0px; top: 0px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div><div style="position: relative; overflow: hidden; width: 67%; height: 1px; left: 16%; background-color: rgb(230, 230, 230); top: 0px;"></div><div style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;" title="Zoom out"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img draggable="false" src="assets/tmapctrl.png" style="position: absolute; left: 0px; top: -15px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div></div></div><div controlheight="28" controlwidth="28" style="background-color: rgb(255, 255, 255); box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); border-radius: 2px; width: 28px; height: 28px; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default; position: absolute; left: 0px; top: 0px;" class="gm-svpc"><div style="position: absolute; left: 1px; top: 1px;"></div><div style="position: absolute; left: 1px; top: 1px;"><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px;"><img draggable="false" src="assets/cb_scout5.png" style="position: absolute; left: -147px; top: -26px; width: 215px; height: 835px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Pegman is on top of the Map" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img draggable="false" src="assets/cb_scout5.png" style="position: absolute; left: -147px; top: -52px; width: 215px; height: 835px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img draggable="false" src="assets/cb_scout5.png" style="position: absolute; left: -147px; top: -78px; width: 215px; height: 835px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div style="display: none; position: absolute;" controlheight="0" controlwidth="28" class="gmnoprint"><div title="Rotate map 90 degrees" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); cursor: pointer; background-color: rgb(255, 255, 255); display: none;"><img draggable="false" src="assets/tmapctrl4.png" style="position: absolute; left: -141px; top: 6px; width: 170px; height: 54px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div class="gm-tilt" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); top: 0px; cursor: pointer; background-color: rgb(255, 255, 255);"><img draggable="false" src="assets/tmapctrl4.png" style="position: absolute; left: -141px; top: -13px; width: 170px; height: 54px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div style="margin: 10px; z-index: 0; position: absolute; cursor: pointer; left: 0px; top: 0px;" class="gmnoprint"><div class="gm-style-mtc" style="float: left;"><div title="Show street map" draggable="false" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(86, 86, 86); font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-left-radius: 2px; border-top-left-radius: 2px; background-clip: padding-box; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); min-width: 22px;">Map</div></div><div class="gm-style-mtc" style="float: left;"><div draggable="false" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(0, 0, 0); font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-right-radius: 2px; border-top-right-radius: 2px; background-clip: padding-box; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); border-left: 0px none; min-width: 67px; font-weight: 500;">Custom Style</div></div></div></div></div>
    </div>
    <!--Map Wrap End-->
    
    <!--Company Wrap Start-->
    <!--div class="kf_property_compnay_bg">
        <div class="container">
            <div style="opacity: 1; display: block;" class="kf_company_slider owl-carousel owl-theme">
                <div class="owl-wrapper-outer"><div style="width: 3192px; left: 0px; display: block; transition: all 1000ms ease 0s; transform: translate3d(0px, 0px, 0px);" class="owl-wrapper"><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-01.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-02.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-03.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-04.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-05.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-01.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-02.jpg" alt=""></a>
                    </div>
                </div></div></div></div>
                
                
                
                
                
                
            <div style="display: block;" class="owl-controls clickable"><div class="owl-pagination"><div class="owl-page active"><span class=""></span></div><div class="owl-page"><span class=""></span></div></div><div class="owl-buttons"><div class="owl-prev">prev</div><div class="owl-next">next</div></div></div></div>
        </div>
    </div-->
    <!--Company Wrap End-->
    <style>
.kf_view_type {
    float: left;
    width: none !important;
    margin-left: 2px;
}	
.kf_property_view {
    float: right;
    margin-left: 10px !important;
    width: auto;
}
    </style>
   <?php include('footer.php'); ?>
