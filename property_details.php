    <?php include('header.php');?>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>

    <!--Sub Banner Wrap Start-->
    <div class="kf_property_sub_banner">
    	<div class="container">
        	<div class="kf_sub_banner_hdg">
            	<h3>Property Detail</h3>
            </div>
            <div class="kf_property_breadcrumb">
            	<ul>
                	<li><a href="#">Home</a></li>
                    <li><a href="#">Property Detail</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!--Sub Banner Wrap End-->
    
    <!--Content Wrap Start-->
    <div class="kf_property_content_wrap">
        <!--Property Detail Wrap Start-->
        <section class="kf_property_detail_bg">
        	<div class="container">
                <div class="row">
                	<!--Property Detail Description Wrap Start-->
                    <div class="col-md-9">
                    	<div class="kf_detail_information">
							<!--kf_property_img_datil strat-->
							<div class="kf_property_img_datil">
                            	<div style="max-width: 100%;" class="bx-wrapper"><div style="width: 100%; overflow: hidden; position: relative; height: 328px;" class="bx-viewport"><ul style="width: 615%; position: relative; transition-duration: 0s; transform: translate3d(-848px, 0px, 0px);" class="property_pager_item"><li class="bx-clone" style="float: left; list-style: outside none none; position: relative; width: 848px;">
                                        <figure>
                                            <img src="assets/property-detail-01.jpg" alt="image">
                                        </figure>
                                        <div class="kf_property_detail_bed">
                                                <ul>
                                                    <li><a href="#"><span>5<i class="fa fa-bed"></i></span>Bedrooms</a></li>
                                                    <li><a href="#"><span>4<i class="fa fa-bed"></i></span>Bathrooms</a></li>
                                                    <li><a href="#"><span>2<i class="fa fa-bed"></i></span>Garage</a></li>
                                                </ul>
                                            </div>
                                        <div class="kf_property_detail_sale">
                                            <a href="#">Sale</a>
                                            <a href="#">$5,540,00</a>
                                        </div>
                                    </li>
                                	<li style="float: left; list-style: outside none none; position: relative; width: 848px;">
                                        <figure>
                                            <img src="assets/property-detail-01.jpg" alt="image">
                                        </figure>
                                        <div class="kf_property_detail_bed">
                                                <ul>
                                                    <li><a href="#"><span>5<i class="fa fa-bed"></i></span>Bedrooms</a></li>
                                                    <li><a href="#"><span>4<i class="fa fa-bed"></i></span>Bathrooms</a></li>
                                                    <li><a href="#"><span>2<i class="fa fa-bed"></i></span>Garage</a></li>
                                                </ul>
                                            </div>
                                        <div class="kf_property_detail_sale">
                                            <a href="#">Sale</a>
                                            <a href="#">$5,540,00</a>
                                        </div>
                                    </li>
                                    <li style="float: left; list-style: outside none none; position: relative; width: 848px;">
                                        <figure>
                                            <img src="assets/property-detail-02.jpg" alt="image">
                                        </figure>
                                        <div class="kf_property_detail_bed">
                                                <ul>
                                                    <li><a href="#"><span>5<i class="fa fa-bed"></i></span>Bedrooms</a></li>
                                                    <li><a href="#"><span>4<i class="fa fa-bed"></i></span>Bathrooms</a></li>
                                                    <li><a href="#"><span>2<i class="fa fa-bed"></i></span>Garage</a></li>
                                                </ul>
                                            </div>
                                        <div class="kf_property_detail_sale">
                                            <a href="#">Sale</a>
                                            <a href="#">$5,540,00</a>
                                        </div>
                                    </li>
                                    <li style="float: left; list-style: outside none none; position: relative; width: 848px;">
                                        <figure>
                                            <img src="assets/property-detail-03.jpg" alt="image">
                                        </figure>
                                        <div class="kf_property_detail_bed">
                                                <ul>
                                                    <li><a href="#"><span>5<i class="fa fa-bed"></i></span>Bedrooms</a></li>
                                                    <li><a href="#"><span>4<i class="fa fa-bed"></i></span>Bathrooms</a></li>
                                                    <li><a href="#"><span>2<i class="fa fa-bed"></i></span>Garage</a></li>
                                                </ul>
                                            </div>
                                        <div class="kf_property_detail_sale">
                                            <a href="#">Sale</a>
                                            <a href="#">$5,540,00</a>
                                        </div>
                                    </li>
                                    <li style="float: left; list-style: outside none none; position: relative; width: 848px;">
                                        <figure>
                                            <img src="assets/property-detail-01.jpg" alt="image">
                                        </figure>
                                        <div class="kf_property_detail_bed">
                                                <ul>
                                                    <li><a href="#"><span>5<i class="fa fa-bed"></i></span>Bedrooms</a></li>
                                                    <li><a href="#"><span>4<i class="fa fa-bed"></i></span>Bathrooms</a></li>
                                                    <li><a href="#"><span>2<i class="fa fa-bed"></i></span>Garage</a></li>
                                                </ul>
                                            </div>
                                        <div class="kf_property_detail_sale">
                                            <a href="#">Sale</a>
                                            <a href="#">$5,540,00</a>
                                        </div>
                                    </li>
                                <li class="bx-clone" style="float: left; list-style: outside none none; position: relative; width: 848px;">
                                        <figure>
                                            <img src="assets/property-detail-01.jpg" alt="image">
                                        </figure>
                                        <div class="kf_property_detail_bed">
                                                <ul>
                                                    <li><a href="#"><span>5<i class="fa fa-bed"></i></span>Bedrooms</a></li>
                                                    <li><a href="#"><span>4<i class="fa fa-bed"></i></span>Bathrooms</a></li>
                                                    <li><a href="#"><span>2<i class="fa fa-bed"></i></span>Garage</a></li>
                                                </ul>
                                            </div>
                                        <div class="kf_property_detail_sale">
                                            <a href="#">Sale</a>
                                            <a href="#">$5,540,00</a>
                                        </div>
                                    </li></ul></div><div class="bx-controls bx-has-controls-direction"><div class="bx-controls-direction"><a class="bx-prev" href="">Prev</a><a class="bx-next" href="">Next</a></div></div></div>
							</div>
							<!--kf_property_img_datil end-->
                            
							<!--kf_property_detail_img_row strat-->
							<div class="kf_property_detail_img_row" id="property_detail_pager">
								<a class="active" data-slide-index="0" href=""><img src="assets/detail-pager-01.jpg" alt="image"></a>
                                <a class="" data-slide-index="1" href=""><img src="assets/detail-pager-02.jpg" alt="image"></a>
                                <a class="" data-slide-index="2" href=""><img src="assets/detail-pager-03.jpg" alt="image"></a>
                                <a class="" data-slide-index="3" href=""><img src="assets/detail-pager-01.jpg" alt="image"></a>
							</div>
							<!--kf_property_detail_img_row end-->
                            
							<!--kf_property_detail_uptwon strat-->
							<div class="kf_property_detail_uptwon">
								<h3>Beautiful Villa in Uptown</h3>
								<ul>
									<li><i class="fa fa-map-marker"></i><a href="#">15421 Westzoon Avenue Tereance</a></li>
									<li><i class="fa fa-phone"></i><a href="#">888.111.22.444</a></li>
								</ul>
								<p>Cozy sphinx waves quart jug of bad milk. A very bad quack 
might jinx zippy fowls. Few quips galvanized the mock jury box. Quick 
brown dogs jump over the lazy fox. The jay, pig, fox, zebra, and my 
wolves quack! Blowzy red vixens fight for a quick jump. Joaquin Phoenix 
was gazed by MTV for luck. A wizard’s job is to vex chumps quickly in 
fog. </p>
							</div>
							<!--kf_property_detail_uptwon end-->
                            
							<!--kf_property_detail_link strat-->
							<div class="kf_property_detail_link">
								<div class="row">
									<div class="col-md-12">
										<div class="kf_property_detail_hdg">
											<h5>Essentail Information</h5>
										</div>
									</div>
									<div class="col-md-4">
										<div class="kf_property_detail_Essentail">
											<ul>
												<li><a href="#"><i class="fa fa-check-circle"></i>Price: $95,000.00</a></li>
												<li><a href="#"><i class="fa fa-check-circle"></i>For Sale/Rent: Sale</a></li>
												<li><a href="#"><i class="fa fa-check-circle"></i>Property Types: Resident</a></li>
											</ul>
										</div>
									</div>
									<div class="col-md-4">
										<div class="kf_property_detail_Essentail">
											<ul>
												<li><a href="#"><i class="fa fa-check-circle"></i>Country: USA</a></li>
												<li><a href="#"><i class="fa fa-check-circle"></i>Adress: San Francisco</a></li>
												<li><a href="#"><i class="fa fa-check-circle"></i>City: California</a></li>
											</ul>
										</div>
									</div>
									<div class="col-md-4">
										<div class="kf_property_detail_Essentail">
											<ul>
												<li><a href="#"><i class="fa fa-check-circle"></i>Garages: 01</a></li>
												<li><a href="#"><i class="fa fa-check-circle"></i>Bedrooms: 03</a></li>
												<li><a href="#"><i class="fa fa-check-circle"></i>Bathrooms: 02</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<!--kf_property_detail_link end-->
                            
							<!--kf_property_detail_link strat-->
							<div class="kf_property_detail_link">
								<div class="row">
									<div class="col-md-12">
										<div class="kf_property_detail_hdg">
											<h5>Home Amenities</h5>
										</div>
									</div>
									<div class="col-md-4">
										<div class="kf_property_detail_Essentail">
											<ul>
												<li><a href="#"><i class="fa fa-check-circle"></i>Air Conditioning</a></li>
												<li><a href="#"><i class="fa fa-check-circle"></i>Balcony</a></li>
												<li><a href="#"><i class="fa fa-check-circle"></i>Private Space</a></li>
											</ul>
										</div>
									</div>
									<div class="col-md-4">
										<div class="kf_property_detail_Essentail">
											<ul>
												<li><a href="#"><i class="fa fa-check-circle"></i>Cot</a></li>
												<li><a href="#"><i class="fa fa-check-circle"></i>Dishwasher</a></li>
												<li><a href="#"><i class="fa fa-check-circle"></i>Swimming Pool</a></li>
											</ul>
										</div>
									</div>
									<div class="col-md-4">
										<div class="kf_property_detail_Essentail">
											<ul>
												<li><a href="#"><i class="fa fa-check-circle"></i>Lift</a></li>
												<li><a href="#"><i class="fa fa-check-circle"></i>Parking</a></li>
												<li><a href="#"><i class="fa fa-check-circle"></i>Wifi</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<!--kf_property_detail_link end-->
                            
							<!--kf_property_detail_map strat-->
							<div class="kf_property_detail_map">
								<h5>Map Veiw</h5>
								<div style="position: relative; background-color: rgb(229, 227, 223); overflow: hidden;" class="map-canvas" id="map-canvas"><div class="gm-style" style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; width: 100%; will-change: transform;"><div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; position: absolute; left: 87px; top: -22px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 87px; top: 234px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 343px; top: -22px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 343px; top: 234px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -169px; top: -22px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -169px; top: 234px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 599px; top: -22px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 599px; top: 234px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 87px; top: -22px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 87px; top: 234px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 343px; top: -22px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 343px; top: 234px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -169px; top: -22px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -169px; top: 234px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 599px; top: -22px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 599px; top: 234px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="position: absolute; left: 87px; top: -22px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_007.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 87px; top: 234px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_004.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 343px; top: -22px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_006.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 343px; top: 234px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_005.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -169px; top: -22px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_002.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -169px; top: 234px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_008.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 599px; top: -22px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_003.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 599px; top: 234px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div></div></div><div class="gm-style-pbc" style="position: absolute; left: 0px; top: 0px; z-index: 2; width: 100%; height: 100%; transition-duration: 0.3s; opacity: 0; display: none;"><p class="gm-style-pbt">Use two fingers to move the map</p></div><div style="position: absolute; left: 0px; top: 0px; z-index: 3; width: 100%; height: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 4; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a title="Click to see this area on Google Maps" href="https://maps.google.com/maps?ll=40.674389,-73.9455&amp;z=15&amp;hl=en-US&amp;gl=US&amp;mapclient=apiv3" target="_blank" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 66px; height: 26px; cursor: pointer;"><img draggable="false" src="assets/google_white5.png" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;"></div></a></div><div style="background-color: white; padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto,Arial,sans-serif; color: rgb(34, 34, 34); box-shadow: 0px 4px 16px rgba(0, 0, 0, 0.2); z-index: 10000002; display: none; width: 256px; height: 148px; position: absolute; left: 274px; top: 80px;"><div style="padding: 0px 0px 10px; font-size: 16px;">Map Data</div><div style="font-size: 13px;">Map data ©2016 Google</div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img draggable="false" src="assets/mapcnt6.png" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div><div style="z-index: 1000001; position: absolute; right: 75px; bottom: 0px; width: 121px;" class="gmnoprint"><div class="gm-style-cc" style="-moz-user-select: none; height: 14px; line-height: 14px;" draggable="false"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer; display: none;">Map Data</a><span style="">Map data ©2016 Google</span></div></div></div><div style="position: absolute; right: 0px; bottom: 0px;" class="gmnoscreen"><div style="font-family: Roboto,Arial,sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Map data ©2016 Google</div></div><div draggable="false" style="z-index: 1000001; -moz-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;" class="gmnoprint gm-style-cc"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_blank" href="https://www.google.com/intl/en-US_US/help/terms_maps.html" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Terms of Use</a></div></div><div style="width: 25px; height: 25px; overflow: hidden; display: none; margin: 10px 14px; position: absolute; top: 0px; right: 0px;"><img class="gm-fullscreen-control" draggable="false" src="assets/sv5.png" style="position: absolute; left: -52px; top: -86px; width: 164px; height: 112px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;"></div><div class="gm-style-cc" style="-moz-user-select: none; height: 14px; line-height: 14px; display: none; position: absolute; right: 0px; bottom: 0px;" draggable="false"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/maps/@40.674389,-73.9455,15z/data=%2110m1%211e1%2112b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;" title="Report errors in the road map or imagery to Google" target="_new">Report a map error</a></div></div><div controlheight="93" controlwidth="28" draggable="false" style="margin: 10px; -moz-user-select: none; position: absolute; bottom: 107px; right: 28px;" class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom"><div controlheight="55" controlwidth="28" style="position: absolute; left: 0px; top: 38px;" class="gmnoprint"><div style="-moz-user-select: none; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); border-radius: 2px; cursor: pointer; background-color: rgb(255, 255, 255); width: 28px; height: 55px;" draggable="false"><div style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;" title="Zoom in"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img draggable="false" src="assets/tmapctrl.png" style="position: absolute; left: 0px; top: 0px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div><div style="position: relative; overflow: hidden; width: 67%; height: 1px; left: 16%; background-color: rgb(230, 230, 230); top: 0px;"></div><div style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;" title="Zoom out"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img draggable="false" src="assets/tmapctrl.png" style="position: absolute; left: 0px; top: -15px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div></div></div><div controlheight="28" controlwidth="28" style="background-color: rgb(255, 255, 255); box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); border-radius: 2px; width: 28px; height: 28px; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default; position: absolute; left: 0px; top: 0px;" class="gm-svpc"><div style="position: absolute; left: 1px; top: 1px;"></div><div style="position: absolute; left: 1px; top: 1px;"><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px;"><img draggable="false" src="assets/cb_scout5.png" style="position: absolute; left: -147px; top: -26px; width: 215px; height: 835px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Pegman is on top of the Map" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img draggable="false" src="assets/cb_scout5.png" style="position: absolute; left: -147px; top: -52px; width: 215px; height: 835px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img draggable="false" src="assets/cb_scout5.png" style="position: absolute; left: -147px; top: -78px; width: 215px; height: 835px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div style="display: none; position: absolute;" controlheight="0" controlwidth="28" class="gmnoprint"><div title="Rotate map 90 degrees" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); cursor: pointer; background-color: rgb(255, 255, 255); display: none;"><img draggable="false" src="assets/tmapctrl4.png" style="position: absolute; left: -141px; top: 6px; width: 170px; height: 54px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div class="gm-tilt" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); top: 0px; cursor: pointer; background-color: rgb(255, 255, 255);"><img draggable="false" src="assets/tmapctrl4.png" style="position: absolute; left: -141px; top: -13px; width: 170px; height: 54px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div style="margin: 10px; z-index: 0; position: absolute; cursor: pointer; left: 0px; top: 0px;" class="gmnoprint"><div class="gm-style-mtc" style="float: left;"><div title="Show street map" draggable="false" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(86, 86, 86); font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-left-radius: 2px; border-top-left-radius: 2px; background-clip: padding-box; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); min-width: 22px;">Map</div></div><div class="gm-style-mtc" style="float: left;"><div draggable="false" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(0, 0, 0); font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-right-radius: 2px; border-top-right-radius: 2px; background-clip: padding-box; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); border-left: 0px none; min-width: 67px; font-weight: 500;">Custom Style</div></div></div></div></div>
							</div>
							<!--kf_property_detail_map end-->
                            
							<!--kf_property_detail_map strat-->
							<div class="kf_property_detail_map">
								<h5>Floor Plans</h5>
								<ul>
									<li>
										<figure>
											<img src="assets/prototype-01.jpg" alt="image">
											<figcaption class="kf_floor_detail">
												<span class="kf_listing_overlay"></span>
												<h5><a href="#">See More</a></h5>
											</figcaption>
										</figure>
									</li>
									<li>
										<figure>
											<img src="assets/prototype-02.jpg" alt="image">
											<figcaption class="kf_floor_detail">
												<span class="kf_listing_overlay"></span>
												<h5><a href="#">See More</a></h5>
											</figcaption>
										</figure>
									</li>
									<li>
										<figure>
											<img src="assets/prototype-03.jpg" alt="image">
											<figcaption class="kf_floor_detail">
												<span class="kf_listing_overlay"></span>
												<h5><a href="#">See More</a></h5>
											</figcaption>
										</figure>
									</li>
								</ul>
							</div>
							<!--kf_property_detail_map end-->
                        
							<!--kf_property_detail_content start-->
                            <div class="kf_property_detail_content">
                                <h5>Contact With Agents</h5>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="kf_property_detail_agent">
                                            <figure>
                                                <img src="assets/detail-agent-01.jpg" alt="iamge">
                                            </figure>
                                            <div class="kf_property_detail_social_icon">
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">		
                                        <div class="kf_property_detail_real">
                                            <h6>Merry Farley</h6>
                                            <span>Real Estate Agent</span>
                                            <ul>
                                                <li><i class="fa fa-envelope-o"></i><a href="#">Merry@yourcompany.com</a></li>
                                                <li><i class="fa fa-phone"></i>+1 555 22 66 8810</li>
                                                <li><i class="fa fa-mobile"></i>+1 555 22 66 8811</li>
                                                <li><i class="fa fa-print"></i>+1 555 22 66 8812</li>
                                            </ul>
                                            <p>Lorem ipsum dolor sit 
amet, consectetur adipiscing elit. Cras et dui vestibulum, bibendum 
purus sit amet, vulputate mauris. Ut adipiscing gravida tincidunt Ut 
adipiscing...</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">		
                                        <div class="kf_property_detail_form">
                                        <h5>Send Us Messages</h5>
                                            <form>
                                                <input class="form_holder" placeholder="Your Name:" type="text">
                                                <input class="form_holder" placeholder="Your Email:" type="text">
                                                <textarea class="form_holder" placeholder="Messages:"></textarea>
                                                <button>search</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            <!--kf_detail_information end-->
                            </div>
							<!--kf_property_detail_content end-->
                            
                            <!--Property For Rent Wrap Start-->
                            <!--div class="kf_related_property">
                            	<h5>Contact With Agents</h5>
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="kf_property_rent_wrap">
											<figure>
												<img src="assets/rent-01.jpg" alt="">
												<figcaption class="kf_listing_detail">
													<span class="kf_listing_overlay"></span>
													<a class="kf_md_btn kf_link_1" href="http://kodeforest.net/html/property/property-detail.html">View Detail</a>
													<div class="kf_rent_label">
														<p>For Rent</p>
													</div>
												</figcaption>
											</figure>
											<div class="kf_rent_property_des">
												<h6><a href="http://kodeforest.net/html/property/property-detail.html">Villa in Hialeah, Dade County</a></h6>
												<ul>
													<li>
														<i class="fa fa-bed"></i>
														<p>bedroom</p>
														<span>9</span>
													</li>
													<li>
														<i class="icon-bath"></i>
														<p>Bathroom</p>
														<span>6</span>
													</li>
													<li>
														<i class="fa fa-car"></i>
														<p>Garage</p>
														<span>2</span>
													</li>
												</ul>
											</div>
											<div class="kf_rent_location">
												<h6><i class="fa fa-map-marker"></i>South Dakota</h6>
												<div class="kf_rent_total_price">
													<h6>$2,500</h6>
												</div>
											</div>
										</div>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="kf_property_rent_wrap">
											<figure>
												<img src="assets/rent-02.jpg" alt="">
												<figcaption class="kf_listing_detail">
													<span class="kf_listing_overlay"></span>
													<a class="kf_md_btn kf_link_1" href="http://kodeforest.net/html/property/property-detail.html">View Detail</a>
													<div class="kf_rent_label">
														<p>For Sale</p>
													</div>
												</figcaption>
											</figure>
											<div class="kf_rent_property_des">
												<h6><a href="http://kodeforest.net/html/property/property-detail.html">401 Biscayne Boulevard, Miami</a></h6>
												<ul>
													<li>
														<i class="fa fa-bed"></i>
														<p>bedroom</p>
														<span>5</span>
													</li>
													<li>
														<i class="icon-bath"></i>
														<p>Bathroom</p>
														<span>4</span>
													</li>
													<li>
														<i class="fa fa-car"></i>
														<p>Garage</p>
														<span>2</span>
													</li>
												</ul>
											</div>
											<div class="kf_rent_location">
												<h6><i class="fa fa-map-marker"></i>Denman, London</h6>
												<div class="kf_rent_total_price">
													<h6>$3,500</h6>
												</div>
											</div>
										</div>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="kf_property_rent_wrap">
											<figure>
												<img src="assets/rent-03.jpg" alt="">
												<figcaption class="kf_listing_detail">
													<span class="kf_listing_overlay"></span>
													<a class="kf_md_btn kf_link_1" href="http://kodeforest.net/html/property/property-detail.html">View Detail</a>
													<div class="kf_rent_label">
														<p>For Rent</p>
													</div>
												</figcaption>
											</figure>
											<div class="kf_rent_property_des">
												<h6><a href="http://kodeforest.net/html/property/property-detail.html">1200 Anastasia Avenue, Coral Gables</a></h6>
												<ul>
													<li>
														<i class="fa fa-bed"></i>
														<p>bedroom</p>
														<span>5</span>
													</li>
													<li>
														<i class="icon-bath"></i>
														<p>Bathroom</p>
														<span>4</span>
													</li>
													<li>
														<i class="fa fa-car"></i>
														<p>Garage</p>
														<span>1</span>
													</li>
												</ul>
											</div>
											<div class="kf_rent_location">
												<h6><i class="fa fa-map-marker"></i>United Kingdom</h6>
												<div class="kf_rent_total_price">
													<h6>$6,600</h6>
												</div>
											</div>
										</div>
                                    </div>
                                </div>
                            </div-->
                            <!--Property For Rent Wrap End-->
                            
						</div>
                    </div>
                    <!--Property Detail Description Wrap End-->
                    
                    <!--Aside bar Wrap Start-->
                    <div class="col-md-3">
                    	<div class="kf_asidebar_wrap">
                        	<div class="kf_search_property_wrap">
                            	<h6>Search for Properties</h6>
                                <div class="kf_property_element">
                                   
                                    <div class="kf_property_field">
                                            <input placeholder="Search by Zip Code, Address or MLS #" spellcheck="false" autocapitalize="off" autocorrect="off" autocomplete="off" class="js-qs-loc ui-autocomplete-input" type="text" style="margin-bottom:10px;width:100%;">
                                    </div>
                                    
                                    <div class="kf_property_field">
										<select style="display: none;" sb="97056361" class="chosen-select" data-name="lprice">
												<option value="">Min Price</option>
												<option value="50000">$50,000</option>
												<option value="60000">$60,000</option>
												<option value="70000">$70,000</option>
												<option value="80000">$80,000</option>
												<option value="90000">$90,000</option>
												<option value="100000">$100,000</option>
												<option value="125000">$125,000</option>
												<option value="150000">$150,000</option>
												<option value="175000">$175,000</option>
												<option value="200000">$200,000</option>
												<option value="225000">$225,000</option>
												<option value="250000">$250,000</option>
												<option value="275000">$275,000</option>
												<option value="300000">$300,000</option>
												<option value="325000">$325,000</option>
												<option value="350000">$350,000</option>
												<option value="375000">$375,000</option>
												<option value="400000">$400,000</option>
												<option value="425000">$425,000</option>
												<option value="450000">$450,000</option>
												<option value="475000">$475,000</option>
												<option value="500000">$500,000</option>
												<option value="550000">$550,000</option>
												<option value="600000">$600,000</option>
												<option value="650000">$650,000</option>
												<option value="700000">$700,000</option>
												<option value="750000">$750,000</option>
												<option value="800000">$800,000</option>
												<option value="850000">$850,000</option>
												<option value="900000">$900,000</option>
												<option value="950000">$950,000</option>
												<option value="1000000">$1,000,000</option>
												<option value="1250000">$1,250,000</option>
												<option value="1500000">$1,500,000</option>
												<option value="1750000">$1,750,000</option>
												<option value="2000000">$2,000,000</option>
												<option value="2250000">$2,250,000</option>
												<option value="2500000">$2,500,000</option>
												<option value="2750000">$2,750,000</option>
												<option value="3000000">$3,000,000</option>
												<option value="3250000">$3,250,000</option>
												<option value="3500000">$3,500,000</option>
												<option value="3750000">$3,750,000</option>
												<option value="4000000">$4,000,000</option>
												<option value="4250000">$4,250,000</option>
												<option value="4500000">$4,500,000</option>
												<option value="4750000">$4,750,000</option>
												<option value="5000000">$5,000,000</option>
												<option value="5500000">$5,500,000</option>
												<option value="6000000">$6,000,000</option>
												<option value="6500000">$6,500,000</option>
												<option value="7000000">$7,000,000</option>
												<option value="7500000">$7,500,000</option>
												<option value="8000000">$8,000,000</option>
												<option value="8500000">$8,500,000</option>
												<option value="9000000">$9,000,000</option>
												<option value="9500000">$9,500,000</option>
												<option value="10000000">$10,000,000</option>
												<option value="11000000">$11,000,000</option>
												<option value="12000000">$12,000,000</option>
												<option value="13000000">$13,000,000</option>
												<option value="14000000">$14,000,000</option>
												<option value="15000000">$15,000,000</option>
												<option value="16000000">$16,000,000</option>
												<option value="17000000">$17,000,000</option>
												<option value="18000000">$18,000,000</option>
												<option value="19000000">$19,000,000</option>
												<option value="20000000">$20,000,000</option>
											</select>
                                    </div>
                                    
                                    <div class="kf_property_field">
											<select style="display: none;" sb="3904622" class="chosen-select" data-name="uprice">
												<option value="">Max Price</option>
												<option value="50000">$50,000</option>
												<option value="60000">$60,000</option>
												<option value="70000">$70,000</option>
												<option value="80000">$80,000</option>
												<option value="90000">$90,000</option>
												<option value="100000">$100,000</option>
												<option value="125000">$125,000</option>
												<option value="150000">$150,000</option>
												<option value="175000">$175,000</option>
												<option value="200000">$200,000</option>
												<option value="225000">$225,000</option>
												<option value="250000">$250,000</option>
												<option value="275000">$275,000</option>
												<option value="300000">$300,000</option>
												<option value="325000">$325,000</option>
												<option value="350000">$350,000</option>
												<option value="375000">$375,000</option>
												<option value="400000">$400,000</option>
												<option value="425000">$425,000</option>
												<option value="450000">$450,000</option>
												<option value="475000">$475,000</option>
												<option value="500000">$500,000</option>
												<option value="550000">$550,000</option>
												<option value="600000">$600,000</option>
												<option value="650000">$650,000</option>
												<option value="700000">$700,000</option>
												<option value="750000">$750,000</option>
												<option value="800000">$800,000</option>
												<option value="850000">$850,000</option>
												<option value="900000">$900,000</option>
												<option value="950000">$950,000</option>
												<option value="1000000">$1,000,000</option>
												<option value="1250000">$1,250,000</option>
												<option value="1500000">$1,500,000</option>
												<option value="1750000">$1,750,000</option>
												<option value="2000000">$2,000,000</option>
												<option value="2250000">$2,250,000</option>
												<option value="2500000">$2,500,000</option>
												<option value="2750000">$2,750,000</option>
												<option value="3000000">$3,000,000</option>
												<option value="3250000">$3,250,000</option>
												<option value="3500000">$3,500,000</option>
												<option value="3750000">$3,750,000</option>
												<option value="4000000">$4,000,000</option>
												<option value="4250000">$4,250,000</option>
												<option value="4500000">$4,500,000</option>
												<option value="4750000">$4,750,000</option>
												<option value="5000000">$5,000,000</option>
												<option value="5500000">$5,500,000</option>
												<option value="6000000">$6,000,000</option>
												<option value="6500000">$6,500,000</option>
												<option value="7000000">$7,000,000</option>
												<option value="7500000">$7,500,000</option>
												<option value="8000000">$8,000,000</option>
												<option value="8500000">$8,500,000</option>
												<option value="9000000">$9,000,000</option>
												<option value="9500000">$9,500,000</option>
												<option value="10000000">$10,000,000</option>
												<option value="11000000">$11,000,000</option>
												<option value="12000000">$12,000,000</option>
												<option value="13000000">$13,000,000</option>
												<option value="14000000">$14,000,000</option>
												<option value="15000000">$15,000,000</option>
												<option value="16000000">$16,000,000</option>
												<option value="17000000">$17,000,000</option>
												<option value="18000000">$18,000,000</option>
												<option value="19000000">$19,000,000</option>
												<option value="20000000">$20,000,000</option>
											</select>
                                    </div>
                                    
                                    <div class="kf_property_field">
											<select style="display: none;" sb="81264612" class="chosen-select" data-name="lbeds">
												<option value="">Beds</option>
												<option value="1">1+</option>
												<option value="2">2+</option>
												<option value="3">3+</option>
												<option value="4">4+</option>
												<option value="5">5+</option>
												<option value="6">6+</option>
											</select>
                                    </div>
                                    
                                    <div class="kf_property_field">
											<select style="display: none;" sb="16590636" class="chosen-select" data-name="lbaths">
												<option value="">Baths</option>
												<option value="1">1+</option>
												<option value="2">2+</option>
												<option value="3">3+</option>
												<option value="4">4+</option>
												<option value="5">5+</option>
												<option value="6">6+</option>
											</select>
                                    </div>
                                    
                                    <div class="kf_property_field">
                                            <select style="display: none;" class="chosen-select">
                                                <option selected="selected">Property Type</option>
                                                <option>Commercial</option>
                                                <option>-Office</option>
												<option>-Shop</option>
                                                <option>-Residential</option>
                                            </select>
                                    </div>
                                    
                                    <div class="kf_property_field">
                                    	<a href="#">More Filter</a>
                                        <input value="Search" type="submit">
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="kf_aside_post_wrap aside_hdg">
                            	<h5>Featured Post</h5>
                                <ul>
                                	<li>
                                    	<figure>
                                        	<img src="assets/aside-post-01.jpg" alt="">
                                        </figure>
                                        <div class="kf_aside_post_des">
                                        	<h6><a href="#">Myths Vealesd </a></h6>
                                            <p>Vila</p>
                                            <span>$2,965,300</span>
                                        </div>
                                    </li>
                                    <li>
                                    	<figure>
                                        	<img src="assets/aside-post-02.jpg" alt="">
                                        </figure>
                                        <div class="kf_aside_post_des">
                                        	<h6><a href="#">Classic home </a></h6>
                                            <p>Vila</p>
                                            <span>$2,965,300</span>
                                        </div>
                                    </li>
                                    <li>
                                    	<figure>
                                        	<img src="assets/aside-post-03.jpg" alt="">
                                        </figure>
                                        <div class="kf_aside_post_des">
                                        	<h6><a href="#">Sunrise Townhomes </a></h6>
                                            <p>Vila</p>
                                            <span>$2,965,300</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            
                            <div class="kf_aside_fea_agent aside_hdg">
                            	<h5>Featured Agent</h5>
                                <ul>
                                	<li>
                                    	<figure>
                                        	<img src="assets/aside-post-04.jpg" alt="">
                                        </figure>
                                        <div class="kf_aside_agent_des">
                                        	<h6><a href="#">Beautiful single home </a></h6>
                                            <span><i class="fa fa-phone"></i>+91 222 334 551</span>
                                        </div>
                                    </li>
                                    <li>
                                    	<figure>
                                        	<img src="assets/aside-post-05.jpg" alt="">
                                        </figure>
                                        <div class="kf_aside_agent_des">
                                        	<h6><a href="#">Charming single family </a></h6>
                                            <span><i class="fa fa-phone"></i>+91 222 334 551</span>
                                        </div>
                                    </li>
                                    <li>
                                    	<figure>
                                        	<img src="assets/aside-post-06.jpg" alt="">
                                        </figure>
                                        <div class="kf_aside_agent_des">
                                        	<h6><a href="#">Hansom Alex </a></h6>
                                            <span><i class="fa fa-phone"></i>+91 222 334 551</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            
                            <div class="kf_aside_fea_properties aside_hdg">
                            	<h5>Featured Agent</h5>
                                <ul>
                                	<li><a href="#"><img src="assets/aside-post-01.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="assets/aside-post-02.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="assets/aside-post-03.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="assets/aside-post-04.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="assets/aside-post-05.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="assets/aside-post-06.jpg" alt=""></a></li>
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                    <!--Aside bar Wrap End-->
                </div>
            </div>
        </section>
        <!--Property Detail Wrap End-->
     </div>   	
    <!--Content Wrap End-->
    
    <!--Footer Wrap Start-->
    
    <!--Company Wrap Start-->
    <div class="kf_property_compnay_bg">
        <div class="container">
            <div style="opacity: 1; display: block;" class="kf_company_slider owl-carousel owl-theme">
                <div class="owl-wrapper-outer"><div style="width: 3192px; left: 0px; display: block; transition: all 800ms ease 0s; transform: translate3d(-456px, 0px, 0px);" class="owl-wrapper"><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-01.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-02.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-03.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-04.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-05.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-01.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-02.jpg" alt=""></a>
                    </div>
                </div></div></div></div>
                
                
                
                
                
                
            <div style="display: block;" class="owl-controls clickable"><div class="owl-pagination"><div class="owl-page"><span class=""></span></div><div class="owl-page active"><span class=""></span></div></div><div class="owl-buttons"><div class="owl-prev">prev</div><div class="owl-next">next</div></div></div></div>
        </div>
    </div>
    <!--Company Wrap End-->
    
 <script>
$(document).ready(function(){
	$("#myModal").modal({ backdrop: 'static',  keyboard: true, show: true });
	$("#Login").click(function(){
		$("#myModal").modal("hide");
		$("#myModal1").modal({ backdrop: 'static',  keyboard: true,show: true });
	});
});


</script>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <!--button type="button" class="close" data-dismiss="modal">&times;</button-->
          <h4 class="modal-title">Activate Your Free Account for Full Access</h4>
        </div>
<div class="modal-body">
              <div class="si-form__error" style="display: none" id="authRegisterMessage"></div>
              <div class="si-row">
                <div class="form-group si-form__column">
                  <label for="authRegisterFirstName" class="control-label">First Name:<span class="req">*</span></label>
                  <input type="text" class="form-control" value="" id="authRegisterFirstName" name="firstName" tabindex="1" aria-required="true">
                </div>
                <div class="form-group si-form__column">
                  <label for="authRegisterLastName" class="control-label">Last Name:<span class="req">*</span></label>
                  <input type="text" class="form-control" value="" id="authRegisterLastName" name="lastName" tabindex="2" aria-required="true">
                </div>
                <div class="form-group si-form__full">
                  <label for="authRegisterEmail" class="control-label">Email:<span class="req">*</span></label>
                  <input type="email" placeholder="This will also be your sign in name" class="form-control" value="" id="authRegisterEmail" name="email" tabindex="3" aria-required="true">
                </div>
              
                <div class="form-group si-form__full">
                  <label for="authRegisterPhone" class="control-label">Phone:<span class="req">*</span></label>
                  <input type="phone" placeholder="(000) 000-0000" required="" class="form-control" id="authRegisterPhone" name="phone" tabindex="5" aria-required="true">
                </div>
              
                <div class="form-group si-form__full" id="authAgentListPanel">
                  <label for="authRegisterAgents" class="control-label">Currently working with one of our agents?<span class="req">*</span></label>
                  <select title="Please select an agent" required="" class="form-control" id="authRegisterAgents" tabindex="6" aria-required="true">
                    <option value="">Select an Option</option>
                    <option value="-1">Not at This Time</option>
                    
                    <option value="514">Karen Anderson</option>
                    
                    <option value="516">Claude Champagne</option>
                    
                    <option value="614">Agent Connect</option>
                    
                    <option value="536">Gilles Dalco</option>
                    
                    <option value="700">Michele Davenport</option>
                    
                    <option value="689">Laurel Dunay</option>
                    
                    <option value="538">Nancy Frey</option>
                    
                    <option value="553">Olivier Hannoun</option>
                    
                    <option value="707">Tommy Holmes</option>
                    
                    <option value="589">Rori Huberman</option>
                    
                    <option value="606">Josh Kallan</option>
                    
                    <option value="708">Marjorie Kent</option>
                    
                    <option value="600">Karen Knox</option>
                    
                    <option value="729">Rachael Kobin</option>
                    
                    <option value="715">Jared Leshner</option>
                    
                    <option value="588">Ryan Neill</option>
                    
                    <option value="604">Shari Orland</option>
                    
                    <option value="510">Caesar Parisi</option>
                    
                    <option value="542">Dina Schwartz</option>
                    
                    <option value="714">Charlene Singer</option>
                    
                    <option value="699">Maxine Sloane</option>
                    
                    <option value="601">Laura Trebatch</option>
                    
                  </select>
                </div>
              
                <div style="display:none;" class="form-group si-form__full" id="authAgentNamePanel">
                  <label for="authRegisterAgentName" class="control-label">Working with Another Agent? If so, Who?</label>
                  <input type="text" placeholder="Agent Name" class="form-control" id="authRegisterAgentName" tabindex="6">
                </div>
              
              </div>
            </div>
<div class="modal-footer">
              <button class="si-btn si-btn--primary si-btn--lg" tabindex="7" id="authRegisterSubmit" type="button">Create Free Account</button>
              <!--button class="si-btn si-btn--link si-btn--lg si-btn--block js-signin-link" tabindex="8" type="button">Returning User? Log In Here</button-->
              <button type="button" class="si-btn si-btn--link si-btn--lg si-btn--block js-signin-link" id="Login">Returning User? Log In Here</button>
            </div>
      </div>
      
    </div>
  </div>
  
  
  
    <!-- Modal -->
  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <!--button type="button" class="close" data-dismiss="modal">&times;</button-->
          <h4 class="modal-title">Activate Your Free Account for Full Access</h4>
        </div>
<form class="si-form" name="authSigninForm" id="authSigninForm" novalidate="novalidate">
            <div class="modal-body">
              <div class="si-form__error" style="display: none" id="authSigninMessage"></div>
              <div class="si-row">
                <div class="form-group si-form__full">
                  <label for="authSigninEmail" class="control-label">Email:<span class="req">*</span></label>
                  <input type="email" placeholder="name@website.com" class="form-control" value="" id="authSigninEmail" name="email" tabindex="11">
                </div>
              
              </div>
            </div>
            <div class="modal-footer">
              <button class="si-btn si-btn--primary si-btn--lg" tabindex="13" id="authSigninSubmit" type="button">Login</button>
            
            </div>
          </form>

      </div>
      
    </div>
  </div>
<style>
.modal-title {
    color: #000 !important;
}
.control-label {
color: #333 !important;
}
.modal-footer {
    text-align: center;
}
.si-btn--lg {
    border-radius: 6px;
    font-size: 18px;
    line-height: 1.33333;
    padding: 10px 16px;
}
.si-btn--primary {
    background-color: #003b66;
    border-color: #002c4d;
    color: #fff;
}
.si-btn {
    -moz-user-select: none;
    background-image: none;
    cursor: pointer;
    display: inline-block;
    font-family: Arial,Helvetica,sans-serif;
    font-weight: 400;
    margin-bottom: 0;
    text-align: center;
    vertical-align: middle;
    white-space: nowrap;
}
</style>   
    
    
 <?php include('footer.php');?>
