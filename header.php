<!DOCTYPE html>
<html class=" js no-touch cssanimations csstransitions" lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<style type="text/css">.gm-style .gm-style-mtc label,.gm-style .gm-style-mtc div{font-weight:400}</style><style type="text/css">.gm-style-pbc{transition:opacity ease-in-out;background-color:black;text-align:center}.gm-style-pbt{font-size:22px;color:white;font-family:Roboto,Arial,sans-serif;position:relative;margin:0;top:50%;-webkit-transform:translateY(-50%);-ms-transform:translateY(-50%);transform:translateY(-50%)}</style>
<link href="assets/css.css" rel="stylesheet" type="text/css">
<style type="text/css">.gm-style .gm-style-cc span,.gm-style .gm-style-cc a,.gm-style .gm-style-mtc div{font-size:10px}</style><style type="text/css">@media print {  .gm-style .gmnoprint, .gmnoprint {    display:none  }}@media screen {  .gm-style .gmnoscreen, .gmnoscreen {    display:none  }}</style>
<style type="text/css">.gm-style{font-family:Roboto,Arial,sans-serif;font-size:11px;font-weight:400;text-decoration:none}.gm-style img{max-width:none}</style>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Prototypes</title>
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!--link href="assets/font-awesome/css/font-awesome-ie7.min.css" rel="stylesheet"-->
    <!-- Jquery UI CSS -->
    <link href="assets/range-slider.css" rel="stylesheet">
    <!-- Owl Carousel CSS -->
    <link href="assets/owl.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="assets/prettyPhoto.css" rel="stylesheet">
    <!-- Bx-Slider StyleSheet CSS -->
    <link href="assets/jquery.css" rel="stylesheet"> 
    <!-- Font Awesome StyleSheet CSS -->
    <!--link href="assets/font-awesome.css" rel="stylesheet"-->

	<!-- SVG Font StyleSheet CSS -->
    <link href="assets/svg.css" rel="stylesheet">
    <!-- Chosen Script CSS -->
    <link href="assets/chosen.css" rel="stylesheet">
	<!-- DL Menu CSS -->
	<link href="assets/component.css" rel="stylesheet">
    <!-- Widget CSS -->
    <link href="assets/widget.css" rel="stylesheet">
    <!-- Typography CSS -->
    <link href="assets/typography.css" rel="stylesheet">
    <!-- Shortcodes CSS -->
    <link href="assets/shortcodes.css" rel="stylesheet">
	<!-- Custom Main StyleSheet CSS -->
    <link href="assets/style.css" rel="stylesheet">
    <!-- Color CSS -->
    <link href="assets/color.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="assets/responsive.css" rel="stylesheet">
 
  <!--script style="" src="assets/common.js" charset="UTF-8" type="text/javascript"></script><script src="assets/map.js" charset="UTF-8" type="text/javascript"></script><script src="assets/util.js" charset="UTF-8" type="text/javascript"></script><script src="assets/onion.js" charset="UTF-8" type="text/javascript"></script><script src="assets/stats.js" charset="UTF-8" type="text/javascript"></script><script src="assets/controls.js" charset="UTF-8" type="text/javascript"></script><script src="assets/marker.js" charset="UTF-8" type="text/javascript"></script-->
  </head>

  <body>

<!--Kode Wrapper Start-->  
<div class="kf_wrapper">
	
	<!--Header Wrap Start-->
    <header>
    	<!--Top Bar Wrap Start-->
        <div class="kf_top_bar">
        	<div class="container">
            	<!--Opening Time Wrap Start-->
            	<div class="kf_opening_time">
                	<p>Open Hours: Monday to Saturday - 8am to 6pm</p>
                </div>
                <!--Opening Time Wrap End-->
                <!--List Your Property Free Wrap Start-->
                <div class="kf_list_free">
                	<a href="account.php">Account</a>
                </div>
                <!--List Your Property Free Wrap Start-->
                <!--UI-Element Wrap Start-->
                <div class="kf_top_social_icon">
                	<ul>
                    	<li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>
                <!--UI-Element Wrap End-->
            </div>
        </div>
        <!--Top Bar Wrap Start-->
        
        <!--Logo & Navigation Wrap Start-->
        <div class="kf_logo_nav_wrap">
            <div class="container">
                <!--Logo Wrap Start-->
                <div class="kf_logo">
                    <a href="index.php"><img src="assets/logo.png" alt=""></a>
                </div>
                <!--Logo Wrap End-->
                <!--Navigation Wrap Start-->
                <div class="kf_main_navigation">
                    <ul>
                        <li class="active"><a href="index.php">Home</a>
							<!--ul>
								<li><a href="#">Home Style 1</a></li>
								<li><a href="http://kodeforest.net/html/property/index-2.html">Home Style 2</a></li>
								<li><a href="http://kodeforest.net/html/property/index-3.html">Home Style 3</a></li>
								<li><a href="http://kodeforest.net/html/property/index-4.html">Home Style 4</a></li>
							</ul-->
						</li>
                        <li><a href="about.php">About Us</a></li>
                        <li><a href="#">Search</a>
							<ul>
								<li><a href="search_map.php">Map Search</a></li>
								<li><a href="adv_search.php">Advanced Search</a></li>
								<!--li><a href="#"></a></li>
								<li><a href="#"></a></li-->
							</ul>
						</li>
                        <li><a href="#">Buy</a>
							<ul>
								<li><a href="home_sale.php">Homes for sale</a></li>
								<li><a href="open_house.php">Open houses</a></li>
								<li><a href="new_construct.php">New Constructions</a></li>
							</ul>
                        </li>

                        <li><a href="#">Rent</a>
							<ul>
								<li><a href="appartment.php">Apartments for rent</a></li>
								<li><a href="coming_soon.php">Houses for rent</a></li>
								<li><a href="pet.php">Pet friendly rentals</a></li>
							</ul>
                        </li>
                        <li><a href="#">Sell</a>
                        	<ul>
								<li><a href="sell_condo.php">Selling a condo</a></li>
								<li><a href="how_much_condo.php">How much is your condo worth</a></li>
							</ul>
                        </li>
                        <li><a href="blog.php">Blog</a>
							<!--ul>
								<li><a href="#">Blog Grid Style</a></li>
								<li><a href="#">Blog Listing Style 1</a></li>
								<li><a href="#">Blog Listing Style 2</a></li>
								<li><a href="#">Blog Detail Style 1</a></li>
								<li><a href="#">Blog Detail Style 2</a></li>
							</ul-->
						</li>
                        <!--li><a href="#">Search</a>
							<!--ul>
								<!--li><a href="#">Property Grid Style</a></li>
								<li><a href="#">Property Listing Style 1</a></li>
								<li><a href="#">Property Listing Style 2</a></li>
								<li><a href="#">Property Detail Style 3</a></li>
								<li><a href="#">Property Detail Style 4</a></li>
								<li><a href="#">Property Detail</a></li-->
							<!--/ul-->
						<!--/li>
                        <!--li><a href="#">Team</a>
							<ul>
								<li><a href="http://kodeforest.net/html/property/our-team.html">Our Team</a></li>
								<li><a href="http://kodeforest.net/html/property/team-detail.html">Team Detail</a></li>
							</ul>
						</li>
                        <li><a href="#">Pages</a>
							<ul>
								<li><a href="http://kodeforest.net/html/property/gallery.html">Gallery</a></li>
								<li><a href="http://kodeforest.net/html/property/submit-property.html">Submit Property</a></li>
								<li><a href="http://kodeforest.net/html/property/Price-table.html">Price Table</a></li>
								<li><a href="http://kodeforest.net/html/property/404-page.html">404 Page</a></li>
								<li><a href="http://kodeforest.net/html/property/comming-soon.html">Comming Soon</a></li>
								<li><a href="http://kodeforest.net/html/property/Login.html">Login</a></li>
								<li><a href="http://kodeforest.net/html/property/Register.html">Register</a></li>
								<li><a href="http://kodeforest.net/html/property/password-reset.html">Password Reset</a></li>
								<li><a href="http://kodeforest.net/html/property/shortcode.html">Short Code</a></li>
							</ul>
						</li-->
                        <li><a href="contact.php">Contact Us</a></li>
                    </ul>
                </div>
                <!--Navigation Wrap End-->
                <!--Call Us Wrap Start-->
                <div class="kf_callus">
                    <i class="fa fa-mobile-phone"></i>
                    <div class="kf_phone_num">
                        <span>Call Us Now</span>
                        <h6>111-222-333-444</h6>
                    </div>
                </div>
                <!--Call Us Wrap End-->
	
				
            </div>
        </div>
        <!--Logo & Navigation Wrap End-->
        
    </header>
    <!--Header Wrap End-->
    
