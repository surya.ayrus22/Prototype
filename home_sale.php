    <?php include('header.php');?>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script type="text/javascript" src="assets/jquery.pajinate.js"></script>
    <!--Sub Banner Wrap Start-->
    <div class="kf_property_sub_banner">
    	<div class="container">
        	<div class="kf_sub_banner_hdg">
            	<h3>Home for Sale</h3>
            </div>
            <div class="kf_property_breadcrumb">
            	<ul>
                	<li><a href="#">Home</a></li>
                    <li><a href="#">Home for Sale</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!--Sub Banner Wrap End-->
    
    <!--Content Wrap Start-->
    <div class="kf_property_content_wrap">
		<!--Most Recent Property Wrap Start-->
        <section class="kf_recent_property_bg">
        	<div class="container" >
               <!--Most Recent Property List Wrap Start-->
                <div class="row ">                	
                   <!-- MAP -->
                   <div class="col-md-6">
						<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d13120.789764323386!2d78.1279358!3d9.9516867!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e1!3m2!1sen!2sin!4v1471081667742" width="1982" height="750" frameborder="0" style="border:0" allowfullscreen></iframe>
                   </div>
                   <div class="col-md-6">
                   
					  <ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#home">Homes for You</a></li>
						<li><a data-toggle="tab" href="#menu1">Newest</a></a></li>
						<li><a data-toggle="tab" href="#menu2">Cheapest</a></li>
						<li><a data-toggle="tab" href="#menu3">More</a></li>
					  </ul>

					  <div class="tab-content">
						<div id="home" class="tab-pane fade in active">
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>
									<div class="kf_recent_property_des">
										<h5>$ 170,000,000</h5>
										<p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
										
										<a class="kf_md_btn kf_link_1" href="#">Book Now</a>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>
									<div class="kf_recent_property_des">
										<h5>$ 170,000,000</h5>
										<p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
										
										<a class="kf_md_btn kf_link_1" href="#">Book Now</a>
									</div>
								</div>
							</div>
							
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>
									<div class="kf_recent_property_des">
										<h5>$ 170,000,000</h5>
										<p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
										
										<a class="kf_md_btn kf_link_1" href="#">Book Now</a>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
						</div>
						<div id="menu1" class="tab-pane fade">
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>
									<div class="kf_recent_property_des">
										<h5>$ 170,000,000</h5>
										<p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
										
										<a class="kf_md_btn kf_link_1" href="#">Book Now</a>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>
									<div class="kf_recent_property_des">
										<h5>$ 170,000,000</h5>
										<p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
										
										<a class="kf_md_btn kf_link_1" href="#">Book Now</a>
									</div>
								</div>
							</div>
							
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>
									<div class="kf_recent_property_des">
										<h5>$ 170,000,000</h5>
										<p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
										
										<a class="kf_md_btn kf_link_1" href="#">Book Now</a>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
						</div>
						<div id="menu2" class="tab-pane fade">
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>
									<div class="kf_recent_property_des">
										<h5>$ 170,000,000</h5>
										<p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
										
										<a class="kf_md_btn kf_link_1" href="#">Book Now</a>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>
									<div class="kf_recent_property_des">
										<h5>$ 170,000,000</h5>
										<p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
										
										<a class="kf_md_btn kf_link_1" href="#">Book Now</a>
									</div>
								</div>
							</div>
							
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>
									<div class="kf_recent_property_des">
										<h5>$ 170,000,000</h5>
										<p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
										
										<a class="kf_md_btn kf_link_1" href="#">Book Now</a>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
						</div>
						<div id="menu3" class="tab-pane fade">
						 							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>
									<div class="kf_recent_property_des">
										<h5>$ 170,000,000</h5>
										<p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
										
										<a class="kf_md_btn kf_link_1" href="#">Book Now</a>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>
									<div class="kf_recent_property_des">
										<h5>$ 170,000,000</h5>
										<p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
										
										<a class="kf_md_btn kf_link_1" href="#">Book Now</a>
									</div>
								</div>
							</div>
							
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>
									<div class="kf_recent_property_des">
										<h5>$ 170,000,000</h5>
										<p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
										
										<a class="kf_md_btn kf_link_1" href="#">Book Now</a>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
							<div class="col-md-6 col-sm-6" >
								<div class="imagetest">
									<figure>
										<img src="assets/recent-02.jpg" alt="">
									</figure>
									<div class="kf_like_property">
										<a href="#"><i class="fa fa-heart"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
									</div>
									<div class="kf_recent_visible_des">
										<h5>$ 170,000,000</h5>
										<p>2139 Anniversary Ln, Newport Beach</p>
									</div>

								</div>
							</div>
						</div>
					  </div>
									  

                   </div>

                </div>
                <!--Most Recent Property List Wrap End-->
                
            </div>
        </section>
        <!--Most Recent Property Wrap End-->
    
	</div>
	<!-- Content Wrap End -->
	<style>
	
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    border-color: #ddd transparent !important;
}
.tab-content {
    margin-top: 15px;
}
.imagetest::before{
	background-color: rgba(0, 0, 0, 0.6);
    content: "";
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
}
.imagetest {
    float: left;
    margin: 0 0 30px;
    overflow: hidden;
    position: relative;
    width: 100%;
}
	</style>
   <?php include('footer.php');?>
