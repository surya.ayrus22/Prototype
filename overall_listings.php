    <?php include('header.php');?>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script type="text/javascript" src="assets/jquery.pajinate.js"></script>
    <!--Sub Banner Wrap Start-->
    <div class="kf_property_sub_banner">
    	<div class="container">
        	<div class="kf_sub_banner_hdg">
            	<h3>Listings</h3>
            </div>
            <div class="kf_property_breadcrumb">
            	<ul>
                	<li><a href="#">Home</a></li>
                    <li><a href="#">All Listings</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!--Sub Banner Wrap End-->
    
    <!--Content Wrap Start-->
    <div class="kf_property_content_wrap">
		<!--Most Recent Property Wrap Start-->
        <section class="kf_recent_property_bg">
        	<div class="container" id="paging_container8">
				<div class="page_navigation "></div>
                <!--Most Recent Property List Wrap Start-->
                <div class="row content">                	
                    <div class="col-md-4 col-sm-6" >
                    	<div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-02.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$ 170,000,000</h5>
                                <p>2139 Anniversary Ln, Newport Beach</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-02.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$ 170,000,000</h5>
                                <p>2139 Anniversary Ln, Newport Beach</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-02.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$ 170,000,000</h5>
                                <p>2139 Anniversary Ln, Newport Beach</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-02.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$ 170,000,000</h5>
                                <p>2139 Anniversary Ln, Newport Beach</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-02.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$ 170,000,000</h5>
                                <p>2139 Anniversary Ln, Newport Beach</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-02.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$ 170,000,000</h5>
                                <p>2139 Anniversary Ln, Newport Beach</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-02.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$ 170,000,000</h5>
                                <p>2139 Anniversary Ln, Newport Beach</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-02.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$ 170,000,000</h5>
                                <p>2139 Anniversary Ln, Newport Beach</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-02.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$ 170,000,000</h5>
                                <p>2139 Anniversary Ln, Newport Beach</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-02.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$ 170,000,000</h5>
                                <p>2139 Anniversary Ln, Newport Beach</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-02.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$ 170,000,000</h5>
                                <p>2139 Anniversary Ln, Newport Beach</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-02.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$ 170,000,000</h5>
                                <p>2139 Anniversary Ln, Newport Beach</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-02.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$ 170,000,000</h5>
                                <p>2139 Anniversary Ln, Newport Beach</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-02.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$ 170,000,000</h5>
                                <p>2139 Anniversary Ln, Newport Beach</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-02.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$ 170,000,000</h5>
                                <p>2139 Anniversary Ln, Newport Beach</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6">
                    	<div class="kf_recent_property_wrap">
                        	<figure>
                            	<img src="assets/recent-02.jpg" alt="">
                            </figure>
                            <div class="kf_like_property">
                            	<a href="#"><i class="fa fa-heart"></i></a>
                            </div>
                            <div class="kf_recent_visible_des">
                            	<h5>$ 170,000,000</h5>
                                <p>2139 Anniversary Ln, Newport Beach</p>
                            </div>
                            <div class="kf_recent_property_des">
                            	<h5>$ 170,000,000</h5>
                                <p>One morning, when Gregor Samsa woke from, he found himself transformed…</p>
                                
                                <a class="kf_md_btn kf_link_1" href="#">Book Now</a>
                            </div>
                        </div>
                    </div>

                </div>
                <!--Most Recent Property List Wrap End-->
                
            </div>
        </section>
        <!--Most Recent Property Wrap End-->
    
	</div>
	<!-- Content Wrap End -->

		<script type="text/javascript">

            
    	$(document).ready(function(){
				$('#paging_container8').pajinate({
					num_page_links_to_display : 3,
					items_per_page : 15
				});
			});     
            
      
		</script>	
			<script>
			$(document).ready(function(){
			//	$('li:odd, .content > *:odd').css('background-color','#FFD9BF');
			});
		</script>
	
  <!--Footer Widget Wrap Start-->
    <footer class="kf_footer_bg">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-3 col-sm-6">
                	<div class="kf_foo_contact">
                    	<div class="kf_footer_logo">
                        	<a href="#"><img src="assets/footer-logo.png" alt=""></a>
                            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum..</p>
                        </div>
                        <ul class="kf_foo_address">
                        	<li>
                            	<i class="fa fa-facebook"></i>
                                <span>666 71 888 &amp; 211 222 333</span>
                            </li>
                            <li>
                            	<i class="fa fa-envelope"></i>
                                <a href="#">info@kodeforest.com</a>
                            </li>
                            <li>
                            	<i class="fa fa-map-marker"></i>
                                <span>Visit Us 153 Fake Street-London United Kindom</span>
                            </li>
                        </ul>
                        <ul class="kf_foo_social_icon">
                        	<li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                	<div class="kf_foo_featured_listing widget">
                    	<h6>Featured Listing</h6>
                        <figure>
                        	<img src="assets/featured-listing-01.jpg" alt="">
                            <figcaption class="kf_foo_listing_hover">
                            	<p>$ 2,856,00</p>
                            </figcaption>
                        </figure>
                        <div class="kf_foo_listing_des">
                        	<h6><a href="#">Good Looking Family Vila</a></h6>
                            <p>2908 Rd. South Colomibanos Stree DE 12907</p>
                            <ul class="kf_foo_listing_meta">
                                <li><i class="fa fa-bed"></i>7 bd</li>
                                <li><i class="icon-bath"></i>4 ba</li>
                                <li><i class="fa fa-arrows-alt"></i>9387 sqf</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                	<div class="kf_foo_latest_wrap widget">
                    	<h6>Latest Post</h6>
                        <ul>
                        	<li>
                            	<div class="kf_foo_latest_post">
                                	<figure>
                                    	<img src="assets/latest-post-01.jpg" alt="">
                                    </figure>
                                    <div class="kf_foo_post_des">
                                    	<p><a href="#">An organization or Economic</a></p>
                                        <span>Feb 12 2016</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                            	<div class="kf_foo_latest_post">
                                	<figure>
                                    	<img src="assets/latest-post-01.jpg" alt="">
                                    </figure>
                                    <div class="kf_foo_post_des">
                                    	<p><a href="#">An organization or Economic</a></p>
                                        <span>Feb 12 2016</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                            	<div class="kf_foo_latest_post">
                                	<figure>
                                    	<img src="assets/latest-post-01.jpg" alt="">
                                    </figure>
                                    <div class="kf_foo_post_des">
                                    	<p><a href="#">An organization or Economic</a></p>
                                        <span>Feb 12 2016</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                	<div class="kf_foo_property_type widget">
                    	<h6>PROPERTY TYPES</h6>
                        <ul>
                        	<li><a href="#">Residential  <span>09</span></a></li>
                            <li><a href="#">Villa  <span>10</span></a></li>
                            <li><a href="#">Single Family  <span>11</span></a></li>
                            <li><a href="#">Condominium  <span>08</span></a></li>
                            <li><a href="#">Apartment Building  <span>03</span></a></li>
                            <li><a href="#">Apartment  <span>06</span></a></li>
                            <li><a href="#">Commercial  <span>18</span></a></li>
                            <li><a href="#">Shop  <span>12</span></a></li>
                            <li><a href="#">Office  <span>09</span></a></li>
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>
    </footer>
    <!--Footer Widget Wrap End-->
    
    <!--Copy Right Wrap Start-->
    <div class="kf_copy_right">
    	<div class="container">
        	<div class="kf_copyright_element">
            	<ul>
                	<li>Copyright@2016 <a href="#">kodeproperty</a></li>
                    <li><a href="#">Terms &amp; Coditions</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li>Designed by <a href="#"> kodeforest</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!--Copy Right Wrap Start-->
    
    <!--Footer Wrap End-->
	
</div>
<!--Kode Wrapper End-->



    <!--Jquery Library-->
    <!--script src="assets/jquery_004.js"></script>
    <!--Jquery UI Library-->
    <script src="assets/range-slider.js"></script>
	<!--Bootstrap core JavaScript-->
    <script src="assets/bootstrap.js"></script>
    <!--Bx-Slider JavaScript-->
	<script src="assets/jquery_003.js"></script>
    <!--Owl Carousel JavaScript-->
	<script src="assets/owl.js"></script-->
    <!--Pretty Photo JavaScript-->
	<script src="assets/jquery_005.js"></script>
    <!--Accordian JavaScript-->
	<script src="assets/jquery.js"></script>
    <!--Number Count (Waypoints) JavaScript-->
	<script src="assets/waypoints-min.js"></script>
    <!-- Chosen Script Javascript -->
    <script src="assets/chosen.js"></script>
	<!-- Time Counter Script Javascript -->
    <script src="assets/jquery_006.js"></script>
	<!-- Filterable Script Javascript -->
    <script src="assets/jquery-filterable.js"></script>
	<!--Dl Menu Script-->
	<script src="assets/modernizr.js"></script>
	<script src="assets/jquery_002.js"></script>
	<!--Map JavaScript-->
    <script src="assets/js"></script>
    <!--Custom JavaScript-->
	<script src="assets/custom.js"></script>
	<style>
#paging_container8 .no_more{
    background-color: white;
    color: gray;
    cursor: default;
}

.page_navigation , .alt_page_navigation{
   /* margin-bottom: 30px;*/
    margin-top: -37px;
    padding-bottom: 10px;
    float:right;
}

.page_navigation a, .alt_page_navigation a{
	padding:3px 5px;
	margin:2px;
	color:white;
	text-decoration:none;
	float: left;
	font-family: Tahoma;
	font-size: 12px;
	background-color:#DB5C04;
}
.active_page{
	background-color:white !important;
	color:black !important;
}	

.content, .alt_content{
	color: black;
}

.content li, .alt_content li, .content > p{
	padding: 5px
}
</style>
  

</body></html>
