 <?php include('header.php');?>
    <!--Sub Banner Wrap Start-->
    <div class="kf_property_sub_banner">
    	<div class="container">
        	<div class="kf_sub_banner_hdg">
            	<h3>How Much Is Your Condo Worth </h3>
            </div>
            <div class="kf_property_breadcrumb">
            	<ul>
                	<li><a href="#">Home</a></li>
                    <li><a href="#">How Much Is Your Condo Worth </a></li>
                </ul>
            </div>
        </div>
    </div>
    <!--Sub Banner Wrap End-->


    
    <!--Content Wrap Start-->
    <div class="kf_property_content_wrap">
		<!--Most Recent Property Wrap Start-->
        <section class="kf_recent_property_bg">
        	<div class="container" >
               <!--Most Recent Property List Wrap Start-->
                <div class="row ">     

<div class="col-xs-12 si-site-container col-md-8">
<div id="pageComponent6111">

      <div class="si-content-area">
<h5>What Is Your Boca Raton Condo Worth?</h5>
<p>Want to know your condos's value? Our FREE service provides you with your home's current market value and suggested selling price! Just fill out and submit the form below, and we'll analyze the comparables, local trends and other local market data to provide you with an up-to-date and accurate estimate of the worth of your condo.</p>
      </div>
<div id="pageComponent17525">

<div class="si-container si-form">
  <div class="si-row">
    <form class="js-contact-form" id="sicmForm17525" name="sicmForm17525" method="post" action="/contact/thank-you/" novalidate="novalidate">
      <input type="hidden" value="7322" name="pageid">
<input type="hidden" value="104" name="sectionid">
<input type="hidden" value="v0s1@o1pny7A74Bp10q15.p1z" name="recipient">
<input type="hidden" value="http://www.bocaluxurycondos.com/sell/how-much-is-your-condo-worth/" name="Sent_From">
<input type="hidden" value="EHKEDJMKGI" name="token">
      <input type="hidden" value="Free Market Analysis Request" name="subject">
      <input type="hidden" value="4" name="form_type">
      <div style="display: none;"><input type="text" id="your_questions" name="your_questions"></div>
      <div class="form-group si-form__column">
        <label for="First_Name" class="control-label">First Name:<span class="req">*</span></label>
        <input type="text" class="form-control" value="" name="First_Name">
      </div>
      <div class="form-group si-form__column">
        <label for="Last_Name" class="control-label">Last Name:<span class="req">*</span></label>
        <input type="text" class="form-control" value="" name="Last_Name">
      </div>
      <div class="form-group si-form__column">
        <label for="Email" class="control-label">Email Address:<span class="req">*</span></label>
        <input type="email" placeholder="name@website.com" class="form-control" value="" name="Email">
      </div>
      <div class="form-group si-form__column">
        <label for="Phone" class="control-label">Phone:<span class="req">*</span></label>
        <input type="phone" required="" placeholder="(000) 000-0000" class="form-control" value="" name="Phone" aria-required="true">
      </div>
      <div class="form-group si-form__column">
        <label for="Address" class="control-label">Address:</label>
        <input type="text" class="form-control" value="" name="Address">
      </div>
      <div class="form-group si-form__column">
        
        <div class="form-group si-form__column--city">
          <label for="City" class="control-label">City:</label>
          <input type="text" class="form-control" value="" name="City">
        </div>
        <div class="form-group si-form__column--state">
          <label for="State" class="control-label">State:</label>
          <select class="form-control" name="State">
            <option value="AL">AL</option>
            <option value="AK">AK</option>
            <option value="AZ">AZ</option>
            <option value="AR">AR</option>

            <option value="CA">CA</option>
            <option value="CO">CO</option>
            <option value="CT">CT</option>
            <option value="DE">DE</option>
            <option value="DC">DC</option>

            <option selected="" value="FL">FL</option>
            <option value="GA">GA</option>
            <option value="HI">HI</option>
            <option value="ID">ID</option>
            <option value="IL">IL</option>

            <option value="IN">IN</option>
            <option value="IA">IA</option>
            <option value="KS">KS</option>
            <option value="KY">KY</option>
            <option value="LA">LA</option>

            <option value="ME">ME</option>
            <option value="MD">MD</option>
            <option value="MA">MA</option>
            <option value="MI">MI</option>
            <option value="MN">MN</option>

            <option value="MS">MS</option>
            <option value="MO">MO</option>
            <option value="MT">MT</option>
            <option value="NE">NE</option>
            <option value="NV">NV</option>

            <option value="NH">NH</option>
            <option value="NJ">NJ</option>
            <option value="NM">NM</option>
            <option value="NY">NY</option>
            <option value="NC">NC</option>

            <option value="ND">ND</option>
            <option value="OH">OH</option>
            <option value="OK">OK</option>
            <option value="OR">OR</option>
            <option value="PA">PA</option>

            <option value="RI">RI</option>
            <option value="SC">SC</option>
            <option value="SD">SD</option>
            <option value="TN">TN</option>
            <option value="TX">TX</option>

            <option value="UT">UT</option>
            <option value="VT">VT</option>
            <option value="VA">VA</option>
            <option value="WA">WA</option>
            <option value="WV">WV</option>

            <option value="WI">WI</option>
            <option value="WY">WY</option>
          </select>
        </div>
        <div class="form-group si-form__column--zip">
          <label for="Zip_Code" class="control-label">Zip:</label>
          <input type="text" class="form-control" name="Zip_Code">
        </div>
        
      </div>
      <div class="form-group si-form__column">
        <label for="Moving_When" class="control-label">I am planning to move:</label>
        <select class="form-control" name="Moving_When">
          <option value="">Select One</option>							
          <option value="not sure">Not Sure</option>
          <option value="right away">Right Away</option>
          <option value="3 months">Within 3 months</option>
          <option value="6 months">Within 6 months</option>
          <option value="9 months">Within 9 months</option>
          <option value="1 year">Within the next year</option>
        </select>
      </div>
      <div class="form-group si-form__column">
        <label for="Interested_In" class="control-label">I am Interested in:</label>
        <select class="form-control" name="Interested_In">
          <option value="">Select One</option>
          <option value="Selling">Selling</option>  
          <option value="Buying">Buying</option>
          <option value="Buying and Selling">Buying &amp; Selling</option>			
          <option value="Leasing (Landlord)">Leasing (Landlord)</option>
          <option value="Leasing (Tenant)">Leasing (Tenant)</option>
          <option value="Broker / Agent">Broker / Agent</option>
        </select>
      </div>
      <div class="form-group si-form__full">
        <h5 class="">Describe Your Property</h5>
      </div>
      <div class="form-group si-form__column">
        <label for="Property_Type" class="control-label">Property Type:</label>
        <select class="form-control" name="Property_Type">
          <option value="">Select One</option>
          <option value="Single Family">Single Family</option>
          <option value="Multi Family">Multi Family</option>
          <option value="Town Home">Town Home</option>
          <option value="Condominium">Condominium</option>
          <option value="Land">Land</option>
          <option value="Commercial">Commercial</option>
          <option value="Investment">Investment</option>
          <option value="Residential">Residential</option>
          <option value="Patio Home">Patio Home</option>
        </select>
      </div>
      <div class="form-group si-form__column">
        <div class="si-form__column--left">
          <label for="Bedrooms" class="control-label">Bedrooms:</label>
          <select class="form-control" name="Bedrooms">
            <option value="">Any</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select>
        </div>
        <div class="si-form__column--right">
          <label for="Bathrooms" class="control-label">Bathrooms:</label>	
          <select class="form-control" name="Bathrooms">
            <option value="">Any</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select>
        </div>
      </div>
      <div class="form-group si-form__column">
        <div class="si-form__column--left">
          <label for="Square_Feet" class="control-label">Square feet:</label>
          <select class="form-control" name="Square_Feet">
            <option value="">Select One</option>			
            <option value="500">500</option>
            <option value="1000">1000</option>
            <option value="1500">1500</option>
            <option value="2000">2000</option>
            <option value="2500">2500</option>
            <option value="3000">3000</option>
            <option value="3500">3500</option>
            <option value="4000">4000</option>
            <option value="4500">4500</option>
            <option value="5000">5000</option>
            <option value="5500">5500</option>
            <option value="6000">6000</option>
            <option value="6500">6500</option>
            <option value="7000">7000</option>
          </select>
        </div>
        <div class="si-form__column--right">
          <label for="Garage_Capacity" class="control-label">Garage space:</label>
          <select class="form-control" name="Garage_Capacity">
            <option value="">Select One</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
          </select>
        </div>
      </div>
      <div class="form-group si-form__column">
        <div class="si-form__column--left">
          <label for="Basement" class="control-label">Basement:</label>
          <select class="form-control" name="Basement">
            <option value="">Select One</option>			
            <option value="No Basement">No Basement</option>
            <option value="Finished">Finished</option>
            <option value="Partially Finished">Partially Finished</option>
            <option value="Unfinished">Unfinished</option>
          </select>
        </div>
        <div class="si-form__column--right">
          <label for="Year_Built" class="control-label">Year Built:</label>
          <input type="text" class="form-control" name="Year_Built">
        </div>
      </div>
      <div class="form-group si-form__full">
        <label for="Other_Features" class="control-label">Other features I think are important (i.e. Main Floor Master, home theater, etc.):</label>
        <textarea class="form-control" name="Other_Features"></textarea>
      </div>
      <div class="form-group si-form__full">
        <label for="What_I_Love" class="control-label">What I love about my house:</label>
        <textarea class="form-control" name="What_I_Love"></textarea>
      </div>
      <div class="form-group si-form__full">
        <button class="si-btn si-btn--secondary" type="submit">Submit</button>
      </div>
    </form>
  </div>
</div>
    </div>
    </div> 
    
        
</div>

            <div class="col-xs-12 col-md-4 si-site-aside">
  <aside class="inner-sidebar">
    <div class="row">
      <div class="col-xs-12">
        <div class="square square--se">
          <div class="square__shell js-inner-search">
<div class="square__title js-qs-count">Search </div>

<div class="square__content">
  <div class="search-pane search-pane--inner">

    
  <form>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="kf_search_field">
                                            <!--input name="property_id" type="text"-->
                                            <input placeholder="Search by Zip Code, Address or MLS #" spellcheck="false" autocapitalize="off" autocorrect="off" autocomplete="off" class="js-qs-loc ui-autocomplete-input" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="kf_search_field">
											<select style="display: none;" sb="97056361" class="chosen-select" data-name="lprice">
												<option value="">Min Price</option>
												<option value="50000">$50,000</option>
												<option value="60000">$60,000</option>
												<option value="70000">$70,000</option>
												<option value="80000">$80,000</option>
												<option value="90000">$90,000</option>
												<option value="100000">$100,000</option>
												<option value="125000">$125,000</option>
												<option value="150000">$150,000</option>
												<option value="175000">$175,000</option>
												<option value="200000">$200,000</option>
												<option value="225000">$225,000</option>
												<option value="250000">$250,000</option>
												<option value="275000">$275,000</option>
												<option value="300000">$300,000</option>
												<option value="325000">$325,000</option>
												<option value="350000">$350,000</option>
												<option value="375000">$375,000</option>
												<option value="400000">$400,000</option>
												<option value="425000">$425,000</option>
												<option value="450000">$450,000</option>
												<option value="475000">$475,000</option>
												<option value="500000">$500,000</option>
												<option value="550000">$550,000</option>
												<option value="600000">$600,000</option>
												<option value="650000">$650,000</option>
												<option value="700000">$700,000</option>
												<option value="750000">$750,000</option>
												<option value="800000">$800,000</option>
												<option value="850000">$850,000</option>
												<option value="900000">$900,000</option>
												<option value="950000">$950,000</option>
												<option value="1000000">$1,000,000</option>
												<option value="1250000">$1,250,000</option>
												<option value="1500000">$1,500,000</option>
												<option value="1750000">$1,750,000</option>
												<option value="2000000">$2,000,000</option>
												<option value="2250000">$2,250,000</option>
												<option value="2500000">$2,500,000</option>
												<option value="2750000">$2,750,000</option>
												<option value="3000000">$3,000,000</option>
												<option value="3250000">$3,250,000</option>
												<option value="3500000">$3,500,000</option>
												<option value="3750000">$3,750,000</option>
												<option value="4000000">$4,000,000</option>
												<option value="4250000">$4,250,000</option>
												<option value="4500000">$4,500,000</option>
												<option value="4750000">$4,750,000</option>
												<option value="5000000">$5,000,000</option>
												<option value="5500000">$5,500,000</option>
												<option value="6000000">$6,000,000</option>
												<option value="6500000">$6,500,000</option>
												<option value="7000000">$7,000,000</option>
												<option value="7500000">$7,500,000</option>
												<option value="8000000">$8,000,000</option>
												<option value="8500000">$8,500,000</option>
												<option value="9000000">$9,000,000</option>
												<option value="9500000">$9,500,000</option>
												<option value="10000000">$10,000,000</option>
												<option value="11000000">$11,000,000</option>
												<option value="12000000">$12,000,000</option>
												<option value="13000000">$13,000,000</option>
												<option value="14000000">$14,000,000</option>
												<option value="15000000">$15,000,000</option>
												<option value="16000000">$16,000,000</option>
												<option value="17000000">$17,000,000</option>
												<option value="18000000">$18,000,000</option>
												<option value="19000000">$19,000,000</option>
												<option value="20000000">$20,000,000</option>
											</select>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="kf_search_field">
											<select style="display: none;" sb="3904622" class="chosen-select" data-name="uprice">
												<option value="">Max Price</option>
												<option value="50000">$50,000</option>
												<option value="60000">$60,000</option>
												<option value="70000">$70,000</option>
												<option value="80000">$80,000</option>
												<option value="90000">$90,000</option>
												<option value="100000">$100,000</option>
												<option value="125000">$125,000</option>
												<option value="150000">$150,000</option>
												<option value="175000">$175,000</option>
												<option value="200000">$200,000</option>
												<option value="225000">$225,000</option>
												<option value="250000">$250,000</option>
												<option value="275000">$275,000</option>
												<option value="300000">$300,000</option>
												<option value="325000">$325,000</option>
												<option value="350000">$350,000</option>
												<option value="375000">$375,000</option>
												<option value="400000">$400,000</option>
												<option value="425000">$425,000</option>
												<option value="450000">$450,000</option>
												<option value="475000">$475,000</option>
												<option value="500000">$500,000</option>
												<option value="550000">$550,000</option>
												<option value="600000">$600,000</option>
												<option value="650000">$650,000</option>
												<option value="700000">$700,000</option>
												<option value="750000">$750,000</option>
												<option value="800000">$800,000</option>
												<option value="850000">$850,000</option>
												<option value="900000">$900,000</option>
												<option value="950000">$950,000</option>
												<option value="1000000">$1,000,000</option>
												<option value="1250000">$1,250,000</option>
												<option value="1500000">$1,500,000</option>
												<option value="1750000">$1,750,000</option>
												<option value="2000000">$2,000,000</option>
												<option value="2250000">$2,250,000</option>
												<option value="2500000">$2,500,000</option>
												<option value="2750000">$2,750,000</option>
												<option value="3000000">$3,000,000</option>
												<option value="3250000">$3,250,000</option>
												<option value="3500000">$3,500,000</option>
												<option value="3750000">$3,750,000</option>
												<option value="4000000">$4,000,000</option>
												<option value="4250000">$4,250,000</option>
												<option value="4500000">$4,500,000</option>
												<option value="4750000">$4,750,000</option>
												<option value="5000000">$5,000,000</option>
												<option value="5500000">$5,500,000</option>
												<option value="6000000">$6,000,000</option>
												<option value="6500000">$6,500,000</option>
												<option value="7000000">$7,000,000</option>
												<option value="7500000">$7,500,000</option>
												<option value="8000000">$8,000,000</option>
												<option value="8500000">$8,500,000</option>
												<option value="9000000">$9,000,000</option>
												<option value="9500000">$9,500,000</option>
												<option value="10000000">$10,000,000</option>
												<option value="11000000">$11,000,000</option>
												<option value="12000000">$12,000,000</option>
												<option value="13000000">$13,000,000</option>
												<option value="14000000">$14,000,000</option>
												<option value="15000000">$15,000,000</option>
												<option value="16000000">$16,000,000</option>
												<option value="17000000">$17,000,000</option>
												<option value="18000000">$18,000,000</option>
												<option value="19000000">$19,000,000</option>
												<option value="20000000">$20,000,000</option>
											</select>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="kf_search_field">
											<select style="display: none;" sb="81264612" class="chosen-select" data-name="lbeds">
												<option value="">Beds</option>
												<option value="1">1+</option>
												<option value="2">2+</option>
												<option value="3">3+</option>
												<option value="4">4+</option>
												<option value="5">5+</option>
												<option value="6">6+</option>
											</select>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="kf_search_field">
											<select style="display: none;" sb="16590636" class="chosen-select" data-name="lbaths">
												<option value="">Baths</option>
												<option value="1">1+</option>
												<option value="2">2+</option>
												<option value="3">3+</option>
												<option value="4">4+</option>
												<option value="5">5+</option>
												<option value="6">6+</option>
											</select>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="kf_search_field">
                                            <select style="display: none;" class="chosen-select">
                                                <option >Any</option>
                                                <option>Commercial</option>
                                                <option>-Office</option>
												<option>-Shop</option>
                                                <option>-Residential</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="kf_search_field">
   											<select style="display: none;" sb="19377466" class="chosen-select" data-name="lsqft">
												<option value="">Square Feet</option>
												<option value="500">500+</option>
												<option value="1000">1000+</option>
												<option value="1500">1500+</option>
												<option value="2000">2000+</option>
												<option value="2500">2500+</option>
												<option value="3000">3000+</option>
												<option value="3500">3500+</option>
												<option value="4000">4000+</option>
												<option value="4500">4500+</option>
												<option value="5000">5000+</option>
												<option value="5500">5500+</option>
												<option value="6000">6000+</option>
												<option value="6500">6500+</option>
												<option value="7000">7000+</option>
											</select>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12">
                                        <div class="kf_search_field">
                                            <input value="More Search options" type="submit">
                                        </div>
                                    </div>

 
                                </div>
                            </form>
 


  </div>
</div></div>
        </div>
      </div>
    </div>



  </aside>
</div>
          </div>
        </div>
      </div>
    </div>
    
 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Activate Your Free Account for Full Access</h4>
        </div>
		<div class="modal-body">
              <div class="si-form__error" style="display: none" id="authRegisterMessage"></div>
              <div class="si-row">
                <div class="form-group si-form__column">
                  <label for="authRegisterFirstName" class="control-label">First Name:<span class="req">*</span></label>
                  <input type="text" class="form-control" value="" id="authRegisterFirstName" name="firstName" tabindex="1" aria-required="true">
                </div>
                <div class="form-group si-form__column">
                  <label for="authRegisterLastName" class="control-label">Last Name:<span class="req">*</span></label>
                  <input type="text" class="form-control" value="" id="authRegisterLastName" name="lastName" tabindex="2" aria-required="true">
                </div>
                <div class="form-group si-form__full">
                  <label for="authRegisterEmail" class="control-label">Email:<span class="req">*</span></label>
                  <input type="email" placeholder="This will also be your sign in name" class="form-control" value="" id="authRegisterEmail" name="email" tabindex="3" aria-required="true">
                </div>
              
                <div class="form-group si-form__full">
                  <label for="authRegisterPhone" class="control-label">Phone:<span class="req">*</span></label>
                  <input type="phone" placeholder="(000) 000-0000" required="" class="form-control" id="authRegisterPhone" name="phone" tabindex="5" aria-required="true">
                </div>
              
                <div class="form-group si-form__full" id="authAgentListPanel">
                  <label for="authRegisterAgents" class="control-label">Currently working with one of our agents?<span class="req">*</span></label>
                  <select title="Please select an agent" required="" class="form-control" id="authRegisterAgents" tabindex="6" aria-required="true">
                    <option value="">Select an Option</option>
                    <option value="-1">Not at This Time</option>
                    
                    <option value="514">Karen Anderson</option>
                    
                    <option value="516">Claude Champagne</option>
                    
                    <option value="614">Agent Connect</option>
                    
                    <option value="536">Gilles Dalco</option>
                    
                    <option value="700">Michele Davenport</option>
                    
                    <option value="689">Laurel Dunay</option>
                    
                    <option value="538">Nancy Frey</option>
                    
                    <option value="553">Olivier Hannoun</option>
                    
                    <option value="707">Tommy Holmes</option>
                    
                    <option value="589">Rori Huberman</option>
                    
                    <option value="606">Josh Kallan</option>
                    
                    <option value="708">Marjorie Kent</option>
                    
                    <option value="600">Karen Knox</option>
                    
                    <option value="729">Rachael Kobin</option>
                    
                    <option value="715">Jared Leshner</option>
                    
                    <option value="588">Ryan Neill</option>
                    
                    <option value="604">Shari Orland</option>
                    
                    <option value="510">Caesar Parisi</option>
                    
                    <option value="542">Dina Schwartz</option>
                    
                    <option value="714">Charlene Singer</option>
                    
                    <option value="699">Maxine Sloane</option>
                    
                    <option value="601">Laura Trebatch</option>
                    
                  </select>
                </div>
              
                <div style="display:none;" class="form-group si-form__full" id="authAgentNamePanel">
                  <label for="authRegisterAgentName" class="control-label">Working with Another Agent? If so, Who?</label>
                  <input type="text" placeholder="Agent Name" class="form-control" id="authRegisterAgentName" tabindex="6">
                </div>
              
              </div>
            </div>
<div class="modal-footer">
              <button class="si-btn si-btn--primary si-btn--lg" tabindex="7" id="authRegisterSubmit" type="button">Create Free Account</button>
              <!--button class="si-btn si-btn--link si-btn--lg si-btn--block js-signin-link" tabindex="8" type="button">Returning User? Log In Here</button-->
              <button type="button" class="si-btn si-btn--link si-btn--lg si-btn--block js-signin-link"  id="Login">Returning User? Log In Here</button>
            </div>
      </div>
      
    </div>
  </div>
   <?php include('footer.php');?>

    <!-- Modal -->
  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" id="close">&times;</button>
          <h4 class="modal-title">Activate Your Free Account for Full Access</h4>
        </div>
<form class="si-form" name="authSigninForm" id="authSigninForm" novalidate="novalidate">
            <div class="modal-body">
              <div class="si-form__error" style="display: none" id="authSigninMessage"></div>
              <div class="si-row">
                <div class="form-group si-form__full">
                  <label for="authSigninEmail" class="control-label">Email:<span class="req">*</span></label>
                  <input type="email" placeholder="name@website.com" class="form-control" value="" id="authSigninEmail" name="email" tabindex="11">
                </div>
              
              </div>
            </div>
            <div class="modal-footer">
              <button class="si-btn si-btn--primary si-btn--lg" tabindex="13" id="authSigninSubmit" data-toggle="modal" type="button">Login</button>
            
            </div>
          </form>

      </div>      
    </div>
    </section
  </div>  
 <script>
// $(document).ready(function(){
	$("#Login").click(function(){
		$("#myModal").modal("hide");
		$("#myModal1").modal("show");
	});
	$("#close").click(function(){
		location.reload();
	});	
// });
 </script> 
<style>
.modal-title {
    color: #000 !important;
}
.control-label {
color: #333 !important;
}
.modal-footer {
    text-align: center;
}
.si-property-tracker__info {
    margin-bottom: 10px;
    margin-top: 10px;
}
.si-container {
    color: #fff;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 14px;
    line-height: 1.42857;
    margin: 20px 0;
    text-align: left;
}
.si-row {
 /*   margin-left: -15px;
    margin-right: -15px; */
}
.si-container * {
    font-family: Arial,Helvetica,sans-serif;
}
.si-property-tracker__section--welcome .si-property-tracker__section-in {
    float: left;
    min-height: 1px;
    padding-left: 15px;
    padding-right: 15px;
    position: relative;
    width: 50%;
}
.si-property-tracker__section--welcome .si-property-tracker__section-in > div {
    text-align: center;
}
.si-property-tracker__section-in > div {
    border: 1px solid #6692b2;
    padding: 25px 15px;
}
.si-property-tracker__section-in > div > h4::after, .si-property-tracker__section-in > div > h4::before {
    content: " ";
    display: table;
}
.si-property-tracker__section-in > div > h4::after, .si-property-tracker__section-in > div > h4::before {
    content: " ";
    display: table;
}
*::before, *::after {
    box-sizing: border-box;
}
.si-property-tracker__section-in > div > h4 {
    background-color: #fff;
    float: left;
    font-size: 18px;
    margin-top: -35px;
    padding: 0 10px;
    text-align: left;
}
.si-container .h1, .si-container .h2, .si-container .h3, .si-container .h4, .si-container .h5, .si-container .h6, .si-container h1, .si-container h2, .si-container h3, .si-container h4, .si-container h5, .si-container h6 {
    font-style: normal;
    font-weight: 400;
}
.si-property-tracker__section--welcome .si-property-tracker__section-in > div .si-btn {
    font-size: 17px;
    font-weight: 700;
    padding: 10px 20px;
}
button, html input[type="button"], input[type="reset"], input[type="submit"] {
    cursor: pointer;
}
.si-btn--secondary {
    background-color: #0070BB;
    border-color: #0070BB;
    color: #fff;
}
.si-btn {
    -moz-user-select: none;
    background-image: none;
    border-radius: 4px;
    display: inline-block;
    font-family: Arial,Helvetica,sans-serif;
     line-height: 1.42857;
    margin-bottom: 0;
    text-align: center;
    vertical-align: middle;
    white-space: nowrap;
}
.si-property-tracker__info > div {
    margin-bottom: 25px;
}
.fa-envelope::before {
    content: "";
}
.si-property-tracker__info > div > i {
    color: #0070BB;
    float: left;
    font-size: 4em;
    margin-right: 15px;
}
.fa {
    display: inline-block;
    font-family: FontAwesome;
    font-feature-settings: normal;
    font-kerning: auto;
    font-language-override: normal;
    font-size-adjust: none;
    font-stretch: normal;
    font-style: normal;
    font-synthesis: weight style;
    font-variant: normal;
    font-weight: normal;
    line-height: 1;
    text-rendering: auto;
}
.si-property-tracker__info > div > strong {
    display: block;
}
.si-site-aside.col-md-4 {
    width: 23%;
}
.si-site-container.col-md-8 {
    width: 77%;
}
.si-btn--lg {
    border-radius: 6px;
    font-size: 18px;
    line-height: 1.33333;
    padding: 10px 16px;
}
.si-btn--primary {
    background-color: #003b66;
    border-color: #002c4d;
    color: #fff;
}
.si-btn {
    -moz-user-select: none;
    background-image: none;
    cursor: pointer;
    display: inline-block;
    font-family: Arial,Helvetica,sans-serif;
    font-weight: 400;
    margin-bottom: 0;
    text-align: center;
    vertical-align: middle;
    white-space: nowrap;
}
button, select {
    text-transform: none;
}
button {
    overflow: visible;
}
.si-form__column {
    float: left;
    width: 50%;
}
.si-form__column {
    min-height: 1px;
    padding-left: 15px;
    padding-right: 15px;
    position: relative;
}
.si-container * {
    font-family: Arial,Helvetica,sans-serif;
}
label {
    display: inline-block;
    font-weight: bold;
    margin-bottom: 5px;
    max-width: 100%;
}
si-form .form-control {
    border-radius: 4px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    color: #333;
    font-size: 14px;
    font-weight: 400;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
}
.si-container * {
    font-family: Arial,Helvetica,sans-serif;
}
.form-control {
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    display: block;
    height: 44px;
    line-height: 1.42857;
    padding: 6px 12px;
    width: 100%;
}
.si-form__full {
    float: left;
    min-height: 1px;
    padding-left: 15px;
    padding-right: 15px;
    position: relative;
    width: 100%;
}
textarea.form-control {
    height: auto;
}
.si-btn.si-btn--secondary {
    padding: 10px;
}
.si-content-area p {
    margin: 0 0 20px;
}
.si-content-area img.img_box_center, .si-content-area img.img_box_left, .si-content-area img.img_box_right {
    border-radius: 1px;
    border-width: 1px;
}
.si-content-area img.img_box_right {
    float: right;
    margin-left: 20px;
    margin-right: 0;
}
.si-content-area img.img_box_center, .si-content-area img.img_box_left, .si-content-area img.img_box_right {
    display: block;
}
.si-form .form-control {
    border-radius: 4px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    color: #333;
    font-size: 14px;
    font-weight: 400;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
}
.si-form__column--city {
    float: left !important;
    padding-right: 10px;
    width: 40%;
}
.si-form__column--state {
    float: left !important;
    padding-right: 10px;
    width: 30%;
}
.si-form__column--zip {
    float: right !important;
    width: 30%;
}
.si-form__column--left {
    float: left !important;
    padding-right: 10px;
    width: 50%;
}
.si-form__column--right {
    float: right !important;
    padding-left: 10px;
    width: 50%;
}
</style>
