    <?php include('header.php');?>
    <!--Sub Banner Wrap Start-->
    <div class="kf_property_sub_banner">
    	<div class="container">
        	<div class="kf_sub_banner_hdg">
            	<h3>Gallery</h3>
            </div>
            <div class="kf_property_breadcrumb">
            	<ul>
                	<li><a href="#">Home</a></li>
                    <li><a href="#">Gallery</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!--Sub Banner Wrap End-->
    
    <!--Content Wrap Start-->
    <div class="kf_property_content_wrap">
		<!--Gallery Wrap Start-->
		<section>
			<div class="container">
				<div class="row">
					<ul id="filterable-item-filter-1">
						<li><a data-value="all">All</a></li>
						<li><a data-value="1">Rent</a></li>
						<li><a data-value="2">Sale</a></li>
						<li><a data-value="3">Leased</a></li>
						<li><a data-value="4">Featured</a></li>
						<li><a data-value="5">Sold</a></li>
					</ul>

					<div class="masoned" style="position: relative; height: 1266px;" id="filterable-item-holder-1">
						<div style="position: absolute; left: 0px; top: 0px;" class="filterable-item all 1 col-md-4 col-sm-6 col-xs-12">
							<div class="edu_masonery_thumb">
								<figure>
									<img src="assets/gallery-07.jpg" alt="">
									<figcaption><a href="#">Introduction of University</a></figcaption>
									<a href="http://kodeforest.net/html/property/extra-images/gallery-07.jpg" data-rel="prettyPhoto"><i class="fa fa-search"></i></a>
								</figure>
							</div>	
						</div>


						<div style="position: absolute; left: 390px; top: 0px;" class="filterable-item all 2 1 5 col-md-4 col-sm-6 col-xs-12">
							<div class="edu_masonery_thumb">
								<figure>
									<img src="assets/gallery-01.jpg" alt="">
									<figcaption><a href="#">Introduction of University</a></figcaption>
									<a href="http://kodeforest.net/html/property/extra-images/gallery-01.jpg" data-rel="prettyPhoto"><i class="fa fa-search"></i></a>
								</figure>
							</div>	
						</div>


						<div style="position: absolute; left: 780px; top: 0px;" class="filterable-item all 3 2 5 col-md-4 col-sm-6 col-xs-12">
							<div class="edu_masonery_thumb">
								<figure>
									<img src="assets/gallery-02.jpg" alt="">
									<figcaption><a href="#">Introduction of University</a></figcaption>
									<a href="http://kodeforest.net/html/property/extra-images/gallery-02.jpg" data-rel="prettyPhoto"><i class="fa fa-search"></i></a>
								</figure>
							</div>	
						</div>


						<div style="position: absolute; left: 390px; top: 342px;" class="filterable-item all 4 2 3 col-md-4 col-sm-6 col-xs-12">
							<div class="edu_masonery_thumb">
								<figure>
									<img src="assets/gallery-03.jpg" alt="">
									<figcaption><a href="#">Introduction of University</a></figcaption>
									<a href="http://kodeforest.net/html/property/extra-images/gallery-03.jpg" data-rel="prettyPhoto"><i class="fa fa-search"></i></a>
								</figure>
							</div>	
						</div>

						<div style="position: absolute; left: 0px; top: 431px;" class="filterable-item all 5 1 2 col-md-4 col-sm-6 col-xs-12">
							<div class="edu_masonery_thumb">
								<figure>
									<img src="assets/gallery-04.jpg" alt="">
									<figcaption><a href="#">Introduction of University</a></figcaption>
									<a href="http://kodeforest.net/html/property/extra-images/gallery-04.jpg" data-rel="prettyPhoto"><i class="fa fa-search"></i></a>
								</figure>
							</div>	
						</div>
						<div style="position: absolute; left: 780px; top: 431px;" class="filterable-item all 3 2 4 col-md-4 col-sm-6 col-xs-12">
							<div class="edu_masonery_thumb">
								<figure>
									<img src="assets/gallery-05.jpg" alt="">
									<figcaption><a href="#">Introduction of University</a></figcaption>
									<a href="http://kodeforest.net/html/property/extra-images/gallery-05.jpg" data-rel="prettyPhoto"><i class="fa fa-search"></i></a>
								</figure>
							</div>	
						</div>
						<div style="position: absolute; left: 0px; top: 681px;" class="filterable-item all 4 5 1 col-md-4 col-sm-6 col-xs-12">
							<div class="edu_masonery_thumb">
								<figure>
									<img src="assets/rent-09.jpg" alt="">
									<figcaption><a href="#">Introduction of University</a></figcaption>
									<a href="http://kodeforest.net/html/property/extra-images/rent-09.jpg" data-rel="prettyPhoto"><i class="fa fa-search"></i></a>
								</figure>
							</div>	
						</div>
						<div style="position: absolute; left: 390px; top: 735px;" class="filterable-item all 3 5 2 col-md-4 col-sm-6 col-xs-12">
							<div class="edu_masonery_thumb">
								<figure>
									<img src="assets/gallery-06.jpg" alt="">
									<figcaption><a href="#">Introduction of University</a></figcaption>
									<a href="http://kodeforest.net/html/property/extra-images/gallery-06.jpg" data-rel="prettyPhoto"><i class="fa fa-search"></i></a>
								</figure>
							</div>	
						</div>
						<div style="position: absolute; left: 780px; top: 835px;" class="filterable-item all 2 4 1 5 col-md-4 col-sm-6 col-xs-12">
							<div class="edu_masonery_thumb">
								<figure>
									<img src="assets/gallery-07.jpg" alt="">
									<figcaption><a href="#">Introduction of University</a></figcaption>
									<a href="http://kodeforest.net/html/property/extra-images/gallery-07.jpg" data-rel="prettyPhoto"><i class="fa fa-search"></i></a>
								</figure>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--Gallery Wrap End-->
<?php include('footer.php');?>
