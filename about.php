   <?php include('header.php');?>
	<style>
	
	</style>
   <!--Sub Banner Wrap Start-->
    <div class="kf_property_sub_banner">
    	<div class="container">
        	<div class="kf_sub_banner_hdg">
            	<h3>About us</h3>
            </div>
            <div class="kf_property_breadcrumb">
            	<ul>
                	<li><a href="#">Home</a></li>
                    <li><a href="#">About Us</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!--Sub Banner Wrap End-->
    
    <!--Content Wrap Start-->
    <div class="kf_property_content_wrap">
    	<!--About Us Wrap Start-->
        <section class="kf_aboutus_video_bg">
        	<div class="container">
            	<!--Video Wrap Start-->
            	<div class="kf_aboutus_des_wrap">
                	<div class="row">
                    	<div class="col-md-6">
                        	<div class="kf_aboutus_video_wrap">
                            	<img src="assets/about-video-01.jpg" alt="">
								<div class="kf_video_icon">
									<a data-rel="prettyPhoto" href="http://kodeforest.net/html/property/extra-images/about-video-01.jpg"><i class="fa fa-play"></i></a>
								</div>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                        	<div class="kf_aboutus_video_des">
                            	<h5>We are the Most Professional Property Company in America Right Now!!</h5>
                                <p>There are many variations of passages
 of Lorem Ipsum available, but the majority have suffered alteration in 
some form, by injected humour, or randomised words which don't look even
 slightly believable. If you are going to use a. There are many 
variations of passages of Lorem Ipsum available, but the majority have 
suffered alteration in some form, by injected...</p>
                                <ul class="kf_aboutus_list">
                                	<li>Professional Variations</li>
                                    <li>Happy Clients</li>
                                    <li>Guaranteed</li>
                                    <li>Clean design</li>
                                    <li>Consulting</li>
                                    <li>Advertise</li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <!--Video Wrap End-->
                
                <!--Services List Wrap Start-->
                <div class="row">
                	<div class="col-md-3 col-sm-6">
                    	<div class="kf_property_services_wrap">
                        	<i class="icon-money"></i>
                            <h5><a href="#">Matching Buyer</a></h5>
                            <p>One morning, when Gregor Samsa woke from dreams, he transformed…  </p>
                            <a class="kf_sm_btn kf_link_1" href="#">See More</a>
                        </div>
                    </div>
                    
                    <div class="col-md-3 col-sm-6">
                    	<div class="kf_property_services_wrap">
                        	<i class="icon-apartment"></i>
                            <h5><a href="#">Market Appraisals</a></h5>
                            <p>The bedding was hardly able to cover it and seemed ready to slide off…  </p>
                            <a class="kf_sm_btn kf_link_1" href="#">See More</a>
                        </div>
                    </div>
                    
                    <div class="col-md-3 col-sm-6">
                    	<div class="kf_property_services_wrap">
                        	<i class="icon-city"></i>
                            <h5><a href="#">Property Management</a></h5>
                            <p>He lay on his armour-like back, and if he lifted his head a little see…  </p>
                            <a class="kf_sm_btn kf_link_1" href="#">See More</a>
                        </div>
                    </div>
                    
                    <div class="col-md-3 col-sm-6">
                    	<div class="kf_property_services_wrap">
                        	<i class="icon-money"></i>
                            <h5><a href="#">Sale &amp; Purches Property</a></h5>
                            <p>Aliquam vitae sem id sem efficitur interdum. Phasellus ut nulla see… </p>
                            <a class="kf_sm_btn kf_link_1" href="#">See More</a>
                        </div>
                    </div>
                </div>
                <!--Services List Wrap End-->
            </div>
        </section>
        <!--About Us Wrap End-->
        
        <!--Leased And Sold Property Wrap Start-->
        <!--div class="kf_leased_property_bg">
            <div class="kf_leased_property">
                <h2>105</h2>
                <span class="kf_property_line white_color"></span>
                <h3>Leased Properties</h3>
                <a class="kf_sm_btn kf_link_1" href="#">View All</a>
            </div>
            
            <div class="kf_leased_property kf_sold_property">
                <h2>105</h2>
                <span class="kf_property_line white_color"></span>
                <h3>Leased Properties</h3>
                <a class="kf_sm_btn kf_link_1" href="#">View All</a>
            </div>
            
        </div-->
        <!--Leased And Sold Property Wrap End-->
        
        <!--Our Agent Warp Start-->
        <section class="kf_our_agent_bg kf_aboutus_our_agent">
        	<div class="container">
            	<!--Heading Wrap Start-->
            	<div class="kf_heading_1">
                    <h3>Our Agent</h3>
                    <p>Lorem Ipsum orem  simply dummy </p>
                    <span class="kf_property_line"></span>
                </div>
                <!--Heading Wrap End-->
                
                <!--Agent List Wrap Start-->
                <div style="opacity: 1; display: block;" class="kf_agent_slider owl-carousel owl-theme">
                	<div class="owl-wrapper-outer"><div style="width: 3420px; left: 0px; display: block; " class="owl-wrapper"><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_agent_wrap">
                            <figure>
                                <img src="assets/agent-01.jpg" alt="">
								<figcaption>
									<span class="kf_listing_overlay"></span>
								</figcaption>
                            </figure>
                            <div class="kf_agent_des">
                                <h6><a href="#">Michael Smart</a></h6>
                                <span class="kf_property_line"></span>
                                <p>Expert Agent</p>
                            </div>
                        </div>
                    </div></div><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_agent_wrap">
                            <figure>
                                <img src="assets/agent-02.jpg" alt="">
								<figcaption>
									<span class="kf_listing_overlay"></span>
								</figcaption>
                            </figure>
                            <div class="kf_agent_des">
                                <h6><a href="#">Michael Smart</a></h6>
                                <span class="kf_property_line"></span>
                                <p>Expert Agent</p>
                            </div>
                        </div>
                    </div></div><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_agent_wrap">
                            <figure>
                                <img src="assets/agent-03.jpg" alt="">
								<figcaption>
									<span class="kf_listing_overlay"></span>
								</figcaption>
                            </figure>
                            <div class="kf_agent_des">
                                <h6><a href="#">Michael Smart</a></h6>
                                <span class="kf_property_line"></span>
                                <p>Expert Agent</p>
                            </div>
                        </div>
                    </div></div><div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_agent_wrap">
                            <figure>
                                <img src="assets/agent-04.jpg" alt="">
								<figcaption>
									<span class="kf_listing_overlay"></span>
								</figcaption>
                            </figure>
                            <div class="kf_agent_des">
                                <h6><a href="#">Michael Smart</a></h6>
                                <span class="kf_property_line"></span>
                                <p>Expert Agent</p>
                            </div>
                        </div>
                    </div></div><!--div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_agent_wrap">
                            <figure>
                                <img src="assets/agent-01.jpg" alt="">
								<figcaption>
									<span class="kf_listing_overlay"></span>
								</figcaption>
                            </figure>
                            <div class="kf_agent_des">
                                <h6><a href="#">Michael Smart</a></h6>
                                <span class="kf_property_line"></span>
                                <p>Expert Agent</p>
                            </div>
                        </div>
                    </div></div--><!--div style="width: 285px;" class="owl-item"><div class="item">
                        <div class="kf_agent_wrap">
                            <figure>
                                <img src="assets/agent-02.jpg" alt="">
								<figcaption>
									<span class="kf_listing_overlay"></span>
								</figcaption>
                            </figure>
                            <div class="kf_agent_des">
                                <h6><a href="#">Michael Smart</a></h6>
                                <span class="kf_property_line"></span>
                                <p>Expert Agent</p>
                            </div>
                        </div>

                    </div></div--></div></div>
                    
                    
                    
                    
                    
                <!--div class="owl-controls clickable"><div class="owl-pagination"><div class="owl-page active"><span class=""></span></div><div class="owl-page"><span class=""></span></div></div><div class="owl-buttons"><div class="owl-prev">prev</div><div class="owl-next">next</div></div></div></div-->
                <!--Agent List Wrap End-->
                
            </div>
        </section>
        <!--Our Agent Warp End-->
        
        <!--Know More About Property Wrap Start-->
        <div class="kf_know_property_bg">
        	<div class="container">
            	<div class="kf_know_property_des">
                	<h2>Know More About Property....</h2>
                    <h3>Contact Our Agent</h3>
                    <a href="#">Contact Agent</a>
                    <div class="kf_know_property_img">
                		<img src="assets/know-property-01.png" alt="">
                	</div>
                </div>
            </div>
        </div>
        <!--Know More About Property Wrap End-->
        
        <!--Testimonial & Company core Value Wrap Start-->
        <section>
        	<div class="container">
            	<div class="row">
                	<!--Testimonial Wrap Start-->
                	<div class="col-md-5">
                        <div class="kf_testimonial_wrap kf_heading_2">
                            <h3>testimoinals</h3>
                            <div style="max-width: 100%;" class="bx-wrapper"><div style="width: 100%; overflow: hidden; position: relative; height: 313px;" class="bx-viewport"><ul style="width: 515%; position: relative; transition-duration: 0.5s; transform: translate3d(-1374px, 0px, 0px);" class="kf_testimonial_slider"><li class="bx-clone" style="float: left; list-style: outside none none; position: relative; width: 458px;">
                                    <div class="kf_testimonial_slide">
                                        <figure>
                                            <img src="assets/testimonial-03.png" alt="">
                                        </figure>
                                        <div class="kf_testimonial_name">
                                            <h5>Derrick James</h5>
                                            <h6>CEO</h6>
                                            <ul>
                                                <li>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="kf_testimonial_slide_des">
                                        <p> I am so happy, my dear 
friend, so absorbed in the exquisite sense of mere tranquil existence, 
that I neglect my talents. I should be incapable of drawing a single 
stroke at the present moment than now.</p>
                                    </div>
                                </li>
                                <li style="float: left; list-style: outside none none; position: relative; width: 458px;">
                                    <div class="kf_testimonial_slide">
                                        <figure>
                                            <img src="assets/testimonial-01.png" alt="">
                                        </figure>
                                        <div class="kf_testimonial_name">
                                            <h5>Derrick James</h5>
                                            <h6>CEO</h6>
                                            <ul>
                                                <li>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="kf_testimonial_slide_des">
                                        <p>A wonderful serenity has 
taken possession of my entire soul, like these sweet mornings of spring 
which I enjoy with my whole heart. I am alone, and feel the charm of 
existence in this spot.</p>
                                    </div>
                                </li>
                                <li style="float: left; list-style: outside none none; position: relative; width: 458px;">
                                    <div class="kf_testimonial_slide">
                                        <figure>
                                            <img src="assets/testimonial-02.png" alt="">
                                        </figure>
                                        <div class="kf_testimonial_name">
                                            <h5>Derrick James</h5>
                                            <h6>CEO</h6>
                                            <ul>
                                                <li>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="kf_testimonial_slide_des">
                                        <p>When she reached the first 
hills of the Italic Mountains, she had a last view back on the skyline 
of her hometown Bookmarksgrove, the headline of Alphabet Village and the
 subline of her own road, the Line Lane.</p>
                                    </div>
                                </li>
                                <li style="float: left; list-style: outside none none; position: relative; width: 458px;">
                                    <div class="kf_testimonial_slide">
                                        <figure>
                                            <img src="assets/testimonial-03.png" alt="">
                                        </figure>
                                        <div class="kf_testimonial_name">
                                            <h5>Derrick James</h5>
                                            <h6>CEO</h6>
                                            <ul>
                                                <li>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="kf_testimonial_slide_des">
                                        <p> I am so happy, my dear 
friend, so absorbed in the exquisite sense of mere tranquil existence, 
that I neglect my talents. I should be incapable of drawing a single 
stroke at the present moment than now.</p>
                                    </div>
                                </li>
                            <li class="bx-clone" style="float: left; list-style: outside none none; position: relative; width: 458px;">
                                    <div class="kf_testimonial_slide">
                                        <figure>
                                            <img src="assets/testimonial-01.png" alt="">
                                        </figure>
                                        <div class="kf_testimonial_name">
                                            <h5>Derrick James</h5>
                                            <h6>CEO</h6>
                                            <ul>
                                                <li>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="kf_testimonial_slide_des">
                                        <p>A wonderful serenity has 
taken possession of my entire soul, like these sweet mornings of spring 
which I enjoy with my whole heart. I am alone, and feel the charm of 
existence in this spot.</p>
                                    </div>
                                </li></ul></div><div class="bx-controls bx-has-pager bx-has-controls-direction"><div class="bx-pager bx-default-pager"><div class="bx-pager-item"><a href="" data-slide-index="0" class="bx-pager-link">1</a></div><div class="bx-pager-item"><a href="" data-slide-index="1" class="bx-pager-link">2</a></div><div class="bx-pager-item"><a href="" data-slide-index="2" class="bx-pager-link active">3</a></div></div><div class="bx-controls-direction"><a class="bx-prev" href="">Prev</a><a class="bx-next" href="">Next</a></div></div></div>
                        </div>
                	</div>
                    <!--Testimonial Wrap Start-->
                    
                    <!--Company Core Value Wrap Start-->
                    <div class="col-md-7">
                    	<div class="kf_core_value_wrap kf_heading_2">
                        	<h3>Company Core Value</h3>
                            <ul data-tabs="tabs">
                            	<li class="active"><a data-toggle="tab" href="#quality">Quality</a></li>
                                <li><a data-toggle="tab" href="#commitment">Commitment</a></li>
                                <li><a data-toggle="tab" href="#success">Success</a></li>
                            </ul>
                        </div>
                        <div class="tab-content">
                        	<div class="tab-pane active" id="quality">
                                <div class="kf_core_value_tab">
                                    <div class="kf_core_value_des">
                                        <figure>
                                            <img src="assets/core-value-01.jpg" alt="">
                                        </figure>
                                        <div class="kf_core_img_des">
                                            <h6>Promo Ads that Enhances your Website to display in another level.</h6>
                                            <p>At vero eos et accusamus 
et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum 
deleniti atque corrupti quos dolores.. </p>
                                        </div>
                                    </div>
                                    <ul class="kf_aboutus_list">
										<li>Professional Variations</li>
										<li>Happy Clients</li>
										<li>Guaranteed</li>
										<li>Clean design</li>
										<li>Consulting</li>
										<li>Advertise</li>
									</ul>
                                </div>
                            </div>
                            
                            <div class="tab-pane" id="commitment">
                                <div class="kf_core_value_tab">
                                    <div class="kf_core_value_des">
                                        <figure>
                                            <img src="assets/core-value-02.jpg" alt="">
                                        </figure>
                                        <div class="kf_core_img_des">
                                            <h6>Bill Tower, Survey, NJ</h6>
                                            <p>At vero eos et accusamus 
et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum 
deleniti atque corrupti quos dolores.. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="tab-pane" id="success">
                                <div class="kf_core_value_tab">
                                    <div class="kf_core_value_des">
                                        <figure>
                                            <img src="assets/core-value-03.jpg" alt="">
                                        </figure>
                                        <div class="kf_core_img_des">
                                            <h6>Florida 5, Pinecrest, FL</h6>
                                            <p>At vero eos et accusamus 
et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum 
deleniti atque corrupti quos dolores.. </p>
                                        </div>
                                    </div>
                                    <ul class="kf_aboutus_list">
                                            <li>Professional Variations</li>
                                            <li>Professional Variations</li>
                                            <li>Professional Variations</li>
                                            <li>Professional Variations</li>
                                            <li>Professional Variations</li>
                                            <li>Professional Variations</li>
                                        </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Company Core Value Wrap End-->
                    
                </div>
            </div>
        </section>
        <!--Testimonial & Company core Value Wrap End-->
        
    </div>
    <!--Content Wrap End-->
    
    <!--Footer Wrap Start-->
    
    <!--Map Wrap Start-->
    <div class="kf_property_map_wrap">
        <div style="position: relative; background-color: rgb(229, 227, 223); overflow: hidden;" class="map-canvas" id="map-canvas"><div class="gm-style" style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; width: 100%; will-change: transform;"><div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; position: absolute; left: 318px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 574px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 318px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 318px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 574px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 574px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 62px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 830px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 62px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 62px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 830px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 830px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -194px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1086px; top: 8px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -194px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -194px; top: 264px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1086px; top: -248px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1086px; top: 264px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 318px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 574px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 318px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 318px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 574px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 574px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 62px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 830px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 62px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 62px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 830px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 830px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -194px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1086px; top: 8px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -194px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -194px; top: 264px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1086px; top: -248px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1086px; top: 264px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="position: absolute; left: 318px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_010.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 574px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_006.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 318px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_017.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 318px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_015.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 574px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_018.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 574px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_011.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 62px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_002.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 830px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_004.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 62px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_012.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 62px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 830px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_005.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 830px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_016.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -194px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_008.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1086px; top: 8px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_009.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -194px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_014.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -194px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_003.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1086px; top: -248px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_007.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1086px; top: 264px; transition: opacity 200ms ease-out 0s;"><img alt="" draggable="false" src="assets/vt_013.png" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div></div></div><div class="gm-style-pbc" style="position: absolute; left: 0px; top: 0px; z-index: 2; width: 100%; height: 100%; transition-duration: 0.3s; opacity: 0; display: none;"><p class="gm-style-pbt">Use two fingers to move the map</p></div><div style="position: absolute; left: 0px; top: 0px; z-index: 3; width: 100%; height: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 4; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a title="Click to see this area on Google Maps" href="https://maps.google.com/maps?ll=40.674389,-73.9455&amp;z=15&amp;hl=en-US&amp;gl=US&amp;mapclient=apiv3" target="_blank" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 66px; height: 26px; cursor: pointer;"><img draggable="false" src="assets/google_white5.png" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;"></div></a></div><div style="background-color: white; padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto,Arial,sans-serif; color: rgb(34, 34, 34); box-shadow: 0px 4px 16px rgba(0, 0, 0, 0.2); z-index: 10000002; display: none; width: 256px; height: 148px; position: absolute; left: 505px; top: 110px;"><div style="padding: 0px 0px 10px; font-size: 16px;">Map Data</div><div style="font-size: 13px;">Map data ©2016 Google</div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img draggable="false" src="assets/mapcnt6.png" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div><div style="z-index: 1000001; position: absolute; right: 75px; bottom: 0px; width: 121px;" class="gmnoprint"><div class="gm-style-cc" style="-moz-user-select: none; height: 14px; line-height: 14px;" draggable="false"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer; display: none;">Map Data</a><span style="">Map data ©2016 Google</span></div></div></div><div style="position: absolute; right: 0px; bottom: 0px;" class="gmnoscreen"><div style="font-family: Roboto,Arial,sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Map data ©2016 Google</div></div><div draggable="false" style="z-index: 1000001; -moz-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;" class="gmnoprint gm-style-cc"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_blank" href="https://www.google.com/intl/en-US_US/help/terms_maps.html" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Terms of Use</a></div></div><div style="width: 25px; height: 25px; overflow: hidden; display: none; margin: 10px 14px; position: absolute; top: 0px; right: 0px;"><img class="gm-fullscreen-control" draggable="false" src="assets/sv5.png" style="position: absolute; left: -52px; top: -86px; width: 164px; height: 112px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px;"></div><div class="gm-style-cc" style="-moz-user-select: none; height: 14px; line-height: 14px; display: none; position: absolute; right: 0px; bottom: 0px;" draggable="false"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/maps/@40.674389,-73.9455,15z/data=%2110m1%211e1%2112b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto,Arial,sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;" title="Report errors in the road map or imagery to Google" target="_new">Report a map error</a></div></div><div controlheight="93" controlwidth="28" draggable="false" style="margin: 10px; -moz-user-select: none; position: absolute; bottom: 107px; right: 28px;" class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom"><div controlheight="55" controlwidth="28" style="position: absolute; left: 0px; top: 38px;" class="gmnoprint"><div style="-moz-user-select: none; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); border-radius: 2px; cursor: pointer; background-color: rgb(255, 255, 255); width: 28px; height: 55px;" draggable="false"><div style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;" title="Zoom in"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img draggable="false" src="assets/tmapctrl.png" style="position: absolute; left: 0px; top: 0px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div><div style="position: relative; overflow: hidden; width: 67%; height: 1px; left: 16%; background-color: rgb(230, 230, 230); top: 0px;"></div><div style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;" title="Zoom out"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img draggable="false" src="assets/tmapctrl.png" style="position: absolute; left: 0px; top: -15px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div></div></div><div controlheight="28" controlwidth="28" style="background-color: rgb(255, 255, 255); box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); border-radius: 2px; width: 28px; height: 28px; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default; position: absolute; left: 0px; top: 0px;" class="gm-svpc"><div style="position: absolute; left: 1px; top: 1px;"></div><div style="position: absolute; left: 1px; top: 1px;"><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px;"><img draggable="false" src="assets/cb_scout5.png" style="position: absolute; left: -147px; top: -26px; width: 215px; height: 835px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Pegman is on top of the Map" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img draggable="false" src="assets/cb_scout5.png" style="position: absolute; left: -147px; top: -52px; width: 215px; height: 835px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img draggable="false" src="assets/cb_scout5.png" style="position: absolute; left: -147px; top: -78px; width: 215px; height: 835px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div style="display: none; position: absolute;" controlheight="0" controlwidth="28" class="gmnoprint"><div title="Rotate map 90 degrees" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); cursor: pointer; background-color: rgb(255, 255, 255); display: none;"><img draggable="false" src="assets/tmapctrl4.png" style="position: absolute; left: -141px; top: 6px; width: 170px; height: 54px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div><div class="gm-tilt" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); top: 0px; cursor: pointer; background-color: rgb(255, 255, 255);"><img draggable="false" src="assets/tmapctrl4.png" style="position: absolute; left: -141px; top: -13px; width: 170px; height: 54px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div style="margin: 10px; z-index: 0; position: absolute; cursor: pointer; left: 0px; top: 0px;" class="gmnoprint"><div class="gm-style-mtc" style="float: left;"><div title="Show street map" draggable="false" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(86, 86, 86); font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-left-radius: 2px; border-top-left-radius: 2px; background-clip: padding-box; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); min-width: 22px;">Map</div></div><div class="gm-style-mtc" style="float: left;"><div draggable="false" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(0, 0, 0); font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-right-radius: 2px; border-top-right-radius: 2px; background-clip: padding-box; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); border-left: 0px none; min-width: 67px; font-weight: 500;">Custom Style</div></div></div></div></div>
    </div>
    <!--Map Wrap End-->
    
    <!--Company Wrap Start-->
    <!--div class="kf_property_compnay_bg">
        <div class="container">
            <div style="opacity: 1; display: block;" class="kf_company_slider owl-carousel owl-theme">
                <div class="owl-wrapper-outer"><div style="width: 3192px; left: 0px; display: block; transition: all 800ms ease 0s; transform: translate3d(-228px, 0px, 0px);" class="owl-wrapper"><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-01.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-02.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-03.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-04.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-05.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-01.jpg" alt=""></a>
                    </div>
                </div></div><div style="width: 228px;" class="owl-item"><div class="item">
                    <div class="kf_compnay_wrap">
                        <a href="#"><img src="assets/company-02.jpg" alt=""></a>
                    </div>
                </div></div></div></div>
                
                
                
                
                
                
            <div style="display: block;" class="owl-controls clickable"><div class="owl-pagination"><div class="owl-page active"><span class=""></span></div><div class="owl-page"><span class=""></span></div></div><div class="owl-buttons"><div class="owl-prev">prev</div><div class="owl-next">next</div></div></div></div>
        </div>
    </div-->
    <!--Company Wrap End-->
 <?php include('footer.php');?>
