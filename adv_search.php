    <?php include('header.php');?>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script type="text/javascript" src="assets/jquery.pajinate.js"></script>
    <!--Sub Banner Wrap Start-->
    <div class="kf_property_sub_banner">
    	<div class="container">
        	<div class="kf_sub_banner_hdg">
            	<h3>Advanced Search</h3>
            </div>
            <div class="kf_property_breadcrumb">
            	<ul>
                	<li><a href="#">Home</a></li>
                    <li><a href="#">Advanced Search</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!--Sub Banner Wrap End-->
    
    <!--Content Wrap Start-->
    <div class="kf_property_content_wrap">
		<!--Most Recent Property Wrap Start-->
        <section class="kf_recent_property_bg">
        	<div class="container" >
               <!--Most Recent Property List Wrap Start-->
                <div class="row ">                	
 
									  
<div style="" class="js-sf">
    <div class="si-content-area">
      <h5 class="js-title">Advanced Property Search</h5>
      </br>
    </div>
    <div class="si-container clearfix">
      <div class="si-sf-main">
      
        <div class="si-sf-qs js-quick-search">
          <div class="input-group">
            <input type="text" placeholder="Quick Search by Address or MLS Number" class="si-sf-qs__field js-qs-input ui-autocomplete-input" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
            <span class="input-group-btn">
              <button type="submit" class="si-sf-qs__btn js-qs-btn"></button>
            </span>
          </div>
        </div>
        <section style="display: none;" class="si-sf-section js-save-search">
          <h2>Name Your Search</h2>
          <div class="si-sf-searchname">
            <input type="text" placeholder="Your Name for this Search" class="js-searchname">
          </div>
          <label>Receive email alert when new listings match your search criteria?</label>
          <div class="si-sf-radio clearfix">
            <div class="si-sf-radio__item">
              <input type="radio" checked="" value="-1" name="emailalert" id="emailalert1">
              <label for="emailalert1">Never</label>
            </div>
            
            <div class="si-sf-radio__item">
              <input type="radio" value="1" name="emailalert" id="emailalert3">
              <label for="emailalert3">Once a Day</label>
            </div>
            <div class="si-sf-radio__item">
              <input type="radio" value="7" name="emailalert" id="emailalert4">
              <label for="emailalert4">Once a Week</label>
            </div>
          </div>
        </section>
        <div class="js-sf-container">
          <section class="si-sf-section">
            <h2>Property Type</h2>
            <div class="si-sf-check clearfix js-property-types">
  <div class="si-sf-check__item">
    <input type="checkbox" value="1" name="listtype" id="pt1">
    <label for="pt1">Residential</label>
  </div>

  <div class="si-sf-check__item">
    <input type="checkbox" value="11" name="listtype" id="pt2">
    <label for="pt2">Business Opportunities</label>
  </div>

  <div class="si-sf-check__item">
    <input type="checkbox" value="3" name="listtype" id="pt3">
    <label for="pt3">Commercial</label>
  </div>

  <div class="si-sf-check__item">
    <input type="checkbox" value="4" name="listtype" id="pt4">
    <label for="pt4">Lots / Land</label>
  </div>

  <div class="si-sf-check__item">
    <input type="checkbox" value="5" name="listtype" id="pt5">
    <label for="pt5">Multi-Family</label>
  </div>

  <div class="si-sf-check__item">
    <input type="checkbox" value="8" name="listtype" id="pt6">
    <label for="pt6">Residential Lease</label>
  </div>
</div>
          </section>
          
          
          <section class="si-sf-section">
            <h2>Search by Location Type</h2>
            <div class="si-row si-sf-location">
              <div class="si-sf-location__select">
                <select class="js-location-select" sb="39884871" style="display: none;"><option value="2">City</option><option value="11">Subdivision</option><option value="10">Development</option><option value="13">Zip Code</option><option value="4">County</option></select><div id="sbHolder_39884871" class="sbHolder"><a id="sbToggle_39884871" href="#" class="sbToggle"></a><a id="sbSelector_39884871" href="#" class="sbSelector">City</a><ul id="sbOptions_39884871" class="sbOptions" style="display: none;"><li><a href="#2" rel="2">City</a></li><li><a href="#11" rel="11">Subdivision</a></li><li><a href="#10" rel="10">Development</a></li><li><a href="#13" rel="13">Zip Code</a></li><li><a href="#4" rel="4">County</a></li></ul></div>
              </div>
              <div class="si-sf-location__input js-location-input-container"><input type="text" data-label="City" data-id="2" id="location2" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="ui-autocomplete-input" style="display: inline;"><input type="text" data-label="Subdivision" data-id="11" id="location11" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="ui-autocomplete-input" style="display: none;"><input type="text" data-label="Development" data-id="10" id="location10" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="ui-autocomplete-input" style="display: none;"><input type="text" data-label="Zip Code" data-id="13" id="location13" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="ui-autocomplete-input" style="display: none;"><input type="text" data-label="County" data-id="4" id="location4" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="ui-autocomplete-input" style="display: none;"></div>
              <div class="si-sf-location__selected si-selected-items js-selected-location-container"></div>
            </div>
          </section>
          
          
          <section class="si-sf-section">
            <div class="si-sf-primary si-row js-primary-criteria">
  <div class="si-sf-primary__item">
    <h2>Price From</h2>
    <div class="si-sf-select">
      <select class="js-primary-select" name="primary1" id="primary1" sb="15592212" style=""><option value="">Min Price</option><option value="50000">$50,000</option><option value="60000">$60,000</option><option value="70000">$70,000</option><option value="80000">$80,000</option><option value="90000">$90,000</option><option value="100000">$100,000</option><option value="125000">$125,000</option><option value="150000">$150,000</option><option value="175000">$175,000</option><option value="200000">$200,000</option><option value="225000">$225,000</option><option value="250000">$250,000</option><option value="275000">$275,000</option><option value="300000">$300,000</option><option value="325000">$325,000</option><option value="350000">$350,000</option><option value="375000">$375,000</option><option value="400000">$400,000</option><option value="425000">$425,000</option><option value="450000">$450,000</option><option value="475000">$475,000</option><option value="500000">$500,000</option><option value="550000">$550,000</option><option value="600000">$600,000</option><option value="650000">$650,000</option><option value="700000">$700,000</option><option value="750000">$750,000</option><option value="800000">$800,000</option><option value="850000">$850,000</option><option value="900000">$900,000</option><option value="950000">$950,000</option><option value="1000000">$1,000,000</option><option value="1250000">$1,250,000</option><option value="1500000">$1,500,000</option><option value="1750000">$1,750,000</option><option value="2000000">$2,000,000</option><option value="2250000">$2,250,000</option><option value="2500000">$2,500,000</option><option value="2750000">$2,750,000</option><option value="3000000">$3,000,000</option><option value="3250000">$3,250,000</option><option value="3500000">$3,500,000</option><option value="3750000">$3,750,000</option><option value="4000000">$4,000,000</option><option value="4250000">$4,250,000</option><option value="4500000">$4,500,000</option><option value="4750000">$4,750,000</option><option value="5000000">$5,000,000</option><option value="5500000">$5,500,000</option><option value="6000000">$6,000,000</option><option value="6500000">$6,500,000</option><option value="7000000">$7,000,000</option><option value="7500000">$7,500,000</option><option value="8000000">$8,000,000</option><option value="8500000">$8,500,000</option><option value="9000000">$9,000,000</option><option value="9500000">$9,500,000</option><option value="10000000">$10,000,000</option><option value="11000000">$11,000,000</option><option value="12000000">$12,000,000</option><option value="13000000">$13,000,000</option><option value="14000000">$14,000,000</option><option value="15000000">$15,000,000</option><option value="16000000">$16,000,000</option><option value="17000000">$17,000,000</option><option value="18000000">$18,000,000</option><option value="19000000">$19,000,000</option><option value="20000000">$20,000,000</option></select>
    </div>
  </div>

  <div class="si-sf-primary__item">
    <h2>Price To</h2>
    <div class="si-sf-select">
      <select class="js-primary-select" name="primary2" id="primary2" sb="5981350" style=""><option value="">Max Price</option><option value="50000">$50,000</option><option value="60000">$60,000</option><option value="70000">$70,000</option><option value="80000">$80,000</option><option value="90000">$90,000</option><option value="100000">$100,000</option><option value="125000">$125,000</option><option value="150000">$150,000</option><option value="175000">$175,000</option><option value="200000">$200,000</option><option value="225000">$225,000</option><option value="250000">$250,000</option><option value="275000">$275,000</option><option value="300000">$300,000</option><option value="325000">$325,000</option><option value="350000">$350,000</option><option value="375000">$375,000</option><option value="400000">$400,000</option><option value="425000">$425,000</option><option value="450000">$450,000</option><option value="475000">$475,000</option><option value="500000">$500,000</option><option value="550000">$550,000</option><option value="600000">$600,000</option><option value="650000">$650,000</option><option value="700000">$700,000</option><option value="750000">$750,000</option><option value="800000">$800,000</option><option value="850000">$850,000</option><option value="900000">$900,000</option><option value="950000">$950,000</option><option value="1000000">$1,000,000</option><option value="1250000">$1,250,000</option><option value="1500000">$1,500,000</option><option value="1750000">$1,750,000</option><option value="2000000">$2,000,000</option><option value="2250000">$2,250,000</option><option value="2500000">$2,500,000</option><option value="2750000">$2,750,000</option><option value="3000000">$3,000,000</option><option value="3250000">$3,250,000</option><option value="3500000">$3,500,000</option><option value="3750000">$3,750,000</option><option value="4000000">$4,000,000</option><option value="4250000">$4,250,000</option><option value="4500000">$4,500,000</option><option value="4750000">$4,750,000</option><option value="5000000">$5,000,000</option><option value="5500000">$5,500,000</option><option value="6000000">$6,000,000</option><option value="6500000">$6,500,000</option><option value="7000000">$7,000,000</option><option value="7500000">$7,500,000</option><option value="8000000">$8,000,000</option><option value="8500000">$8,500,000</option><option value="9000000">$9,000,000</option><option value="9500000">$9,500,000</option><option value="10000000">$10,000,000</option><option value="11000000">$11,000,000</option><option value="12000000">$12,000,000</option><option value="13000000">$13,000,000</option><option value="14000000">$14,000,000</option><option value="15000000">$15,000,000</option><option value="16000000">$16,000,000</option><option value="17000000">$17,000,000</option><option value="18000000">$18,000,000</option><option value="19000000">$19,000,000</option><option value="20000000">$20,000,000</option></select>
    </div>
  </div>

  <div class="si-sf-primary__item">
    <h2>Square Ft.</h2>
    <div class="si-sf-select">
      <select class="js-primary-select" name="primary7" id="primary7" sb="47423423" style="">
        
  <option value="">...</option>

  <option value="500">500+</option>

  <option value="1000">1000+</option>

  <option value="1500">1500+</option>

  <option value="2000">2000+</option>

  <option value="2500">2500+</option>

  <option value="3000">3000+</option>

  <option value="3500">3500+</option>

  <option value="4000">4000+</option>

  <option value="4500">4500+</option>

  <option value="5000">5000+</option>

  <option value="5500">5500+</option>

  <option value="6000">6000+</option>

  <option value="6500">6500+</option>

  <option value="7000">7000+</option>

      </select>
    </div>
  </div>

  <div class="si-sf-primary__item">
    <h2>Bedrooms</h2>
    <div class="si-sf-select">
      <select class="js-primary-select" name="primary3" id="primary3" sb="20102515" style="">
        
  <option value="">...</option>

  <option value="1">1+</option>

  <option value="2">2+</option>

  <option value="3">3+</option>

  <option value="4">4+</option>

  <option value="5">5+</option>

  <option value="6">6+</option>

      </select>
    </div>
  </div>

  <div class="si-sf-primary__item">
    <h2>Bathrooms</h2>
    <div class="si-sf-select">
      <select class="js-primary-select" name="primary5" id="primary5" sb="61642686" style="">
        
  <option value="">...</option>

  <option value="1">1+</option>

  <option value="2">2+</option>

  <option value="3">3+</option>

  <option value="4">4+</option>

  <option value="5">5+</option>

  <option value="6">6+</option>

      </select>
    </div>
  </div>

  <div class="si-sf-primary__item">
    <h2>Garage Space</h2>
    <div class="si-sf-select">
      <select class="js-primary-select" name="primary8" id="primary8" sb="69855271" style="">
        
  <option value="">...</option>

  <option value="1">1+</option>

  <option value="2">2+</option>

  <option value="3">3+</option>

  <option value="4">4+</option>

      </select>
    </div>
  </div>

  <div class="si-sf-primary__item">
    <h2>Year Built</h2>
    <div class="si-sf-select">
      <select class="js-primary-select" name="primary9" id="primary9" sb="47454279" style="">
        
  <option value="">...</option>

  <option value="2">&lt; 2 years old</option>

  <option value="3">&lt; 3 years old</option>

  <option value="5">&lt; 5 years old</option>

  <option value="15">&lt; 15 years old</option>

  <option value="25">&lt; 25 years old</option>

  <option value="26">More Than 25 years old</option>

      </select>
    </div>
  </div>
</div>
          </section>
          <section class="si-sf-section">
            <h2>Additional Features</h2>
            <div class="si-sf-feature si-row js-feature-criteria">
  <div class="si-sf-feature__item">
    <div class="si-sf-select">
      <select data-selectiontype="0" data-id="11" class="js-feature-select" name="feature11" id="feature11" sb="62238756" style="">
        <option value="">Lot/Community Features</option>
        
  <option data-property="MLSGolfCourseFront" data-value="1" value="120">On Golf Course</option>

  <option data-property="MLSWaterfront" data-value="1" value="121">Waterfront</option>

  <option data-property="MLSFeaturesLot" data-value="Private Dock" value="122">Private Dock</option>

  <option data-property="MLSFeaturesLot" data-value="Treed Lot" value="123">Wooded Lot</option>

  <option data-property="MLSFeaturesExterior" data-value="Gate-manned" value="124">Manned Gate</option>

  <option data-property="MLSFeaturesExterior" data-value="Sec Patrol" value="125">Security Patrol</option>

  <option data-property="MLSFeaturesLot" data-value="Ocean" value="126">Ocean View</option>

  <option data-property="MLSFeaturesLot" data-value="Canal" value="127">Canal View</option>

  <option data-property="MLSFeaturesLot" data-value="Golf" value="128">Golf Course View</option>

      </select>
    </div>
    <div data-id="11" class="si-selected-items is-block js-selected-features">
    </div>
  </div>

  <div class="si-sf-feature__item">
    <div class="si-sf-select">
      <select data-selectiontype="1" data-id="25" class="js-feature-select" name="feature25" id="feature25" sb="68658886">
        <option value="">Pets Allowed</option>
        
  <option data-property="MLSFeaturesCondition" data-value="P237" value="261">Yes</option>

  <option data-property="MLSFeaturesCondition" data-value="P239" value="262">No</option>

      </select>
    </div>
    <div data-id="25" class="si-selected-items is-block js-selected-features">
    </div>
  </div>

  <div class="si-sf-feature__item">
    <div class="si-sf-select">
      <select data-selectiontype="1" data-id="26" class="js-feature-select" name="feature26" id="feature26" sb="97736249" style="">
        <option value="">55+ Community</option>
        
  <option data-property="MLSFeaturesCondition" data-value="55281" value="263">Yes</option>

  <option data-property="MLSFeaturesCondition" data-value="55244" value="264">No</option>

      </select>
    </div>
    <div data-id="26" class="si-selected-items is-block js-selected-features">
    </div>
  </div>

  <div class="si-sf-feature__item">
    <div class="si-sf-select">
      <select data-selectiontype="1" data-id="27" class="js-feature-select" name="feature27" id="feature27" sb="14817976" style="">
        <option value="">Equity Required</option>
        
  <option data-property="MLSFeaturesCondition" data-value="ER283" value="265">Yes</option>

  <option data-property="MLSFeaturesCondition" data-value="ER284" value="266">No</option>

      </select>
    <div data-id="27" class="si-selected-items is-block js-selected-features">
    </div>
  </div>
 </div>
  <div class="si-sf-feature__item">
    <div class="si-sf-select">
      <select data-selectiontype="0" data-id="10" class="js-feature-select" name="feature10" id="feature10" sb="59840582" style="">
        <option value="">Exterior</option>
        
  <option data-property="MLSFeaturesInterior" data-value="Inground" value="114">Inground Pool</option>

  <option data-property="MLSSpa" data-value="1" value="115">Spa</option>

  <option data-property="MLSFeaturesExterior" data-value="Scrnd Patio" value="116">Screened Patio</option>

  <option data-property="MLSFeaturesExterior" data-value="Deck" value="117">Deck</option>

  <option data-property="MLSFeaturesExterior" data-value="Fence" value="118">Fence</option>

  <option data-property="MLSFeaturesExterior" data-value="Auto Sprink" value="119">Auto Sprinklers</option>

      </select>
    </div>
    <div data-id="10" class="si-selected-items is-block js-selected-features">
    </div>
  </div>

  <div class="si-sf-feature__item">
    <div class="si-sf-select">
      <select data-selectiontype="0" data-id="9" class="js-feature-select" name="feature9" id="feature9" sb="45917449" style="">
        <option value="">Interior</option>
        
  <option data-property="MLSFeaturesEnergy" data-value="Cooling:&lt;/strong&gt; Zoned" value="108">Zoned Cooling</option>

  <option data-property="MLSFeaturesInterior" data-value="Elevator" value="110">Elevator</option>

  <option data-property="MLSFeaturesInterior" data-value="Fireplace" value="111">Fireplace</option>

  <option data-property="MLSFeaturesInterior" data-value="Wet Bar" value="112">Wet Bar</option>

  <option data-property="MLSFeaturesInterior" data-value="Cat/Vau Ceil" value="113">Vaulted Ceilings</option>

      </select>
    </div>
    <div data-id="9" class="si-selected-items is-block js-selected-features">
    </div>
  </div>
</div>
          </section>
    <section class="si-sf-section">
            <div class="si-sf-filter si-row js-filter-criteria">
  <div class="si-sf-filter__item">
    <h2>New on Market</h2>
    <div class="si-sf-select">
      <select class="js-primary-select" name="primary10" id="primary10" sb="78996644" >
        
  <option value="">...</option>

  <option value="14">2 Weeks</option>

  <option value="7">1 Week</option>

  <option value="3">3 Days</option>

  <option value="1">1 Day</option>

      </select>
    </div>
  </div>

  <div class="si-sf-filter__item">
    <h2>Sort Results By</h2>
    <div class="si-sf-select">
      <select class="js-primary-select" name="primary4" id="primary4" sb="85432738" >
        
  <option value="m.Price DESC">Price (High to Low)</option>

  <option value="m.Price ASC">Price (Low to High)</option>

  <option value="m.DateListed DESC">Days on Market (Newest)</option>

  <option value="m.DateListed ASC">Days on Market (Oldest)</option>

  <option value="m.Beds DESC">Bedrooms (Most)</option>

      </select>
    </div>
  </div>

  <div class="si-sf-filter__item">
    <h2>Price Reduction</h2>
    <div class="si-sf-select">
      <select class="js-primary-select" name="primary6" id="primary6" sb="87191632" >
        
  <option value="">...</option>

  <option value="-.05">At least 5%</option>

  <option value="-.1">At least 10%</option>

  <option value="-.15">At least 15%</option>

  <option value="-.2">At least 20%</option>

  <option value="-.25">At least 25%</option>

      </select>
    </div>
  </div>
</div>
          </section>
          <section class="si-sf-section">
            <h2>Show Only</h2>
            <div class="si-sf-check clearfix js-secondary-criteria">
  <div class="si-sf-check__item">
    <input type="checkbox" data-type="13" name="secondary" id="secondary13">
    <label for="secondary13">Foreclosure Properties</label>
  </div>

  <div class="si-sf-check__item">
    <input type="checkbox" data-type="14" name="secondary" id="secondary14">
    <label for="secondary14">Short Sale Properties</label>
  </div>

  <div class="si-sf-check__item">
    <input type="checkbox" data-type="16" name="secondary" id="secondary16">
    <label for="secondary16">New Construction Properties</label>
  </div>
</div>
          </section>
  </div>
  <div class="si-sf-btn js-search-buttons">
          <a class="si-btn si-btn--secondary si-sf-btn__submit js-list-result" href="list_view_all.php"><i class="fa fa-bars"></i> List Results</a>
          <a class="si-btn si-btn--secondary si-sf-btn__submit js-map-result" href="search_map.php"><i class="fa fa-map-marker"></i> Map Results</a>
        </div>

                </div>
                <!--Most Recent Property List Wrap End-->
                
            </div>
        </section>
        <!--Most Recent Property Wrap End-->
    
	</div>
	<!-- Content Wrap End -->
	<style>
	
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    border-color: #ddd transparent !important;
}
.tab-content {
    margin-top: 15px;
}
.imagetest::before{
	background-color: rgba(0, 0, 0, 0.6);
    content: "";
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
}
.imagetest {
    float: left;
    margin: 0 0 30px;
    overflow: hidden;
    position: relative;
    width: 100%;
}
.si-sf-main {
    width: calc(100% - 235px);
}
.si-sf-main {
    float: left;
}
.si-sf-qs {
    margin-bottom: 30px;
}
.si-container * {
    font-family: Arial,Helvetica,sans-serif;
}
.si-container * {
    font-family: Arial,Helvetica,sans-serif;
}
.input-group {
    border-collapse: separate;
    display: table;
    position: relative;
}
.si-sf-qs__field {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: transparent;
    border-color: #6692b2 -moz-use-text-color #6692b2 #6692b2;
    border-image: none;
    border-style: solid none solid solid;
    border-width: 1px medium 1px 1px;
    color: #fff;
    font-size: 17px;
    height: 54px;
    line-height: 54px;
    padding: 0 20px;
    width: 100%;
}
.si-container * {
    font-family: Arial,Helvetica,sans-serif;
}
.si-container * {
    font-family: Arial,Helvetica,sans-serif;
}
.input-group-btn {
    font-size: 0;
    position: relative;
    white-space: nowrap;
}
.input-group-addon, .input-group-btn {
    vertical-align: middle;
    width: 1%;
}
.input-group-addon, .input-group-btn, .input-group .form-control {
    display: table-cell;
}
.si-sf-qs__btn {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: #6692b2 #6692b2 #6692b2 -moz-use-text-color;
    border-image: none;
    border-style: solid solid solid none;
    border-width: 1px 1px 1px medium;
    height: 54px;
    width: 54px;
}
.si-container * {
    font-family: Arial,Helvetica,sans-serif;
}
textarea, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
    height: 54px !important;
}
.si-sf-section {
    border-bottom: 1px solid #6692b2;
    margin: 0 0 40px;
    padding: 0 0 30px;
}
.si-container * {
    font-family: Arial,Helvetica,sans-serif;
}
article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary {
    display: block;
}.si-sf-section h2 {
    font-size: 18px;
    margin-bottom: 20px;
}
.si-container .h1, .si-container .h2, .si-container .h3, .si-container .h4, .si-container .h5, .si-container .h6, .si-container h1, .si-container h2, .si-container h3, .si-container h4, .si-container h5, .si-container h6 {
    font-style: normal;
    font-weight: 400;
    padding: 0;
}
.si-container * {
    font-family: Arial,Helvetica,sans-serif;
}
.si-sf-check__item {
    float: left;
}
.si-container * {
    font-family: Arial,Helvetica,sans-serif;
}
.si-sf-check__item input {
    display: none;
}
.si-sf-check__item input:checked + label::after {
    color: #5a9e00;
    content: "";
    display: inline-block;
    font: 17px/1 FontAwesome;
    padding-left: 8px;
    padding-top: 12px;
    text-rendering: auto;
}
.si-sf-check__item input + label::after {
    border: 1px solid #6692b2;
    content: "";
    display: block;
    height: 44px;
    position: absolute;
    right: -36px;
    top: -1px;
    width: 36px;
}
*::before, *::after {
    box-sizing: border-box;
}
.si-sf-check__item input + label {
    border: 1px solid #6692b2;
    cursor: pointer;
    display: block;
    font-weight: 400;
    height: 44px;
    margin-bottom: 15px;
    margin-right: 52px;
    padding: 10px;
    position: relative;
    white-space: nowrap;
}
.si-container * {
    font-family: Arial,Helvetica,sans-serif;
}
.si-sf-location__select .sbHolder {
    background-color: #006baa;
    border: 1px solid #6692b2;
    border-radius: 0;
    box-shadow: none;
    color: #fff;
    height: 54px;
}
.sbHolder {
    position: relative;
    width: 100%;
}
.si-row {
    margin-left: -15px;
    margin-right: -15px;
}
.si-sf-location__select {
    padding-right: 0;
}
.si-sf-location__select {
    float: left;
    width: 33.3333%;
}
.si-sf-location__select {
	min-height: 1px;
    padding-left: 15px;
    position: relative;
}
.si-sf-location__select .sbToggle {
    background-color: transparent;
    border-left: medium none;
    height: 25px;
    right: 10px;
    top: 13px;
    width: 25px;
}
.si-container a, .si-container a:focus, .si-container a:hover {
    text-decoration: none;
}
.sbToggle {
    display: inline-block;
    font-family: FontAwesome;
    font-feature-settings: normal;
    font-kerning: auto;
    font-language-override: normal;
    font-size: inherit;
    font-size-adjust: none;
    font-stretch: normal;
    font-style: normal;
    font-synthesis: weight style;
    font-variant: normal;
    font-weight: normal;
    line-height: 1;
    outline: 0 none;
    position: absolute;
    text-align: center;
    text-rendering: auto;
}
.si-sf-location__select .sbSelector:hover, .si-sf-location__select .sbSelector:link, .si-sf-location__select .sbSelector:visited {
    color: #fff;
}
.sbSelector:hover, .sbSelector:link, .sbSelector:visited {
    text-decoration: none;
}
.si-sf-location__select .sbSelector {
    font-family: Arial,sans-serif;
    font-size: 17px;
    font-weight: 400;
    height: 54px;
    line-height: 54px;
}
.sbSelector {
    display: block;
    left: 0;
    outline: 0 none;
    overflow: hidden;
    position: absolute;
    text-align: left;
    text-indent: 10px;
    top: 0;
    width: 100%;
}
.si-sf-location__select .sbOptions {
    background-color: #005694;
    border: 1px solid #6692b2;
    font-size: 15px;
    font-weight: 400;
}
.sbOptions {
    left: -1px;
    list-style: outside none none;
    margin: -2px 0 0;
    overflow-y: auto;
    padding: 0;
    position: absolute;
    text-align: left;
    top: 0;
    width: 100%;
    z-index: 1000;
}
.si-sf-location__select .sbOptions > li > a:link, .si-sf-location__select .sbOptions > li > a:visited {
    color: #fff;
}
.sbOptions > li > a, .sbOptions > li > a.sbFocus, .sbOptions > li > a:focus, .sbOptions > li > a:hover, .sbOptions > li > a:link, .sbOptions > li > a:visited {
    text-decoration: none;
}
.si-sf-location__select .sbOptions > li > a {
    border-bottom: 1px dotted #fff;
}
.si-sf-primary__item {
    float: left;
    width: 25%;
}
.si-sf-feature__item, .si-sf-filter__item, .si-sf-primary__item {
	  margin-bottom: 30px;
    min-height: 1px;
    padding-left: 15px;
    padding-right: 15px;
    position: relative;
}
.si-sf-section h2 {
    font-size: 18px;
    margin-bottom: 20px;
}
.si-container .h1, .si-container .h2, .si-container .h3, .si-container .h4, .si-container .h5, .si-container .h6, .si-container h1, .si-container h2, .si-container h3, .si-container h4, .si-container h5, .si-container h6 {
    font-style: normal;
    font-weight: 400;
     padding: 0;
}
input, button, select, textarea {
    font-size: inherit;
    line-height: inherit;
}
button, select {
    text-transform: none;
}
button, input, optgroup, select, textarea {
    color: inherit;
    margin: 0;
}
.si-sf-select .sbHolder {
    background-color: transparent;
    border: 1px solid #6692b2;
    border-radius: 0;
    box-shadow: none;
    color: #fff;
    height: 44px;
}
.sbHolder {
    position: relative;
    width: 100%;
}
.si-sf-select .sbToggle::after {
    color: #fff;
    content: "";
    font-size: 1.7em;
    line-height: 44px;
}
*::before, *::after {
    box-sizing: border-box;
}
.si-sf-select .sbToggle {
    background-color: transparent;
    border-left: medium none;
    height: 44px;
    right: 0;
    top: 0;
    width: 32px;
}
.si-container a, .si-container a:focus, .si-container a:hover {
    text-decoration: none;
}
.sbToggle {
    display: inline-block;
    font-family: FontAwesome;
    font-feature-settings: normal;
    font-kerning: auto;
    font-language-override: normal;
    font-size: inherit;
    font-size-adjust: none;
    font-stretch: normal;
    font-style: normal;
    font-synthesis: weight style;
    font-variant: normal;
    font-weight: normal;
    line-height: 1;
    outline: 0 none;
    position: absolute;
    text-align: center;
    text-rendering: auto;
}
.si-sf-btn {
    text-align: center;
}
.si-container a, .si-container a:focus, .si-container a:hover {
    text-decoration: none;
}
.si-sf-btn__submit {
    border-radius: 0;
    font-size: 17px;
    margin: 0 15px 20px;
    padding: 17px 10px;
    width: 32%;
}
.si-btn--secondary {
    background-color: #006BAA;
    border-color: #006BAA;
    color: #fff;
}
.si-btn {
    -moz-user-select: none;
    background-image: none;
     cursor: pointer;
    display: inline-block;
    font-family: Arial,Helvetica,sans-serif;
     font-weight: 400;
    line-height: 1.42857;
     text-align: center;
    vertical-align: middle;
    white-space: nowrap;
}
.fa-navicon::before, .fa-reorder::before, .fa-bars::before {
    content: "";
}
.si-container .fa {
    font-family: FontAwesome !important;
}
.si-container * {
    font-family: Arial,Helvetica,sans-serif;
}
.fa {
    display: inline-block;
    font-feature-settings: normal;
    font-kerning: auto;
    font-language-override: normal;
    font-size: inherit;
    font-size-adjust: none;
    font-stretch: normal;
    font-style: normal;
    font-synthesis: weight style;
    font-variant: normal;
    font-weight: normal;
    line-height: 1;
    text-rendering: auto;
}
	</style>
   <?php include('footer.php');?>
